#!/bin/bash

#module unload intel
#module load intel/19.1
module load netcdf
module load metis
module load cmake
#https://bitbucket.org/berkeleylab/upcxx/issues/302/build-failure-for-intel-c-with-std-c-17
#so load gcc8?
module load gcc/8.3
module load cmake/3.14.4
module load boost/1.68.0-intel-impi

make clean

rm CMakeCache.txt
rm -r CMakeFiles
rm cmake_install.cmake
rm Makefile

export UPCXX_INSTALL=~/upcxx-intel-mpp3

cmake . -DCMAKE_CXX_COMPILER=mpiicpc -DCMAKE_C_COMPILER=mpiicc -DCMAKE_PREFIX_PATH=$UPCXX_INSTALL
make actorlib -j 16
make pond -j 16