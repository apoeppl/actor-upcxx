#!/usr/bin/env bash

echo download upc++...
FILE=upcxx-2021.3.0.tar.gz
if [ -f "$FILE" ]
then
    echo "$FILE exists"
else
    wget https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2021.3.0.tar.gz
fi
tar xvf upcxx-2021.3.0.tar.gz
mkdir ~/upcxx-intel-mpp3
cd upcxx-2021.3.0/

echo load modules...
module load gcc/8.3
module load netcdf
module load metis 
module load cmake/3.14.4


echo start building upc++...
export UPATH=~/upcxx-intel-mpp3
./configure --prefix=$UPATH --with-cc=mpiicc --with-cxx=mpiicpc --with-mpi-cc=mpiicc --with-mpi-cxx=mpiicpc \
--with-default-network=mpi
make -j16 all
make install


