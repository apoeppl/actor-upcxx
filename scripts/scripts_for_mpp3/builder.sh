#!/bin/bash

#script for building and copying right executables
#~/upcxx-intel

#mpp3 already has the intel compiler that is ok
#module unload intel
#module load intel/19.1
module load gcc/8.3
module load netcdf
module load metis 
module load cmake/3.14.4
module load boost/1.68.0-intel-impi

make clean
rm CMakeCache.txt
rm -r CMakeFiles
rm cmake_install.cmake
rm Makefile

#JOBTYPES is readas an environment variable, the user has to make sure that the needed joytypes are compiled!!!!

#no loop for joytypes here because every job type will have a different set of arguments so, copy by yourself
cmake . -DCMAKE_C_COMPILER=mpiicc -DCMAKE_CXX_COMPILER=mpiicpc -DCMAKE_PREFIX_PATH=${UPCXX_INSTALL} -DENABLE_FILE_OUTPUT=OFF -DBUILD_RELEASE=ON \
-DENABLE_LOGGING=OFF -DENABLE_O3_UPCXX_BACKEND=ON -DENABLE_PARALLEL_UPCXX_BACKEND=OFF -DENABLE_MEMORY_SANITATION=OFF -DIS_CROSS_COMPILING=OFF \
-DPRINT=OFF -DINVASION=OFF -DRANKMIG=OFF -DTIME=OFF -DANALYZE=OFF -DTRACE=OFF -DMIGRATION=0 -DREPLICATION=OFF -DREPORT_MAIN_ACTIONS=OFF

make actorlib -j 16
make pond -j 16
#pond-* should be same as pond-${jobtypes[@]} that is an environment variable
mv pond pond-plain-task

#no loop for joytypes here because every job type will have a different set of arguments so, copy by yourself
cmake . -DCMAKE_C_COMPILER=mpiicc -DCMAKE_CXX_COMPILER=mpiicpc -DCMAKE_PREFIX_PATH=${UPCXX_INSTALL} -DENABLE_FILE_OUTPUT=OFF -DBUILD_RELEASE=ON \
-DENABLE_LOGGING=OFF -DENABLE_O3_UPCXX_BACKEND=ON -DENABLE_PARALLEL_UPCXX_BACKEND=OFF -DENABLE_MEMORY_SANITATION=OFF -DIS_CROSS_COMPILING=OFF \
-DPRINT=OFF -DINVASION=ON -DRANKMIG=OFF -DTIME=OFF -DANALYZE=OFF -DTRACE=OFF -DMIGRATION=2 -DREPLICATION=OFF -DREPORT_MAIN_ACTIONS=OFF

make actorlib -j 16
make pond -j 16
#pond-* should be same as pond-${jobtypes[@]} that is an environment variable
mv pond pond-static-imbalance-async-migration

