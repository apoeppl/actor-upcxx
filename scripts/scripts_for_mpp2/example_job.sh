#!/bin/bash
#SBATCH -J ttt
#SBATCH -o ./%x.%j.out
#SBATCH -e ./%x.%j.err
#Initial working directory (also --chdir):
#SBATCH -D ./
#Notification and type
#SBATCH --mail-type=end,fail,timeout
#SBATCH --mail-user=yakup.paradox@gmail.com
# Wall clock limit:
#SBATCH --time=01:30:00
#SBATCH --no-requeue
#Setup of execution environment
#SBATCH --export=NONE
#SBATCH --get-user-env
#SBATCH --clusters=cm2_tiny
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=28

module load slurm_setup
module load intel
module load mpi.intel
module load netcdf-hdf5-all/4.7_hdf5-1.10-intel19-serial
module load metis/5.1.0-intel19-i64-r64
module load itac

export UPCXX_INSTALL=~/upcxx-intel
export PATH=$PATH:~/upcxx-intel/bin
export GASNET_PHYSMEM_MAX='32 GB'
export GASNET_BACKTRACE=1
export OMP_NUM_THREADS=2
export MLX5_SINGLE_THREADED=0

#41 GB is the max number in mpp2 (both _inter and _tiny)

upcxx-run -n 56 -N 2 -shared-heap 1024MB ./pond -x 8000 -y 8000 -p 250 -c 10 --scenario 2 -o ${SCRATCH}/work/out -e 1
