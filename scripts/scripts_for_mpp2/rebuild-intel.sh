#!/bin/bash

module load intel
module load intel-mpi
module load netcdf-hdf5-all
module load metis/5.1.0-intel19-i64-r64 
module load cmake
module load itac
module load boost 

make clean

rm CMakeCache.txt
rm -r CMakeFiles
rm cmake_install.cmake
rm Makefile

export UPCXX_INSTALL=~/upcxx-intel

cmake . -DCMAKE_CXX_COMPILER=mpiicpc -DCMAKE_C_COMPILER=mpiicc -DCMAKE_PREFIX_PATH=$UPCXX_INSTALL
make actorlib -j 8
make pond -j 8