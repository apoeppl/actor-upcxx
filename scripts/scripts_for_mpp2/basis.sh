#!/bin/bash
#SBATCH -J ${str}
#SBATCH -o ${SCRATCH}/${workdir}/${str}/%x.%j.out
#SBATCH -e ${SCRATCH}/${workdir}/${str}/%x.%j.err
#SBATCH -D ${pwd}
#Notification and type
#SBATCH --mail-type=end,fail,timeout
#SBATCH --mail-user=yakup.paradox@gmail.com
# Wall clock limit:
#SBATCH --time=${hours}:${mins}:${secs}
#SBATCH --no-requeue
#Setup of execution environment
#SBATCH --export=NONE
#SBATCH --get-user-env
#SBATCH --clusters=${cluster}
#SBATCH --partition=${partition}
#SBATCH --qos=${qos}
#SBATCH --nodes=${nodecount}
#SBATCH --ntasks-per-node=${corecountpernode}

module load slurm_setup
module load intel
module load intel-mpi/2019-intel
module load netcdf-hdf5-all/4.7_hdf5-1.10-intel19-serial
module load metis/5.1.0-intel19-i64-r64

export UPCXX_INSTALL=~/upcxx-intel
export PATH=\$PATH:~/upcxx-intel/bin
export GASNET_PHYSMEM_MAX='32 GB'
#41 GB is the max number in mpp2 (both _inter and _tiny)

upcxx-run -n ${corecountforjob} -N ${nodecount} -shared-heap 512MB ./pond-${job} -x ${size} -y ${size} -p ${patchsize} -c 10 --scenario 2 -o ${SCRATCH}/${workdir}/${str}/out/out -e ${endtime}

cat > ${SCRATCH}/${workdir}/${str}/finished.txt