#!/bin/bash

module unload cmake
module unload intel-mpi
module unload intel
module load cmake/3.16.5
module load boost/1.75.0-intel19
module load intel/20.0
module load intel-mpi/2019-intel
module load netcdf-hdf5-all 
module load metis/5.1.0-intel19-i64-r64 

export PATH=$PATH:/dss/dsshome1/lxc05/ge69xij2/upcxx-intel/bin
export GASNET_PHYSMEM_MAX='41 GB'
export GASNET_MAX_SEGSIZE="512MB/P"
export UPCXX_INSTALL="/dss/dsshome1/lxc05/ge69xij2/upcxx-intel"
export UPCXX_SHARED_HEAP_SIZE="512 MB"


#when compiled with intel-mpi
/dss/dsshome1/lrz/sys/spack/staging/20.1.1/opt/x86_64/intel-mpi/2019.7.217-gcc-vf2p2hg/compilers_and_libraries_2020.1.217/linux/mpi/intel64/bin/mpirun \
-np 56 -ppn 28 \
/dss/dsshome1/lxc05/ge69xij2/actor-upcxx/./pond \
-x 8000 -y 8000 -p 1000 -c 10 --scenario 3 \
-o /gpfs/scratch/pr63so/ge69xij2/out/NetCdf -e 0.1 
