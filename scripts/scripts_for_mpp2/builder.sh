#!/bin/bash

#script for building and copying right executables
#~/upcxx-intel

module unload cmake
module unload intel-mpi
module unload intel
module unload boost
module load cmake/3.16.5
module load boost/1.75.0-intel19
module load intel/20.0
module load intel-mpi/2019-intel
module load netcdf-hdf5-all 
module load metis/5.1.0-intel19-i64-r64 

make clean
rm CMakeCache.txt
rm -r CMakeFiles
rm cmake_install.cmake
rm Makefile

#JOBTYPES is readas an environment variable, the user has to make sure that the needed joytypes are compiled!!!!

#no loop for joytypes here because every job type will have a different set of arguments so, copy by yourself

#no loop for joytypes here because every job type will have a different set of arguments so, copy by yourself
cmake . -DCMAKE_C_COMPILER=mpiicc -DCMAKE_CXX_COMPILER=mpiicpc -DCMAKE_PREFIX_PATH=${UPCXX_INSTALL} -DENABLE_FILE_OUTPUT=OFF -DBUILD_RELEASE=ON \
-DENABLE_LOGGING=OFF -DENABLE_O3_UPCXX_BACKEND=ON -DENABLE_PARALLEL_UPCXX_BACKEND=OFF -DENABLE_MEMORY_SANITATION=OFF -DIS_CROSS_COMPILING=OFF \
-DPRINT=OFF -DINVASION=ON -DRANKMIG=OFF -DTIME=OFF -DANALYZE=OFF -DTRACE=OFF -DMIGRATION=2 -DREPLICATION=OFF -DREPORT_MAIN_ACTIONS=OFF \
-DCENTRALIZED_MIGRATION=OFF -DGLOBAL_MIGRATION=ON -DCHAOS=ON -DLAZY_ACTIVATION=ON -DVARYING_JOB=OFF

make actorlib -j 8
make pond -j 8
#pond-* should be same as pond-${jobtypes[@]} that is an environment variable
mv pond pond-expansion
