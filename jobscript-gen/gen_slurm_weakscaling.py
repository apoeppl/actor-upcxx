import argparse
from math import *


myHomeDir = '/global/homes/a/apoeppl/'

pondRaw = """#!/bin/bash
#SBATCH -N {numNodes}
#SBATCH -q regular
#SBATCH -J Pond-{executionType}-WS-{numNodes}
#SBATCH -C knl
#SBATCH --mail-user=poeppl@in.tum.de
#SBATCH --mail-type=ALL
#SBATCH -t 00:30:00

#OpenMP settings:
export OMP_NUM_THREADS={coresPerRank}
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

#run the application:
srun -n {numRanks} -c {coresPerRank} --cpu-bind=threads {base}/{executableDir}/pond -x {xSize} -y {ySize} -p {patchSize} -e {end} -c 1 --scenario 2 -o out > {base}/{logPrefix}/{executionTypeLog}/ws-{numNodes:04d}.txt"""

swex10Raw = """#!/bin/bash
#SBATCH -N {numNodes}
#SBATCH -q regular
#SBATCH -J SWE-X10-WS-{numNodes}
#SBATCH --mail-user=poeppl@in.tum.de
#SBATCH -C knl
#SBATCH --mail-type=ALL
#SBATCH -t 01:00:00

export X10_NTHREADS={numThreads}

#run the application:
srun -n {numRanks} -c {coresPerRank} --cpu_bind=cores {base}/{executableDir}/Main -x {xSize} -y {ySize} -ps {patchSize} -write false -solver n_hlle -end {end} -ds false -ts fixed -sc 6 > {base}/{logPrefix}/swex10/ws-{numNodes:04d}.txt"""

sweRaw = """#!/bin/bash
#SBATCH -N {numNodes}
#SBATCH -q regular
#SBATCH -J SWE-MPI-WS-{numNodes}
#SBATCH -C knl
#SBATCH --mail-user=poeppl@in.tum.de
#SBATCH --mail-type=ALL
#SBATCH -t 00:30:00

#OpenMP settings:
export OMP_NUM_THREADS={coresPerRank}
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

#run the application:
srun -n {numRanks} -c {coresPerRank} --cpu-bind=threads {base}/{executableDir}/{executableName} -x {xSize} -y {ySize} -e {end} -c 1 -o out > {base}/{logPrefix}/{executionTypeLog}/ws-{numNodes:04d}.txt"""

end = 1
coresPerNode = 256
patchSize = 256
basePath = "/global/homes/a/apoeppl"


def getXSize(coresPerRank, ranksPerNode, timesDoubled):
    patchSizeMultiplier = timesDoubled
    patchesPerNode = ranksPerNode * coresPerRank
    ppnExp = (patchesPerNode - 1).bit_length()
    patchSizeMultiplier += ppnExp
    patchSizeMultiplier /= 2
    patchSizeMultiplier = 2 ** int(patchSizeMultiplier)
    xSize = patchSize * patchSizeMultiplier
    return int(xSize)

def getYSize(coresPerRank, ranksPerNode, timesDoubled):
    patchSizeMultiplier = timesDoubled
    patchesPerNode = ranksPerNode * coresPerRank
    ppnExp = (patchesPerNode - 1).bit_length()
    patchSizeMultiplier += ppnExp
    if patchSizeMultiplier % 2 == 1:
        patchSizeMultiplier += 1
    patchSizeMultiplier /= 2
    patchSizeMultiplier = 2 ** patchSizeMultiplier
    ySize = patchSize * patchSizeMultiplier
    return int(ySize)

def getEndTime(timesDoubled):
    return 2 ** -int(timesDoubled/2)

def generateSwex10(timesDoubled, logPrefix):
    patchesPerCore = 1
    ranksPerNode = 4
    numNodes = 2 ** timesDoubled
    coresPerRank = 64
    xSize = getXSize(coresPerRank, ranksPerNode, timesDoubled)
    ySize = getYSize(coresPerRank, ranksPerNode, timesDoubled)
    endTime = getEndTime(timesDoubled)
    slurmScript = swex10Raw.format(
            numNodes = (2 ** timesDoubled),
            numThreads = 1 * coresPerRank,
            xSize = xSize,
            ySize = ySize,
            coresPerRank = coresPerRank,
            base = basePath,
            logPrefix = logPrefix,
            end = endTime,
            numRanks = ranksPerNode * numNodes,
            patchSize = int(patchSize / sqrt(patchesPerCore)),
            executableDir = "swex10/swex10/bin"
            )
    return slurmScript

def generateSwe(timesDoubled, logPrefix):
    ranksPerNode = 1
    numNodes = 2 ** timesDoubled
    coresPerRank = 272
    xSize = getXSize(256, ranksPerNode, timesDoubled)
    ySize = getYSize(256, ranksPerNode, timesDoubled)
    endTime = getEndTime(timesDoubled)
    slurmScript = sweRaw.format(
            numNodes = (2 ** timesDoubled),
            xSize = xSize,
            ySize = ySize,
            coresPerRank = coresPerRank,
            base = basePath,
            logPrefix = logPrefix,
            end = endTime,
            numRanks = ranksPerNode * numNodes,
            executableDir = "SWE/build",
            executableName = "SWE_cray_release_mpi_hlle_vec",
            executionTypeLog = "swe"
            )
    return slurmScript

def generatePondPar(timesDoubled, logPrefix):
    patchesPerCore = 1
    ranksPerNode = 2
    numNodes = 2 ** timesDoubled
    coresPerRank = 128
    xSize = getXSize(coresPerRank, ranksPerNode, timesDoubled)
    ySize = getYSize(coresPerRank, ranksPerNode, timesDoubled)
    endTime = getEndTime(timesDoubled)
    slurmScript = pondRaw.format(
            numNodes = (2 ** timesDoubled),
            xSize = xSize,
            ySize = ySize,
            coresPerRank = coresPerRank,
            base = basePath,
            logPrefix = logPrefix,
            end = endTime,
            numRanks = ranksPerNode * numNodes,
            patchSize = int(patchSize / sqrt(patchesPerCore)),
            executableDir = "actor-upcxx/build/coriknl_par_release_nowrite",
            executionType = "PAR",
            executionTypeLog = "pond_par"
            )
    return slurmScript

def generatePondTasks(timesDoubled, logPrefix):
    patchesPerCore = 4
    ranksPerNode = 2
    numNodes = 2 ** timesDoubled
    coresPerRank = 136
    xSize = getXSize(128, ranksPerNode, timesDoubled)
    ySize = getYSize(128, ranksPerNode, timesDoubled)
    endTime = getEndTime(timesDoubled)
    slurmScript = pondRaw.format(
            numNodes = (2 ** timesDoubled),
            xSize = xSize,
            ySize = ySize,
            coresPerRank = coresPerRank,
            base = basePath,
            logPrefix = logPrefix,
            end = endTime,
            numRanks = ranksPerNode * numNodes,
            patchSize = int(patchSize / sqrt(patchesPerCore)),
            executableDir = "actor-upcxx/build/coriknl_tasks_release_nowrite",
            executionType = "TASKS",
            executionTypeLog = "pond_tasks"
            )
    return slurmScript

def generatePondTasks16PerNode(timesDoubled, logPrefix):
    patchesPerCore = 4
    ranksPerNode = 16
    numNodes = 2 ** timesDoubled
    coresPerRank = 17
    xSize = getXSize(16, ranksPerNode, timesDoubled)
    ySize = getYSize(16, ranksPerNode, timesDoubled)
    endTime = getEndTime(timesDoubled)
    slurmScript = pondRaw.format(
            numNodes = (2 ** timesDoubled),
            xSize = xSize,
            ySize = ySize,
            coresPerRank = coresPerRank,
            base = basePath,
            logPrefix = logPrefix,
            end = endTime,
            numRanks = ranksPerNode * numNodes,
            patchSize = int(patchSize / sqrt(patchesPerCore)),
            executableDir = "actor-upcxx/build/coriknl_tasks_release_nowrite",
            executionType = "TASKS",
            executionTypeLog = "pond_tasks_16_ranks_per_node"
            )
    return slurmScript

def generatePondSeq(timesDoubled, logPrefix):
    patchesPerCore = 4
    ranksPerNode = 128
    numNodes = 2**timesDoubled
    coresPerRank = 2
    xSize = getXSize(coresPerRank, ranksPerNode, timesDoubled)
    ySize = getYSize(coresPerRank, ranksPerNode, timesDoubled)
    endTime = getEndTime(timesDoubled)
    slurmScript = pondRaw.format(
            numNodes=(2**timesDoubled),
            xSize=xSize,
            ySize=ySize,
            coresPerRank=coresPerRank,
            base=basePath,
            logPrefix=logPrefix,
            end=endTime,
            numRanks=ranksPerNode * numNodes,
            patchSize = int(patchSize / sqrt(patchesPerCore)),
            executableDir = "actor-upcxx/build/coriknl_seq_release_nowrite",
            executionType = "SEQ",
            executionTypeLog = "pond_seq"
            )
    return slurmScript

def application_type(arg):
    if arg == "swex10":
        return 0
    elif arg == "pond_seq":
        return 1
    elif arg == "pond_par":
        return 2
    elif arg == "pond_tasks":
        return 3
    elif arg == "pond_tasks16":
        return 4
    elif arg == "swe":
        return 5
    else:
        raise argparse.ArgumentError("Application type must be either swex10 (for SWE-X10), pond_seq (for Pond with sequential UPC++ backend), pond_tasks (for Pond with OpenMP tasks backend) or pond_par (for Pond with parallel UPC++ backend).")

def valid_node_number(arg):
    value = int(arg)
    for i in range(0, 20):
        if value == 2 ** i:
            return i
    raise argparse.ArgumentError("Number of nodes must be an even 2^x, e.g. 1, 2, 4, 8, 16, ...")

parser = argparse.ArgumentParser(description='Generate SLURM Scripts for Scaling tests of Pond & SWE-X10 on Cori')
parser.add_argument("--nodes", dest='nodes', type=valid_node_number, default=0, help="Indicate how many nodes the problem should be run on. Must be an even 2^x, e.g. 1, 2, 4, 8, 16, ...")
parser.add_argument("--app", dest='application', type=application_type, default=0, help="Indicate the application the script needs to be generated for. Possible values are: swex10 (for SWE-X10), pond_seq (for Pond with sequential UPC++ backend), or pond_par (for Pond with parallel UPC++ backend).")
parser.add_argument("--log-prefix", dest='logPrefix', default='misc', help="Indicate the prefix for the test. Logs are placed in <basePath>/prefix/<APPLICATION>/ws-x.txt")
args = parser.parse_args()

app = args.application
nodes = args.nodes
logPrefix = args.logPrefix

if app == 0:
    print(generateSwex10(nodes, logPrefix))
elif app == 1:
    print(generatePondSeq(nodes,logPrefix))
elif app == 2:
    print(generatePondPar(nodes,logPrefix))
elif app == 3:
    print(generatePondTasks(nodes, logPrefix))
elif app == 4:
    print(generatePondTasks16PerNode(nodes, logPrefix))
elif app == 5:
    print(generateSwe(nodes, logPrefix))
