import argparse
from math import *


myHomeDir = '/global/homes/a/apoeppl/'

pondRaw = """#!/bin/bash
#SBATCH -N {numNodes}
#SBATCH -q regular
#SBATCH -J Pond-{executionType}-SS-{numNodes}
#SBATCH -C knl
#SBATCH --mail-user=poeppl@in.tum.de
#SBATCH --mail-type=ALL
#SBATCH -t 00:30:00

#OpenMP settings:
export OMP_NUM_THREADS={coresPerRank}
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

#run the application:
srun -n {numRanks} -c {coresPerRank} --cpu-bind=threads {base}/{executableDir}/pond -x {xSize} -y {ySize} -p {patchSize} -e {end} -c 1 --scenario 2 -o out > {base}/{logPrefix}/{executionTypeLog}/ss-{numNodes:04d}.txt"""

pondRaw = """#!/bin/bash
#SBATCH -N {numNodes}
#SBATCH -q regular
#SBATCH -J SWE-MPI-SS-{numNodes}
#SBATCH -C knl
#SBATCH --mail-user=poeppl@in.tum.de
#SBATCH --mail-type=ALL
#SBATCH -t 00:30:00

#OpenMP settings:
export OMP_NUM_THREADS={coresPerRank}
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

#run the application:
srun -n {numRanks} -c {coresPerRank} --cpu-bind=threads {base}/{executableDir}/{executableName} -x {xSize} -y {ySize} -e {end} -c 1 -o out > {base}/{logPrefix}/{executionTypeLog}/ss-{numNodes:04d}.txt"""

swex10Raw = """#!/bin/bash
#SBATCH -N {numNodes}
#SBATCH -q regular
#SBATCH -J SWE-X10-SS-{numNodes}
#SBATCH --mail-user=poeppl@in.tum.de
#SBATCH -C knl
#SBATCH --mail-type=ALL
#SBATCH -t 01:00:00

export X10_NTHREADS={numThreads}

#run the application:
srun -n {numRanks} -c {coresPerRank} --cpu_bind=cores {base}/{executableDir}/Main -x {xSize} -y {ySize} -ps {patchSize} -write false -solver n_hlle -end {end} -ds false -ts fixed -sc 6 > {base}/{logPrefix}/swex10/ss-{numNodes:04d}.txt"""

end = 1
baseSize = 16384
coresPerNode = 256
basePath = "/global/homes/a/apoeppl"

def getPatchSize(patchesPerRank, ranksPerNode, timesDoubled):
    patchSizeDivisor = timesDoubled
    patchesPerNode = ranksPerNode * patchesPerRank
    ppnExp = (patchesPerNode-1).bit_length()
    patchSizeDivisor += ppnExp
    if patchSizeDivisor % 2 == 1:
        patchSizeDivisor += 1
    patchSizeDivisor /= 2
    patchSizeDivisor = 2 ** patchSizeDivisor
    patchSize = baseSize / patchSizeDivisor
    return int(patchSize)

def generateSwex10(timesDoubled, logPrefix):
    patchesPerRank = 64
    ranksPerNode = 4
    numNodes = 2 ** timesDoubled
    coresPerRank = 64
    patchSize = getPatchSize(patchesPerRank, ranksPerNode, timesDoubled)
    slurmScript = swex10Raw.format(
            numNodes = (2 ** timesDoubled),
            numThreads = 1 * coresPerRank,
            xSize = baseSize,
            ySize = baseSize,
            coresPerRank = coresPerRank,
            base = basePath,
            logPrefix = logPrefix,
            end = end,
            numRanks = ranksPerNode * numNodes,
            patchSize = patchSize,
            executableDir = "swex10/swex10/bin"
            )
    return slurmScript

def generateSwe(timesDoubled, logPrefix):
    ranksPerNode = 1
    numNodes = 2 ** timesDoubled
    coresPerRank = 272
    slurmScript = pondRaw.format(
            numNodes = (2 ** timesDoubled),
            xSize = baseSize,
            ySize = baseSize,
            coresPerRank = coresPerRank,
            base = basePath,
            logPrefix = logPrefix,
            end = end,
            numRanks = ranksPerNode * numNodes,
            executableDir = "SWE/build",
            executableName = "SWE_cray_release_mpi_hlle_vec",
            executionTypeLog = "swe"
            )
    return slurmScript

def generatePondPar(timesDoubled, logPrefix):
    patchesPerRank = 128
    ranksPerNode = 2
    numNodes = 2 ** timesDoubled
    coresPerRank = 128
    patchSize = getPatchSize(patchesPerRank, ranksPerNode, timesDoubled)
    slurmScript = pondRaw.format(
            numNodes = (2 ** timesDoubled),
            xSize = baseSize,
            ySize = baseSize,
            coresPerRank = coresPerRank,
            base = basePath,
            logPrefix = logPrefix,
            end = end,
            numRanks = ranksPerNode * numNodes,
            patchSize = patchSize,
            executableDir = "actor-upcxx/build/coriknl_par_release_nowrite",
            executionType = "PAR",
            executionTypeLog = "pond_par"
            )
    return slurmScript

def generatePondTasks(timesDoubled, logPrefix, tasksPerNode):
    patchesPerRank = 272 / tasksPerNode
    ranksPerNode = tasksPerNode
    numNodes = 2 ** timesDoubled
    coresPerRank = patchesPerRank
    patchSize = getPatchSize(patchesPerRank, ranksPerNode, timesDoubled)
    slurmScript = pondRaw.format(
            numNodes = (2 ** timesDoubled),
            xSize = baseSize,
            ySize = baseSize,
            coresPerRank = coresPerRank,
            base = basePath,
            logPrefix = logPrefix,
            end = end,
            numRanks = ranksPerNode * numNodes,
            patchSize = patchSize,
            executableDir = "actor-upcxx/build/coriknl_tasks_release_nowrite",
            executionType = "TASKS",
            executionTypeLog = "pond_tasks_" + tasksPerNode + "_ranks_per_node"
            )
    return slurmScript

def generatePondSeq(timesDoubled, logPrefix):
    patchesPerRank = 4
    ranksPerNode = 128
    numNodes = 2**timesDoubled
    coresPerRank = 2
    patchSize = getPatchSize(patchesPerRank, ranksPerNode, timesDoubled)
    slurmScript = pondRaw.format(
            numNodes=(2**timesDoubled),
            xSize=baseSize,
            ySize=baseSize,
            coresPerRank=coresPerRank,
            base=basePath,
            logPrefix=logPrefix,
            end=end,
            numRanks=ranksPerNode * numNodes,
            patchSize=patchSize,
            executableDir = "actor-upcxx/build/coriknl_seq_release_nowrite",
            executionType = "SEQ",
            executionTypeLog = "pond_seq"
            )
    return slurmScript

def application_type(arg):
    if arg == "swex10":
        return 0
    elif arg == "pond_seq":
        return 1
    elif arg == "pond_par":
        return 2
    elif arg == "pond_tasks":
        return 3
    elif arg == "swe":
        return 4
    else:
        raise argparse.ArgumentError("Application type must be either swex10 (for SWE-X10), pond_seq (for Pond with sequential UPC++ backend), pond_tasks (for Pond with OpenMP tasks backend) or pond_par (for Pond with parallel UPC++ backend).")

def valid_node_number(arg):
    value = int(arg)
    for i in range(0, 20):
        if value == 2 ** i:
            return i
    raise argparse.ArgumentError("Number of nodes must be an even 2^x, e.g. 1, 2, 4, 8, 16, ...")

def valid_tpn_number(arg):
    value = int(arg)
    for i in range(0, 4):
        if value == 2 ** i:
            return i
    raise argparse.ArgumentError("Number of nodes must be an even 2^x, e.g. 1, 2, 4, 8, 16, ...")

parser = argparse.ArgumentParser(description='Generate SLURM Scripts for Scaling tests of Pond & SWE-X10 on Cori')
parser.add_argument("--tpn", dest='tasksPerNode', type=valid_tpn_number, default=1, help="Indicate how many tasks per node the problem should be run on. Must be 1, 2, 4, 8 or 16")
parser.add_argument("--nodes", dest='nodes', type=valid_node_number, default=0, help="Indicate how many nodes the problem should be run on. Must be an even 2^x, e.g. 1, 2, 4, 8, 16, ...")
parser.add_argument("--app", dest='application', type=application_type, default=0, help="Indicate the application the script needs to be generated for. Possible values are: swex10 (for SWE-X10), pond_seq (for Pond with sequential UPC++ backend), or pond_par (for Pond with parallel UPC++ backend).")
parser.add_argument("--log-prefix", dest='logPrefix', default='misc', help="Indicate the prefix for the test. Logs are placed in <basePath>/prefix/<APPLICATION>/ws-x.txt")
parser.add_argument("--problem-size", dest='baseSize', default=16384, help="Indicate the problem size for the test. Grid size will be <problem-size> x <problem-size>")
args = parser.parse_args()

app = args.application
nodes = args.nodes
logPrefix = args.logPrefix
tasksPerNode = args.tasksPerNode
baseSize = args.baseSize

if app == 0:
    print(generateSwex10(nodes, logPrefix))
elif app == 1:
    print(generatePondSeq(nodes,logPrefix))
elif app == 2:
    print(generatePondPar(nodes,logPrefix))
elif app == 3:
    print(generatePondTasks(nodes, logPrefix, tasksPerNode))
elif app == 4:
    print(generateSwe(nodes, logPrefix))
