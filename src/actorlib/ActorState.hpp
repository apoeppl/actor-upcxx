#pragma once
#include <string>
#include <unordered_map>

enum class ActorState
{
    Running,
    NeedsActivation,
    TemporaryStoppedForMigration,
    Terminated,
    Transitioning,
    WantsToTerminate
};

class ASPrinter
{
  public:
    ASPrinter() noexcept {}

    std::string as2str(ActorState ar) const
    {
        switch (ar)
        {
        case ActorState::Running:
        {
            return "Running";
        }
        case ActorState::NeedsActivation:
        {
            return "Needs Activation";
        }
        case ActorState::TemporaryStoppedForMigration:
        {
            return "Stopped and will be migrated";
        }
        case ActorState::Terminated:
        {
            return "Temrinated";
        }
        case ActorState::Transitioning:
        {
            return "Transitioning";
        }
        case ActorState::WantsToTerminate:
        {
            return "Wants to terminate";
        }
        default:
        {
            return "Undefined";
        }
        }
    }
};
