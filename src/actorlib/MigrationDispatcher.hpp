#pragma once

#include "AcquisitonResult.hpp"
#include "Utility.hpp"
#include <optional>
#include <string>
#include <upcxx/upcxx.hpp>

// This an empty to give access
// A Base class for Dynamic ActorGraph so that
class MigrationDispatcher
{
  public:
    virtual upcxx::future<GlobalActorRef> stealActorAsync(const std::string &name) = 0;
    virtual std::vector<std::string> findActorsToSteal() = 0;
    virtual std::vector<std::string> findRandomActorsToSteal(size_t count) = 0;
    virtual upcxx::future<AcquisitionResult> tryLockActorForMigration(const std::string &name) = 0;
    virtual upcxx::future<bool> tryStopSelfAndMarkNeighborhood(const std::string &name) = 0;
    virtual std::vector<std::string> findAnActorToStealFromTheCrowdiestRank() = 0;
};