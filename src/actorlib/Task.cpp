#include "Task.hpp"
#include "ActorImpl.hpp"

static ARPrinter arp;
static ASPrinter asp;

Task::Task() noexcept : finished(false), source(), tasktype(TaskType::Unspecified), uniques{}, unique_needed(0) {}

Task::Task(const std::string &source, TaskType tt, size_t unique_needed) noexcept
    : finished(false), source(source), tasktype(tt), uniques{}, unique_needed(unique_needed){};

Task::Task(std::string &&source, TaskType tt, size_t unique_needed) noexcept
    : finished(false), source(std::move(source)), tasktype(tt), uniques{}, unique_needed(unique_needed){};

Task &Task::operator=(Task &&t) noexcept
{
    this->source = std::move(t.source);
    this->finished = t.finished;
    this->tasktype = t.tasktype;
    this->unique_needed = t.unique_needed;
    this->uniques = std::move(t.uniques);
    t.finished = false;
    t.source.clear();
    t.uniques.clear();
    t.unique_needed = 0;
    t.tasktype = TaskType::Unspecified;
    return *this;
}

Task &Task::operator=(const Task &t) noexcept
{
    this->source = t.source;
    this->finished = t.finished;
    this->tasktype = t.tasktype;
    this->unique_needed = t.unique_needed;
    this->uniques = t.uniques;
    return *this;
}

Task::Task(const Task &t) noexcept : finished(t.finished), source(t.source), tasktype(t.tasktype), uniques(t.uniques), unique_needed(t.unique_needed) {}

Task::Task(Task &&t) noexcept
    : finished(t.finished), source(std::move(t.source)), tasktype(t.tasktype), uniques(std::move(t.uniques)), unique_needed(t.unique_needed)
{
    t.finished = false;
    t.source.clear();
    t.uniques.clear();
    t.unique_needed = 0;
}

void Task::reset() noexcept { source.clear(); }

std::string Task::print() const noexcept
{
    std::string s;
    s.reserve(20);
    s.append(source);
    s.append(", finished = ");
    s.append(std::to_string(finished));
    return s;
}

bool Task::none() const noexcept { return this->source.empty(); }

void Task::execute(ActorImpl *ai)
{
#ifndef NDEBUG
    assert(tasktype != TaskType::Unspecified);
#endif

    ActorState as = ai->getRunningState();

    if (as == ActorState::Terminated || (tasktype == TaskType::Act && as == ActorState::WantsToTerminate))
    {
        this->finished = true;
        // std::cout << source << " is terminated not going to act" << std::endl;
        return;
    }

    assert(uniques.size() <= unique_needed);
    if (unique_needed != 0 && uniques.size() != unique_needed)
    {
        // std::cout << source << " unique count is not ready, return early" << std::endl;
        return;
    }

    if (tasktype == TaskType::Act)
    {
        assert(ai != nullptr);
        assert(ai->getName() == this->source);

        switch (as)
        {
        case ActorState::WantsToTerminate:
        case ActorState::Running:
        {
            // std::cout << source << " calls act - running | wants to terminate" << std::endl;
            ai->b_act();
            ai->stealable = true;
            this->finished = true;
            break;
        }
        case ActorState::Terminated:
        {
            // std::cout << source << " calls act - terminated" << std::endl;
            ai->b_act();
            ai->stealable = true;
            assert(ai->buffersEmpty());
            assert(ai->noNeedToNotify());
            this->finished = true;
            break;
        }
        default:
            // std::cout << source << " non handled case:" << asp.as2str(as) << std::endl;
            break;
        }
    }
    else if (tasktype == TaskType::Flush)
    {
        assert(ai != nullptr);
        assert(ai->getName() == this->source);

        switch (as)
        {
        case ActorState::Terminated:
        {
            // std::cout << source << " calls flush - terminated" << std::endl;
            assert(ai->buffersEmpty());
            assert(ai->noNeedToNotify());
            this->finished = true;
            break;
        }
        case ActorState::Running:
        case ActorState::WantsToTerminate:
        {
            // std::cout << source << " calls flush - running | wants to terminate" << std::endl;
            ai->flushBuffers();
            if (ai->buffersEmpty())
            {
                this->finished = true;
            }
            break;
        }
        default:
            // std::cout << source << " non handled case:" << asp.as2str(as) << std::endl;
            break;
        }
    }
    else
    {
        assert(ai != nullptr);
        assert(ai->getName() == this->source);

        switch (as)
        {
        case ActorState::Terminated:
        {
            // std::cout << source << " calls notify - running | wants to terminate" << std::endl;
            assert(ai->buffersEmpty());
            assert(ai->noNeedToNotify());
            this->finished = true;
            break;
        }
        case ActorState::Running:
        case ActorState::WantsToTerminate:
        {
            // std::cout << source << " calls notify - running | wants to terminate" << std::endl;
            ai->sendNotifications();
            if (ai->noNeedToNotify())
            {
                this->finished = true;
            }
            break;
        }
        default:
            // std::cout << source << " non handled case:" << asp.as2str(as) << std::endl;
            break;
        }
    }
}
