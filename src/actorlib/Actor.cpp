#include "Actor.hpp"

ActorImpl &Actor::asBase() { return static_cast<ActorImpl &>(*this); }

const ActorImpl &Actor::asConstBase() const { return static_cast<const ActorImpl &>(*this); }

void Actor::b_act() { ActorImpl::b_act(); }

// bool Actor::inActableState() const { return ActorImpl::inActableState(); }

void Actor::prepare() { ActorImpl::prepare(); }

std::string Actor::getName() const { return ActorImpl::getName(); }

void Actor::copyPortsInformation(const Actor &other)
{
    const ActorImpl *tmpref = &other.asConstBase();
    ActorImpl::copyPortsInformation(*tmpref);
}

void Actor::movePortsInformation(Actor &&other) { ActorImpl::movePortsInformation(std::move(other.asBase())); }

void Actor::stop() { ActorImpl::stop(); }

Actor::Actor(std::string const &name) noexcept : ActorImpl(name) {}

Actor::Actor(std::string &&name) noexcept : ActorImpl(std::move(name)) {}

Actor::Actor(Actor const &other) noexcept : ActorImpl(other.asConstBase()) {}

Actor::Actor(Actor &&other) noexcept : ActorImpl(std::move(other.asBase())) {}

Actor::Actor(ActorData &&data) noexcept : ActorImpl(std::move(data)) {}

Actor::Actor(ActorImpl &&other) noexcept : ActorImpl(std::move(other)) {}

Actor::Actor(ActorImpl const &other) noexcept : ActorImpl(other) {}

Actor::Actor(ActorData &&data,
             std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> &&portsInformation) noexcept
    : ActorImpl(std::move(data), std::move(portsInformation))
{
}
