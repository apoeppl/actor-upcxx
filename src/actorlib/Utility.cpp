#include "Utility.hpp"

uint64_t util::calcMeanOverhead()
{
    uint64_t totaloverhead = 0;
    for (int i = 0; i < 10000; i++)
    {
        auto beg = std::chrono::high_resolution_clock::now();
        auto end = std::chrono::high_resolution_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - beg).count();
        totaloverhead += elapsed;
    }
    double mean = (double)totaloverhead / 10000.0;
    return (uint64_t)mean;
}

float util::nanoToSec(uint64_t val) { return (float)val / 1000000000.0; }

std::pair<bool, uint64_t> util::addWOverflow(uint64_t a, uint64_t b)
{
    if (a <= std::numeric_limits<uint64_t>::max() - b)
    {
        return std::make_pair(true, a + b);
    }
    else
    {
        long long neg = b - std::numeric_limits<uint64_t>::max();
        neg += a;
        uint64_t uval = neg;
        return std::make_pair(true, uval);
    }
}

upcxx::future<> util::combineFutures(const std::vector<upcxx::future<>> &futs)
{
    upcxx::future<> combined;
    if (futs.size() == 0)
    {
        combined = upcxx::make_future();
    }
    else if (futs.size() == 1)
    {
        combined = futs[0];
    }
    else if (futs.size() == 2)
    {
        combined = upcxx::when_all(futs[0], futs[1]);
    }
    else
    {
        combined = upcxx::when_all(futs[0], futs[1]);
        for (size_t i = 2; i < futs.size(); i++)
        {
            combined = upcxx::when_all(combined, futs[2]);
        }
    }
    return combined;
}

void util::printConnectionTuple(const std::tuple<std::string, std::string, std::string, std::string> &tup)
{
    std::string s = std::get<0>(tup) + " : " + std::get<1>(tup) + " -> " + std::get<2>(tup) + " : " + std::get<3>(tup);
    std::cout << s << std::endl;
}

void util::printConnectionTuple(std::tuple<std::string, std::string, std::string, std::string> &&tup)
{
    std::string s =
        std::move(std::get<0>(tup)) + " : " + std::move(std::get<1>(tup)) + " -> " + std::move(std::get<2>(tup)) + " : " + std::move(std::get<3>(tup));
    std::cout << s << std::endl;
}

std::string util::migrationList2Str(int rank, const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    std::stringstream ss;
    ss << "Rank-" << rank << ": ";
    for (auto &el : migList)
    {
        ss << "( " << el.first << " -> " << el.second << " ) ";
    }

    return ss.str();
}

#include "PortInformation.hpp"

void util::freePortsInformation(std::tuple<int, InPortInformationBase **, int, OutPortInformationBase **> &portsInformation)
{
    int ipoCount = std::get<0>(portsInformation);
    InPortInformationBase **ipoInfos = std::get<1>(portsInformation);
    if (ipoInfos != nullptr)
    {
        for (int i = 0; i < ipoCount; i++)
        {
            delete ipoInfos[i];
            ipoInfos[i] = nullptr;
        }
        delete[] ipoInfos;
        ipoInfos = nullptr;
    }
    std::get<0>(portsInformation) = 0;

    int opoCount = std::get<2>(portsInformation);
    OutPortInformationBase **opoInfos = std::get<3>(portsInformation);
    if (opoInfos != nullptr)
    {
        for (int i = 0; i < opoCount; i++)
        {
            delete opoInfos[i];
            opoInfos[i] = nullptr;
        }
        delete[] opoInfos;
        opoInfos = nullptr;
    }
    std::get<2>(portsInformation) = 0;
}

upcxx::intrank_t util::rank_n() { return upcxx::rank_n(); }

int util::intPow(int x, unsigned int p)
{
    if (p == 0)
        return 1;
    if (p == 1)
        return x;

    int tmp = intPow(x, p / 2);
    if (p % 2 == 0)
        return tmp * tmp;
    else
        return x * tmp * tmp;
}