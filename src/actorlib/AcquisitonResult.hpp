#pragma once
#include <string>
#include <unordered_map>

/*d::d
    Intenal usage, needed for Migration it indicates whether the actor could be locked,
    Terminated means it was teminated because the actor reached the terminated state,
    and it was not stopped form the ActoGraph before ther Termination
*/

enum class AcquisitionResult
{
    Ok,
    LostLockingRace,
    ActorTerminated,
    ActorBeingMigrated,
    UnmigrateableNeighbor,
    RankMigrationOnLimit,
    HasNoTasks,
    NotEnoughActors
};

class ARPrinter
{
  private:
    const std::unordered_map<AcquisitionResult, std::string> mp = {{AcquisitionResult::Ok, "Ok"},
                                                                   {AcquisitionResult::LostLockingRace, "Lost Locking Race"},
                                                                   {AcquisitionResult::ActorTerminated, "Terminated"},
                                                                   {AcquisitionResult::ActorBeingMigrated, "Being Migrationg"},
                                                                   {AcquisitionResult::UnmigrateableNeighbor, "Unmigrateable"},
                                                                   {AcquisitionResult::RankMigrationOnLimit, "The rank is already migrating too many actors"},
                                                                   {AcquisitionResult::HasNoTasks, "Has 0 Tasks"},
                                                                   {AcquisitionResult::NotEnoughActors, "Has only one Actor"}};

  public:
    std::string ar2str(AcquisitionResult ar) { return mp.find(ar)->second; }
};
