#pragma once
#include <chrono>
#include <iostream>
#include <limits>
#include <optional>
#include <sstream>
#include <string>
#include <tuple>
#include <upcxx/upcxx.hpp>
#include <utility>

class InPortInformationBase;
class OutPortInformationBase;

/*
    Utility functions and additional definitions to help calls in library
*/

enum TaskType
{
    Act,
    Flush,
    Notify,
    Unspecified
};

// std::cout << for std::optionals
template <class T> constexpr std::ostream &operator<<(std::ostream &os, std::optional<T> const &opt)
{
    if (opt.has_value())
    {
        os << *opt;
    }
    else
    {
        os << "{}";
    }
    return os;
}

namespace util
{

// checks for overflow, complains if there is an overflow
template <class T> T addNoOverflow(const T a, const T b)
{
    /*
    #ifdef PRINT
    std::cout << a << " + " << b << " = " << a+b << std::endl;
    #endif
    */
    assert(a <= std::numeric_limits<T>::max() - b);
    return a + b;
}

// return true if it overflows
template <class T> bool addOverflows(const T a, const T b)
{
    if (a <= std::numeric_limits<T>::max() - b)
    {
        return false;
    }
    else
    {
        return true;
    }
}

// prints std::optional<T>
template <typename T> std::string optional2Str(const std::optional<T> &opt)
{
    if (opt.has_value())
    {
        return std::to_string(*opt);
    }
    else
    {
        return "None";
    }
}

// adds a and b with overflow
std::pair<bool, uint64_t> addWOverflow(uint64_t a, uint64_t b);

// tries to approximate overhead caused by measuring time
uint64_t calcMeanOverhead();

// converts saved nanoseconds to seconds
float nanoToSec(uint64_t nano);

// combines the vector of futures and wait for all of them
upcxx::future<> combineFutures(const std::vector<upcxx::future<>> &futs);

// prints a connection document
void printConnectionTuple(const std::tuple<std::string, std::string, std::string, std::string> &tup);

// print the connection tuple but rvalue
void printConnectionTuple(std::tuple<std::string, std::string, std::string, std::string> &&tup);

// makes a string of migratio list (needs rank too)
std::string migrationList2Str(int rank, const std::unordered_map<std::string, upcxx::intrank_t> &migList);

void freePortsInformation(std::tuple<int, InPortInformationBase **, int, OutPortInformationBase **> &portsInformation);

// creates a string of vector of pairs
template <typename A, typename B> std::string vector2Str(const std::vector<std::pair<A, B>> &vec)
{
    std::stringstream ss;
    ss << "{ ";
    for (auto &el : vec)
    {
        ss << "(" << el.first << ", " << el.second << ")";
    }
    ss << "}";

    return ss.str();
}

// creates a string of type T
template <typename T> std::string vector2Str(const std::vector<T> &vec)
{
    std::stringstream ss;
    ss << "{ ";
    for (auto &el : vec)
    {
        ss << el << " ";
    }
    ss << "}";

    return ss.str();
}

// creates a string of type map T,U
template <typename T, typename U> std::string map2Str(const std::unordered_map<T, U> &map)
{
    {
        std::stringstream ss;
        ss << "{";
        for (auto &el : map)
        {
            ss << "(" << el.first << " -> " << el.second << ")";
        }
        ss << "}";

        return ss.str();
    }
}

// compile time variadic template lest length function
template <class I> constexpr size_t counter() { return 1; }

template <class I, class J, class... Is> constexpr size_t counter() { return 1 + counter<J, Is...>(); } // compile time variadic template lest length function

template <class I, class J, class... Is> constexpr bool multiple() { return true; } // compile time variadic template lest length >=1 function

template <class I> constexpr bool multiple() { return false; } // compile time variadic template lest length >= 1 function

template <class Reader, class TypeToRead> upcxx::deserialized_type_t<TypeToRead> *read_diff_types(Reader &reader)
{
    auto adstore = new typename std::aligned_storage<sizeof(upcxx::deserialized_type_t<TypeToRead>), alignof(upcxx::deserialized_type_t<TypeToRead>)>::type;
    reader.template read_into<TypeToRead>(adstore);
    upcxx::deserialized_type_t<TypeToRead> *ad = reinterpret_cast<upcxx::deserialized_type_t<TypeToRead> *>(adstore);
    return ad;
}

template <class Reader, class TypeToRead> TypeToRead *read(Reader &reader)
{
    auto adstore = new typename std::aligned_storage<sizeof(TypeToRead), alignof(TypeToRead)>::type;
    reader.template read_into<TypeToRead>(adstore);
    TypeToRead *ad = reinterpret_cast<TypeToRead *>(adstore);
    return ad;
}

// wait in bulk for size 8, if since the tuple cant be dynamically allocated
namespace
{
template <class T> std::vector<T> getTup8(const std::vector<upcxx::future<T>> &refs)
{
    if (refs.size() < 8)
    {
        throw std::runtime_error("size < 8");
    }
    std::vector<T> retcopy;
    retcopy.reserve(8);
    auto xfut = upcxx::when_all(refs[0], refs[1], refs[2], refs[3], refs[4], refs[5], refs[6], refs[7]);
    auto x = xfut.wait();
    retcopy.push_back(std::get<0>(x));
    retcopy.push_back(std::get<1>(x));
    retcopy.push_back(std::get<2>(x));
    retcopy.push_back(std::get<3>(x));
    retcopy.push_back(std::get<4>(x));
    retcopy.push_back(std::get<5>(x));
    retcopy.push_back(std::get<6>(x));
    retcopy.push_back(std::get<7>(x));
    return retcopy;
}

// wait in bulk for size 4, if since the tuple cant be dynamically allocated
template <class T> std::vector<T> getTup4(const std::vector<upcxx::future<T>> &refs)
{
    if (refs.size() < 4)
    {
        throw std::runtime_error("size < 4");
    }
    std::vector<T> retcopy;
    retcopy.reserve(4);
    auto xfut = upcxx::when_all(refs[0], refs[1], refs[2], refs[3]);
    auto x = xfut.wait();
    retcopy.push_back(std::get<0>(x));
    retcopy.push_back(std::get<1>(x));
    retcopy.push_back(std::get<2>(x));
    retcopy.push_back(std::get<3>(x));
    return retcopy;
}

// wait in bulk for size 16, if since the tuple cant be dynamically allocated
template <class T> std::vector<T> getTup16(const std::vector<upcxx::future<T>> &refs)
{
    if (refs.size() < 16)
    {
        throw std::runtime_error("size < 16");
    }
    std::vector<T> retcopy;
    retcopy.reserve(16);
    auto xfut = upcxx::when_all(refs[0], refs[1], refs[2], refs[3], refs[4], refs[5], refs[6], refs[7], refs[8], refs[9], refs[10], refs[11], refs[12],
                                refs[13], refs[14], refs[15]);
    auto x = xfut.wait();
    retcopy.push_back(std::get<0>(x));
    retcopy.push_back(std::get<1>(x));
    retcopy.push_back(std::get<2>(x));
    retcopy.push_back(std::get<3>(x));
    retcopy.push_back(std::get<4>(x));
    retcopy.push_back(std::get<5>(x));
    retcopy.push_back(std::get<6>(x));
    retcopy.push_back(std::get<7>(x));
    retcopy.push_back(std::get<8>(x));
    retcopy.push_back(std::get<9>(x));
    retcopy.push_back(std::get<10>(x));
    retcopy.push_back(std::get<11>(x));
    retcopy.push_back(std::get<12>(x));
    retcopy.push_back(std::get<13>(x));
    retcopy.push_back(std::get<14>(x));
    retcopy.push_back(std::get<15>(x));
    return retcopy;
}
} // namespace

// wait for the vector of future in bulks where pairs of 1,4,8,16 are waited in bulks
template <class T> std::vector<T> waitForRuntimeLength(const std::vector<upcxx::future<T>> &refs)
{
    if (refs.empty())
    {
        return {};
    }

    std::vector<upcxx::future<T>> interncopy = refs;
    std::vector<T> retcopy;

    int size = refs.size();
    int i = 0;
    while (i < size)
    {
        if ((size - i) > 16)
        {
            auto firsteight = getTup16(interncopy);
            retcopy.insert(retcopy.end(), firsteight.begin(), firsteight.end());
            interncopy.erase(interncopy.begin(), interncopy.begin() + 16);
            i += 16;
        }
        else if ((size - i) > 8)
        {
            auto firsteight = getTup8(interncopy);
            retcopy.insert(retcopy.end(), firsteight.begin(), firsteight.end());
            interncopy.erase(interncopy.begin(), interncopy.begin() + 8);
            i += 8;
        }
        else if ((size - i) > 4)
        {
            auto firsteight = getTup4(interncopy);
            retcopy.insert(retcopy.end(), firsteight.begin(), firsteight.end());
            interncopy.erase(interncopy.begin(), interncopy.begin() + 4);
            i += 4;
        }
        else
        {
            auto fut = refs[i];
            auto ref = fut.wait();
            retcopy.push_back(ref);
            i += 1;
        }
    }
    return retcopy;
}

template <unsigned int p> int constexpr ctPow(const int x)
{
    if constexpr (p == 0)
        return 1;
    if constexpr (p == 1)
        return x;

    int tmp = ctPow<p / 2>(x);
    if constexpr ((p % 2) == 0)
    {
        return tmp * tmp;
    }
    else
    {
        return x * tmp * tmp;
    }
}

int intPow(int x, unsigned int p);

upcxx::intrank_t rank_n();

} // namespace util
