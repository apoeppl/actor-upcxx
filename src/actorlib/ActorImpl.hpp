/**
 * @file
 * This file is part of actorlib.
 *
 * @author Alexander Pöppl (poeppl AT in.tum.de,
 * https://www5.in.tum.de/wiki/index.php/Alexander_P%C3%B6ppl,_M.Sc.)
 *
 * @section LICENSE
 *
 * actorlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * actorlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with actorlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @section DESCRIPTION
 *
 * TODO
 */
#pragma once
#include "ActorData.hpp"
#include "PortInformationBase.hpp"
#include "Utility.hpp"
#include <array>
#include <atomic>
#include <chrono>
#include <memory>
#include <optional>
#include <random>
#include <string>
#include <thread>
#include <upcxx/upcxx.hpp>
#include <utility>
#include <vector>

#ifdef TRACE
#include "VT.h"
#endif

#include "ActorState.hpp"

#define history 32

class ActorImpl;
using GlobalActorRef = upcxx::global_ptr<ActorImpl *>;
using GlobalChannelRef = upcxx::global_ptr<void>;

class ActorGraph;
class AbstractInPort;
class AbstractOutPort;
template <typename T, int capacity> class InPort;
template <typename T, int capacity> class OutPort;
class TaskDeque;

class ActorImpl
{
    friend class ActorGraph;
    friend class AbstractInPort;
    friend class AbstractOutPort;

  private:
    const static ASPrinter asp;
    // the name to help differ initial and following run(x,y) calls
    // does the same thing as start, checks for parallization as extra -> has
    // start actor
    // stop actor
    const std::string name; // name

  public:
    bool stealable = true;
    int nomadiness; // amount of times migrated

  private:
#ifdef TRACE
    static int event_act;
#endif

  protected:
    ActorGraph *parentActorGraph;                                // Ag that this actor is connected
    std::unordered_map<std::string, AbstractInPort *> inPorts;   // map of all inports
    std::unordered_map<std::string, AbstractOutPort *> outPorts; // map of all outports
    upcxx::persona actorPersona;                                 // used in OMP strategy
    std::atomic<ActorState> state;
    // required for reinitialization/reconnection of the actor on act rank
    mutable std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>>
        portsInformation;                                                                           // a struct to save port informations in heap
    bool portsInfoApplicable;                                                                       // save if portsInformation can be used to refill
    uint64_t workTokens;                                                                            // amount of tokens actor has gathered
    uint64_t workTime;                                                                              // amount of the time this actor spent in its act
    template <typename T, int capacity> InPort<T, capacity> *makeInPort(std::string const &name);   // makes an InPort
    template <typename T, int capacity> OutPort<T, capacity> *makeOutPort(std::string const &name); // maes an OutPort

  private:
    std::chrono::steady_clock::time_point tbegin;
    std::chrono::steady_clock::time_point tend;
    std::mutex markmutex;
    upcxx::intrank_t markedBy;
    size_t pinnedBy;
    std::vector<unsigned short> pin_count;
    std::array<unsigned int, history> actor_cost;
    size_t index_for_actor_cost;
    bool ret_to_wants_to_terminate = false;
    int called = 0;

  public:
    ActorImpl(const std::string &name) noexcept;
    ActorImpl(std::string &&name) noexcept;
    virtual ~ActorImpl() noexcept;
    ActorImpl(const ActorImpl &act) noexcept;
    ActorImpl(ActorImpl &&act) noexcept;
    ActorImpl(ActorData &&data) noexcept;
    ActorImpl(
        ActorData &&data,
        std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> &&portsInformation) noexcept;
    ActorImpl &operator=(ActorImpl &act) = delete;
    std::string toString();
    AbstractOutPort *getOutPort(std::string &name);
    AbstractInPort *getInPort(std::string &name);
    template <typename T, int capacity>
    std::tuple<InPort<T, capacity> *, InPort<T, capacity> *, OutPort<T, capacity> *> generateMultiplexerPorts(std::string &portName);
    template <typename T, int capacity>
    std::tuple<OutPort<T, capacity> *, OutPort<T, capacity> *, InPort<T, capacity> *> generateDemultiplexerPorts(std::string &portName);
    virtual void act() = 0;
    void b_act(); // runs act() but tracks time
    void triggeredAct(const std::string &portName) const;
    void triggeredFlushBuffer();
    void setPersonas();                          // set personas of ports to self' persona
    void rmInPort(const std::string &portName);  // rm inport
    void rmOutPort(const std::string &portName); // rm outport
    std::pair<std::vector<std::pair<std::string, AbstractInPort *>>, std::vector<std::pair<std::string, AbstractOutPort *>>>
    getPorts() const;                                                              // get pointers and names of all ports
    std::vector<std::pair<std::string, AbstractInPort *>> getAllInPorts() const;   // get only the in ports
    std::vector<std::pair<std::string, AbstractOutPort *>> getAllOutPorts() const; // get only or the out ports
    std::vector<std::string> getAllInPortNames() const;                            // get only the inport names
    std::vector<std::string> getAllOutPortNames() const;                           // get only the outport name
    std::string getName() const;
    std::string getReplicationName() const;
    std::tuple<uint64_t, uint64_t> getWork() const;
    void resetWork();                                                // sets both work tracking variables to 0
    bool needsReactivation();                                        // checs if a reactivation is necessary
    static bool isRunningGlobal(upcxx::global_ptr<ActorImpl *> ref); // static method to check if an actor is running
    // refills ports with data according to the information available in ports
    // information
    void refillPorts(); // changes state of ports to the state that is saved in the portsInformation struct
    // generates 2 arrays of ports informations, should be assigned to
    // portsInformation as soon as possible
    std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>>
    generatePortsInformation() const; // generates the portsInformation struct from the ports
    // uses info in member
    // moves from actor a and assigns ports information ans assigns to member
    // variable
    void movePortsInformation(ActorImpl &&a); // moves the portsinfo of anact actor
    // copies from actor a/ generates ports informatiion from ports of actor a,
    // and assigns to member var
    void copyPortsInformation(const ActorImpl &a); // copies the port info of anact actor
    void generateAndAssignPortsInformation();      // generates portinfo and assigns to member portsInformation
    bool portsInfoBadState() const;                // checks if the ports information in a legal state
    void assignPortsInformation(std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>>
                                    &&tup); // assignes the new portsinfo frees the old
    std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>>
    copyPortsInformation() const;  // copies ports info and returns the portsINormation
    void prepare();                // prepare for migration
    virtual void prepareDerived(); // overriden if extra preparation is needed for migration
    std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>>
    moveTuple() const;         // get portsInformation
    bool getIsRunning() const; // return true if isRunning is true
    ActorState getRunningState() const;
    virtual std::string getTimeInfo() { return ""; } // get time Information
    bool start();
    const std::string &getNameRef() const;
    bool tryLock(ActorState expected, ActorState desired);
    bool mark(upcxx::intrank_t marker);
    bool unmark(upcxx::intrank_t marker);
    bool pin(upcxx::intrank_t pinner);
    bool unpin(upcxx::intrank_t pinner);
    bool isPinned() const;
    bool isMarked() const;
    bool stopWithCaller(ActorState as, upcxx::intrank_t caller); // make stop private because, the graph must ensure some stuff for migration
    bool startWithCaller(upcxx::intrank_t caller);
    void broadcastTaskDeque(TaskDeque *tdq);
    bool messageOnWire() const;
    bool messageOnWire(const std::string &outportName) const;
    void severeConnectionIn(const std::string &portName);
    void severeConnectionOut(const std::string &portName);
    void resurrectConnection(const std::string &inPortName, upcxx::intrank_t rank, AbstractOutPort *outptr);
    void resurrectConnection(const std::string &outPortName, GlobalChannelRef channelptr);
    AbstractInPort *getInPort(const std::string &name);
    AbstractOutPort *getOutPort(const std::string &name);
    void flushBuffers();
    void flushBuffer(const std::string &portName);
    bool buffersEmpty() const noexcept;
    bool bufferEmpty(const std::string &portName) const noexcept;
    void sendNotifications();
    void sendNotification(const std::string &portName);
    bool noNeedToNotify() const noexcept;
    bool noNeedToNotify(const std::string &portName) const noexcept;
    bool markableForMigration(int caller) const;
    bool markedForMigration() const;
    bool centralStealable() const;
    void set_port_size();
    bool connected();
    size_t getMark() const;
    uint64_t calc_cost();
    void setRunning();
    bool isTerminated();

  protected:
    bool stop();

  private:
    void initPinCount();
    /*
      check explanation in ActorGraph for registration methods
    */
    friend void registerRpc(ActorImpl *a);
    friend void deregisterRpc(ActorImpl *a);
    friend void registerLpc(ActorImpl *a);
    friend void deregisterLpc(ActorImpl *a);

    bool stopFromAct();

  public:
    /*
      Serialized the abstract base class to an ActorData object that describes state
    */
    struct upcxx_serialization
    {
        template <typename Writer> static void serialize(Writer &writer, ActorImpl const &object)
        {
            writer.write(object.name);
            writer.write(object.nomadiness);
            writer.write(object.workTokens);
            writer.write(object.workTime);
            writer.write(object.markedBy);

            return;
        }

        template <typename Reader> static ActorData *deserialize(Reader &reader, void *storage)
        {
            std::string name = reader.template read<std::string>();
            int nomadiness = reader.template read<int>();
            uint64_t workTokens = reader.template read<uint64_t>();
            uint64_t workTime = reader.template read<uint64_t>();
            auto rc = reader.template read<upcxx::intrank_t>();

            ActorData *v =
                ::new (storage) ActorData(std::move(name), nomadiness, workTokens, workTime, std::move(rc), ActorState::TemporaryStoppedForMigration);
            return v;
        }
    };
};

template <typename type, int capacity> InPort<type, capacity> *ActorImpl::makeInPort(const std::string &name)
{
    InPort<type, capacity> *ip = ::new InPort<type, capacity>(name, this->name);
    ip->connectedActor = this;
    inPorts[name] = ip;
    return ip;
}

template <typename type, int capacity> OutPort<type, capacity> *ActorImpl::makeOutPort(const std::string &name)
{
    OutPort<type, capacity> *op = ::new OutPort<type, capacity>(name);
    op->connectedActor = this;
    outPorts[name] = op;
    return op;
}
