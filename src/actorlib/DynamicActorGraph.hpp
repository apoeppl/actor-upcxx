#pragma once
#include "AbstractInPort.hpp"
#include "AbstractOutPort.hpp"
#include "AcquisitonResult.hpp"
#include "Actor.hpp"
#include "ActorGraph.hpp"
#include "ActorImpl.hpp"
#include "ActorState.hpp"
#include "Distribution.hpp"
#include "MigrationDispatcher.hpp"
#include "PortGraph.hpp"
#include "Statistics.hpp"
#include "Utility.hpp"
#include <algorithm>
#include <chrono>
#include <numeric>
#include <random>
#include <set>
#include <type_traits>
#include <upcxx/upcxx.hpp>

using clk = std::chrono::steady_clock;

/*
    DynamicGraph uses curiously recurring template pattern with 2 Templates, as 2 Strategies for implementing DynamicActorGraph,
    SingleActorAGraph and DiynamicActorGraph where SAAG is for one actor type and has less overhead, DAAG is not complete but it
    is used to support actorGraphs with multiple ActorTypes

    1-Wrapper around AG
    2-PortGraph functionality
    3-Distribution functionality
    3-Migration functionality

    Any functionality that differs when the there can be multiple actor or not are distributed to the template argumnets which are
    SAAD or DAAG
*/

template <class T, class... Ts> class MultipleActorAGraph;

template <class T> class SingleActorAGraph;
/*
    Convenienve type declaration
*/
template <class T, class... Ts> using AG = typename std::conditional<util::multiple<T, Ts...>(), MultipleActorAGraph<T, Ts...>, SingleActorAGraph<T>>::type;

template <class A> class DynamicActorGraph : public MigrationDispatcher
{
    friend A;

  private:
    unsigned int interrupts;          // number of migration phases between run calls
    std::shared_mutex migrationMutex; // this is needed even in rank mode because other ranks can try to steal at the same time
    mutable std::mt19937 randomEngine;

    // not needed in rank strategy
    // std::shared_mutex mark_lock;
    std::vector<bool> used;
    std::vector<std::vector<upcxx::future<bool>>> pin_attempts;
    std::vector<std::vector<bool>> pins;
    std::vector<upcxx::promise<>> promises;
    ASPrinter asp;
    size_t iter = 0;
    std::vector<GlobalActorRef> actors_after_mig;

  protected:
    PortGraph pg; // port graph to save connections
    ActorGraph ag;
    upcxx::persona &masterPersona;                               // masterPersona propagated to ag
    std::optional<double> lastIterVar;                           // runtime variance between ranks, of the iteration before
    std::optional<double> currIterVar;                           // current runtime variance betweenr anks
    upcxx::dist_object<DynamicActorGraph<A> *> remoteComponents; // remote components for sending shit
    std::set<std::string> moving;

  public:
    Distribution *actorDistribution; // for actor distribution, pointer because

  public:
    DynamicActorGraph()
        : MigrationDispatcher(), interrupts(0), migrationMutex(), randomEngine{std::random_device{}()}, used(), pin_attempts(), pins(), pg(),
          ag(dynamic_cast<MigrationDispatcher *>(this), &pg), masterPersona(upcxx::master_persona()), lastIterVar(), currIterVar(), remoteComponents(this),
          moving(), actorDistribution(::new Distribution(ag.getActors()))
    {
    }

    /*
    /dss/dsshome1/lxc05/ge69xij2/actor-upcxx/src/actorlib/DynamicActorGraph.hpp(100): warning #1011: missing return statement at end of non-void function
    "DynamicActorGraph<A>::upcastActor(Act *) [with A=MultipleActorAGraph<SimulationActor, DoubleHeightSimulationActor>, Act=SimulationActor]"
          }
    */
    template <typename Act> ActorImpl *upcastActor(Act *a) // upcast actor regardles it inherits from Actor or ActorImpl
    {
        if constexpr (std::is_base_of<Actor, Act>::value)
        {
            Actor *tmpp = static_cast<Actor *>(a);
            return &(tmpp->asBase());
        }
        else if constexpr (std::is_base_of<ActorImpl, Act>::value)
        {
            ActorImpl *ap = static_cast<ActorImpl *>(a);
            return ap;
        }

        return nullptr;
    }

    void addActor(ActorImpl *a); // calls ag::addactor

    void addActor(Actor *a); // calls ag::addactor

    upcxx::future<> addActorAsync(ActorImpl *a); // calls ag::addactor

    upcxx::future<> addActorAsync(Actor *a); // calls ag::addactor

    upcxx::future<> addActorToAnotherAsync(GlobalActorRef a, upcxx::intrank_t rank); // calls ag::addActorTo..

    upcxx::future<> addActorsToAnotherAsync(const std::vector<GlobalActorRef> &refs);

    std::string prettyPrint(); // calls ag::

    upcxx::future<> connectPortsAsync(const std::string &sourceActorName, const std::string &sourcePortName, const std::string &destinationActorName,
                                      const std::string &destinationPortName); // calls::

    upcxx::future<> connectPortsAsync(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                      const std::string &destinationPortName); // calls ag::connectPorts

    upcxx::future<> disconnectPortsAsync(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                         const std::string &destinationPortName);

    void connectPorts(const std::string &sourceActorname, const std::string &sourcePortName, const std::string &destinationActorName,
                      const std::string &destinationPortName); // calls::

    void connectPorts(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                      const std::string &destinationPortName); // calls ag::connectPorts

    GlobalActorRef getActor(const std::string &name) { return ag.getActor(name); } // calls ag::get

    double run(); // calls ag::run

    void initConnections(std::vector<std::tuple<std::string, std::string, std::string, std::string>> &&connections);

    void initConnections(const std::vector<std::tuple<std::string, std::string, std::string, std::string>>
                             &connections); // initializes connections given in the connevtion vector

    // inits actors given with a name list, connection vector, and input argument U for a solo actor
    // initializes actors after distribution and connect thems
    // constructors must look like A(std::string name,U u)
    template <typename U>
    void initFromList(std::vector<std::string> &&actorNames, std::vector<std::tuple<std::string, std::string, std::string, std::string>> &&connections,
                      std::vector<U> &&inputArguments)
    {
        static_cast<A *>(this)->initFromList(std::move(actorNames), std::move(connections), std::move(inputArguments));
    }

    template <typename U, typename Z, typename... Us>
    void initFromList(std::vector<std::string> &&actorNames, std::vector<std::tuple<std::string, std::string, std::string, std::string>> &&connections,
                      std::vector<std::tuple<U, Z, Us...>> &&inputArguments)
    {
        static_cast<A *>(this)->initFromList(std::move(actorNames), std::move(connections), std::move(inputArguments));
    }

    // init actors that do not require input arguments but only their names
    // constructor must look like A(std::string name)
    void initFromList(std::vector<std::string> &&actorNames, std::vector<std::tuple<std::string, std::string, std::string, std::string>> &&connections)
    {
        static_cast<A *>(this)->initFromList(std::move(actorNames), std::move(connections));
    }

    // migrates actor with name from its rank to rank rank but async and consist of a chain of callbacks
    // std::optional<upcxx::future<GlobalActorRef>> migrateActorAsync(int rank, const std::string &name, size_t onHoldCalls) override final;

    // steals actor with name from its rank to rank rank but async and consist of a chain of callbacks
    // it is like migrateActor but it is not called from the rank that has the actor but from the rank that wants to actors
    // is not totally async, it blocks until the lock is acquired

    upcxx::future<GlobalActorRef> stealActorAsync(const std::string &name) override final;

    ~DynamicActorGraph() { delete actorDistribution; }

    void stopActors(const std::unordered_map<std::string, upcxx::intrank_t> &l);

    upcxx::future<> restartActors(const std::unordered_map<std::string, upcxx::intrank_t> &l);

    std::vector<std::string> findAnActorToStealFromTheCrowdiestRank() override final;

    const std::unordered_map<std::string, GlobalActorRef> *getActors() const { return this->ag.getActors(); }

    const std::unordered_map<std::string, GlobalActorRef> &getActorsRef() const { return this->ag.getActorsRef(); }

  private:
    upcxx::future<> disconnectFromNeighboursAsync(const std::unordered_map<std::string, upcxx::intrank_t> &migList);

    upcxx::future<> disconnectFromNeighboursAsync(const std::string &name);

    upcxx::future<> reconnectToNeighboursAsync(const std::unordered_map<std::string, upcxx::intrank_t> &migList);

    upcxx::future<> reconnectToNeighboursAsync(const std::string &name);

    void phases(); // fires the migration phase, it redistributed actors and calls the migration::migrateDiscretePhases for bulk migration

    upcxx::future<GlobalActorRef> sendActorToAsync(int rank, std::string const &name)
    {
        return static_cast<A *>(this)->sendActorToAsyncImpl(rank, name);
    } // send actor waith name name (part of ag) to rank rank, return the global ref of the sent actor

    upcxx::future<GlobalActorRef> sendActorToAsync(int rank, GlobalActorRef name)
    {
        return static_cast<A *>(this)->sendActorToAsyncImpl(rank, name);
    } // send actor described by the global pointer to rank rank, return the global ref of the sent actor

    void rmActor(const std::string &name) { return this->ag.rmActor(name); } // calls ag::rmAct

    std::tuple<upcxx::future<>, GlobalActorRef> rmActorAsync(const std::string &name) { return this->ag.rmActorAsync(name); } // calls ag::rmActAsync

    std::tuple<upcxx::future<>, std::vector<GlobalActorRef>> rmActorsAsync(const std::unordered_map<std::string, upcxx::intrank_t> &migList);

    void prepareActors(const std::unordered_map<std::string, upcxx::intrank_t> &l) { this->ag.prepare(l); } // prepare all actors that need migration

    void updateWork(); // cals ag::updateWork with the right Track::t (does not delegate directly because of both)

    void disconnectActors(const std::unordered_map<std::string, upcxx::intrank_t> &migList); // disconnect actors in map from all neighbors

    std::vector<std::string> findActorsToSteal() override final;

    std::vector<std::string> findRandomActorsToSteal(size_t count) override final;

    AcquisitionResult tryToAcquireLockForActor(const std::string &name, upcxx::intrank_t marker);

    upcxx::future<AcquisitionResult> tryLockActorForMigration(const std::string &name) override final;

    upcxx::future<bool> tryStopSelfAndMarkNeighborhood(const std::string &name) override final;

    upcxx::future<> severeConnections(const std::string &name);

    upcxx::future<> resurrectConnections(const std::string &name, GlobalActorRef ref);

    // rm actors from the graph and save their global pointers as well as triggers,
    // wait in bulk
    std::tuple<upcxx::future<>, std::vector<GlobalActorRef>> rmOldActors(const std::unordered_map<std::string, upcxx::intrank_t> &migList);

    // del remains after sending
    // this is needed because deleting them before the send actions are completed ( they return futures )
    // then there will be segmentation faults
    void delRemains(const std::vector<GlobalActorRef> &l);

    // send actors in the migration list to their destination ranks,
    // returns the new actors on the destination and their triggercount
    upcxx::future<> sendActorsAsync(const std::vector<GlobalActorRef> &trigs, const std::unordered_map<std::string, upcxx::intrank_t> &migList);

    std::vector<GlobalActorRef> sendActors(const std::vector<GlobalActorRef> &trigs, const std::unordered_map<std::string, upcxx::intrank_t> &migList);

    // reinserts the actors in the vector to the actorgraph of the other rank
    upcxx::future<> reinsert(const std::vector<GlobalActorRef> &refs);

    // this is different than asynchron one, in the asynchron one you are not allowed to migrate if there is a pinned/migrating neighbor
    // here we severe the connection regardless of that
    upcxx::future<> severeConnections(const std::unordered_map<std::string, upcxx::intrank_t> &migList);

    // reconnects the actors that are migrated to their neighbors
    upcxx::future<> resurrectConnections(const std::unordered_map<std::string, upcxx::intrank_t> &migList);

    // same as void refillPorts(const std::vector<std::pair<GlobalActorRef, std::optional<size_t>>> &news)
    // but uses names and need 1 look up more per actor
    void refillPorts(const std::unordered_map<std::string, upcxx::intrank_t> &migList);

    std::unordered_map<std::string, upcxx::intrank_t> cleanseMigrationList(const std::unordered_map<std::string, upcxx::intrank_t> &migList);

    std::unordered_map<std::string, upcxx::intrank_t> createLocalMigrationList(const std::unordered_map<std::string, upcxx::intrank_t> &migList);

    void clearSentActorReferences();

    size_t getMarkOffset();

    void releaseMarkOffset(size_t);

    void migrateActorsDiscretePhases(const std::unordered_map<std::string, upcxx::intrank_t> &migList);

    upcxx::future<> sendTasks(const std::unordered_map<std::string, upcxx::intrank_t> &migList);

#ifdef TIME
    void printTimeInfo();
#endif
};

/*

    Implementation begin

*/
#include "MultipleActorAGraph.hpp"
#include "SingleActorAGraph.hpp"

#ifdef TIME
template <class A> void DynamicActorGraph<A>::printTimeInfo()
{
    std::stringstream ss;
    auto aps = ag.getActors();
    for (auto pr : *aps)
    {
        GlobalActorRef ref = pr.second;
        if (ref.where() == upcxx::rank_me())
        {
            ActorImpl *aimpl = *ref.local();
            ss << aimpl->getTimeInfo();
        }
    }
    std::cout << ss.str() << std::endl;
}
#endif

template <class A> size_t DynamicActorGraph<A>::getMarkOffset()
{
    for (size_t i = 0; i < used.size(); i++)
    {
        if (used[i] == false)
        {
            used[i] = true;
            pin_attempts[i].clear();
            pins[i].clear();
            upcxx::promise<> p;
            promises[i] = std::move(p);
            return i;
        }
    }

    std::vector<upcxx::future<bool>> attmps;
    std::vector<bool> mrks;
    upcxx::promise<> p;

    pin_attempts.emplace_back(std::move(attmps));
    pins.emplace_back(std::move(mrks));
    promises.push_back(std::move(p));
    used.push_back(true);

    return used.size() - 1;
}

template <class A> void DynamicActorGraph<A>::releaseMarkOffset(size_t offset)
{
    pin_attempts[offset].clear();
    pins[offset].clear();
    upcxx::promise<> p;
    promises[offset] = std::move(p);
    used[offset] = false;
}

template <class A> void DynamicActorGraph<A>::updateWork() { ag.addToTotalWork(); }

template <class A> void DynamicActorGraph<A>::initConnections(const std::vector<std::tuple<std::string, std::string, std::string, std::string>> &connections)
{
    // initialize connections
    std::vector<upcxx::future<>> futs;
    int myrank = upcxx::rank_me();

    for (auto &tup : connections)
    {
        std::string from = std::get<0>(tup);
        std::string op = std::get<1>(tup);
        std::string to = std::get<2>(tup);
        std::string ip = std::get<3>(tup);

        GlobalActorRef ref = this->ag.getActor(from);
        if (ref.where() == myrank)
        {
            GlobalActorRef otherref = this->ag.getActor(to);
            upcxx::future<> con = this->ag.connectPortsAsync(ref, op, otherref, ip);
            this->pg.insertEdge(std::move(from), std::move(op), std::move(to), std::move(ip));
            futs.push_back(con);
        }
    }

    // wait for all connections to be applied
    if (!futs.empty())
    {
        upcxx::future<> combined = util::combineFutures(futs);
        combined.wait();
    }
}

template <class A> void DynamicActorGraph<A>::initConnections(std::vector<std::tuple<std::string, std::string, std::string, std::string>> &&connections)
{
    const std::vector<std::tuple<std::string, std::string, std::string, std::string>> &lval = connections;
    this->initConnections(lval);
}

template <class A> double DynamicActorGraph<A>::run()
{
    double interval = std::numeric_limits<float>::max();

    if constexpr (config::migrationtype == config::MigrationType::BULK)
    {
        interval = 20.f;
    }

    if (upcxx::rank_me() == 0)
    {
        if constexpr (config::migrationtype == config::MigrationType::BULK)
        {
            std::cout << "Bulk Migration only" << std::endl;
        }
        else if constexpr (config::migrationtype == config::MigrationType::ASYNC)
        {
            std::cout << "Async Migration only" << std::endl;
        }
        else if constexpr (config::migrationtype == config::MigrationType::NO)
        {
            std::cout << "No Migration" << std::endl;
        }
    }

    bool finished = false;
    double totalRunTime = 0.0;
    int runcalls = 0;
    bool firsttime = true;

    while (!finished)
    {
        auto x = this->ag.run(interval, firsttime);
        interrupts += 1;
        firsttime = false;
        runcalls += 1;
        upcxx::progress();
        finished = upcxx::reduce_all(x.second, upcxx::experimental::op_bit_and).wait();
        totalRunTime += x.first;

        if constexpr (config::migrationtype == config::MigrationType::BULK)
        {
            if (!finished)
            {
                // call to the real part of the migration
                while (this->ag.rpcsInFlight > 0)
                {
                    upcxx::discharge();
                    upcxx::progress();
                }
                upcxx::barrier();
                phases();
                this->ag.addToTotalWork();
                this->ag.resetWork();
            }
        }
    }

    /*
        one last update work
    */
    this->ag.addToTotalWork();
    this->ag.resetWork();

#ifdef TIME
    printTimeInfo();
#endif

    if constexpr (config::migrationtype == config::MigrationType::BULK)
    {
        auto innanos = ag.getTotalWork();
        auto totalTime = util::nanoToSec(std::get<1>(innanos));
        auto totalTokens = std::get<0>(innanos);
        std::cout << "ActorGraph of rank-" << upcxx::rank_me() << " worked for: " << totalRunTime << " seconds, counted: " << totalTokens
                  << " tokens, with an active runtime of: " << totalTime << " seconds" << std::endl;
    }
    else
    {
        std::cout << "ActorGraph of rank-" << upcxx::rank_me() << " worked for: " << totalRunTime << std::endl;
    }

    return totalRunTime;
}

template <class A> void DynamicActorGraph<A>::phases()
{
    auto start = std::chrono::high_resolution_clock::now();
    actorDistribution->redistributeActors(pg, ag);
    upcxx::barrier();
    std::unordered_map<std::string, upcxx::intrank_t> migs = actorDistribution->getMigrationList();
#ifdef REPORT_MAIN_ACTIONS
    std::cout << util::migrationList2Str(upcxx::rank_me(), migs) << std::endl;
#endif

    /*
        Migration sends actors to the ranks found by the partitioner, of course if they are marked or pinned due to asynchronous migration
        they should not be migrated and removed from the list, also if their old rank and new rank are the same then we do not migrate
    */

    if (!migs.empty())
    {
        migrateActorsDiscretePhases(migs);
    }
#ifndef NDEBUG
    else
    {
        // if (!upcxx::rank_me())
        std::cout << "Migration List Empty." << std::endl;
    }
#endif

    auto end = std::chrono::high_resolution_clock::now();
    double elapsed = std::chrono::duration<double, std::milli>(end - start).count();
    int rank = upcxx::rank_me();
    std::string s = "Migration phase for rank: " + std::to_string(rank) + " took " + std::to_string(elapsed) +
                    " milliseconds, has: " + std::to_string(this->ag.getActiveActors()) + " active actors.";
    std::cout << s << std::endl;

    upcxx::barrier();
}

template <class A> upcxx::future<> DynamicActorGraph<A>::disconnectFromNeighboursAsync(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    std::vector<upcxx::future<>> futs;
    for (const auto &pr : migList)
    {
        GlobalActorRef actorre = this->ag.getActor(pr.first);

        if (actorre.where() == upcxx::rank_me())
        {
            auto f = disconnectFromNeighboursAsync(pr.first);
            futs.emplace_back(std::move(f));
        }
    }
    return util::combineFutures(futs);
}

template <class A> upcxx::future<> DynamicActorGraph<A>::disconnectFromNeighboursAsync(const std::string &name)
{
    GlobalActorRef addedactor = this->ag.getActor(name);
    // outportname,otheractor,inportname
    auto out = this->pg.getOutgoing(name);
    // return order: inportname, otheractor name, other actors ourport name
    auto in = this->pg.getIncoming(name);

    std::vector<upcxx::future<>> futs;
    futs.reserve(out.size() + in.size() + 1);
    for (size_t i = 0; i < out.size(); i++)
    {
        GlobalActorRef arec = this->ag.getActor(std::get<1>(out[i]));

        futs.push_back(this->ag.disconnectPortsAsync(addedactor, std::get<0>(out[i]), arec, std::get<2>(out[i])));
#ifdef REPORT_MAIN_ACTIONS
        std::cout << "(this->other) disconnect: " << name << "'s " << std::get<0>(out[i]) << " to " << std::get<1>(out[i]) << "'s " << std::get<2>(out[i])
                  << std::endl;
#endif
    }

    for (size_t i = 0; i < in.size(); i++)
    {
        GlobalActorRef asend = this->ag.getActor(std::get<1>(in[i]));

        futs.push_back(this->ag.disconnectPortsAsync(asend, std::get<2>(in[i]), addedactor, std::get<0>(in[i])));
#ifdef REPORT_MAIN_ACTIONS
        std::cout << "(other->this) disconnect: " << std::get<1>(in[i]) << "'s " << std::get<2>(in[i]) << " to " << name << "'s " << std::get<0>(in[i])
                  << std::endl;
#endif
    }

    upcxx::future<> all = util::combineFutures(futs);
    return all;
}

template <class A> upcxx::future<> DynamicActorGraph<A>::reconnectToNeighboursAsync(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    std::vector<upcxx::future<>> futs;

    for (const auto &pr : migList)
    {
        GlobalActorRef actorre = this->ag.getActor(pr.first);

        if (actorre.where() == upcxx::rank_me())
        {
            ActorImpl *ai = *actorre.local();

            if (ai->getRunningState() != ActorState::Terminated && ai->getRunningState() != ActorState::WantsToTerminate)
            {
                auto f = reconnectToNeighboursAsync(pr.first);
                futs.emplace_back(std::move(f));
            }
        }
    }

    return util::combineFutures(futs);
}

template <class A> upcxx::future<> DynamicActorGraph<A>::reconnectToNeighboursAsync(const std::string &name)
{
    // std::cout << "reconnect " << name << " to its neighbours" << std::endl;

    GlobalActorRef addedactor = this->ag.getActor(name);
    // outportname,otheractor,inportname
    auto out = this->pg.getOutgoing(name);
    // return order: inportname, otheractor name, other actors ourport name
    auto in = this->pg.getIncoming(name);

    std::vector<upcxx::future<>> futs;
    futs.reserve(1 + out.size() + in.size());

    for (size_t i = 0; i < out.size(); i++)
    {
        GlobalActorRef arec = this->ag.getActor(std::get<1>(out[i]));
        upcxx::future<> fut = this->ag.connectPortsAsync(addedactor, std::get<0>(out[i]), arec, std::get<2>(out[i]));
        futs.push_back(std::move(fut));
        // std::cout << "(this->other) reconnect: " << name << "'s " << std::get<0>(out[i]) << " to " << std::get<1>(out[i]) << "'s " << std::get<2>(out[i])
        //          << std::endl;
    }

    for (size_t i = 0; i < in.size(); i++)
    {
        GlobalActorRef asend = this->ag.getActor(std::get<1>(in[i]));
        upcxx::future<> fut = this->ag.connectPortsAsync(asend, std::get<2>(in[i]), addedactor, std::get<0>(in[i]));
        futs.push_back(std::move(fut));

        // std::cout << "(other->this) reconnect: " << std::get<1>(in[i]) << "'s " << std::get<2>(in[i]) << " to " << name << "'s " << std::get<0>(in[i])
        //         << std::endl;
    }

    upcxx::future<> all = util::combineFutures(futs);
    return all;
}

template <class A>
void DynamicActorGraph<A>::connectPorts(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                        const std::string &destinationPortName)
{
    this->connectPortsAsync(sourceActor, sourcePortName, destinationActor, destinationPortName).wait();
}

template <class A>
upcxx::future<> DynamicActorGraph<A>::connectPortsAsync(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                                        const std::string &destinationPortName)
{
    upcxx::future<std::string> fut1;
    upcxx::future<std::string> fut2;

    if (sourceActor.where() == upcxx::rank_me())
    {
        std::string saname = (*sourceActor.local())->getName();
        fut1 = upcxx::make_future(saname);
    }
    else
    {
        fut1 = upcxx::rpc(
            sourceActor.where(), [](GlobalActorRef sourceActor) { return (*sourceActor.local())->getName(); }, sourceActor);
    }

    if (destinationActor.where() == upcxx::rank_me())
    {
        std::string daname = (*destinationActor.local())->getName();
        fut2 = upcxx::make_future(daname);
    }
    else
    {
        fut2 = upcxx::rpc(
            destinationActor.where(), [](GlobalActorRef destinationActor) { return (*destinationActor.local())->getName(); }, destinationActor);
    }

    auto fut = upcxx::when_all(fut1, fut2);

    auto pgFut = fut.then(
        [this, sourcePortName, destinationPortName](const std::string &saname, const std::string &daname) -> upcxx::future<> {
            return pg.insertEdgeAsync(saname, sourcePortName, daname, destinationPortName);
        });

    auto connectFut = this->ag.connectPortsAsync(sourceActor, sourcePortName, destinationActor, destinationPortName);
    return upcxx::when_all(connectFut, pgFut);
}

template <class A>
upcxx::future<> DynamicActorGraph<A>::disconnectPortsAsync(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                                           const std::string &destinationPortName)
{
    upcxx::future<std::string> fut1;
    upcxx::future<std::string> fut2;

    if (sourceActor.where() == upcxx::rank_me())
    {
        std::string saname = (*sourceActor.local())->getName();
        fut1 = upcxx::make_future(saname);
    }
    else
    {
        fut1 = upcxx::rpc(
            sourceActor.where(), [](GlobalActorRef sourceActor) { return (*sourceActor.local())->getName(); }, sourceActor);
    }

    if (destinationActor.where() == upcxx::rank_me())
    {
        std::string daname = (*destinationActor.local())->getName();
        fut2 = upcxx::make_future(daname);
    }
    else
    {
        fut2 = upcxx::rpc(
            destinationActor.where(), [](GlobalActorRef destinationActor) { return (*destinationActor.local())->getName(); }, destinationActor);
    }

    auto fut = upcxx::when_all(fut1, fut2);

    auto pgFut = fut.then(
        [this, sourcePortName, destinationPortName](const std::string &saname, const std::string &daname) -> upcxx::future<> {
            return pg.removeEdgeAsync(saname, sourcePortName, daname, destinationPortName);
        });

    auto connectFut = this->ag.disconnectPortsAsync(sourceActor, sourcePortName, destinationActor, destinationPortName);
    return upcxx::when_all(connectFut, pgFut);
}

template <class A>
void DynamicActorGraph<A>::connectPorts(const std::string &sourceActorName, const std::string &sourcePortName, const std::string &destinationActorName,
                                        const std::string &destinationPortName)
{
    connectPortsAsync(sourceActorName, sourcePortName, destinationActorName, destinationPortName).wait();
}

template <class A>
upcxx::future<> DynamicActorGraph<A>::connectPortsAsync(const std::string &sourceActorName, const std::string &sourcePortName,
                                                        const std::string &destinationActorName, const std::string &destinationPortName)
{
    GlobalActorRef sa = ag.getActor(sourceActorName);
    GlobalActorRef da = ag.getActor(destinationActorName);
    return this->connectPortsAsync(sa, sourcePortName, da, destinationPortName);
}

template <class A> std::string DynamicActorGraph<A>::prettyPrint()
{
    auto s = ag.prettyPrint();
    s += pg.toStr();
    s += ("Count of active actors on rank: " + std::to_string(upcxx::rank_me()) + " = " + std::to_string(ag.getActiveActors()));
    return s;
}

template <class A> upcxx::future<> DynamicActorGraph<A>::addActorToAnotherAsync(GlobalActorRef a, upcxx::intrank_t rank)
{
    return this->ag.addActorToAnotherAsync(a, rank);
}

template <class A> void DynamicActorGraph<A>::addActor(ActorImpl *a)
{
    ag.addActor(a);
    pg.insertNode(a->getName(), upcxx::rank_me());
    // std::cout << "add " << a->getName() << std::endl;
}

template <class A> void DynamicActorGraph<A>::addActor(Actor *a)
{
    ag.addActor(a);
    pg.insertNode(a->getName(), upcxx::rank_me());
    // std::cout << "add " << a->getName() << std::endl;
}

template <class A> upcxx::future<> DynamicActorGraph<A>::addActorAsync(ActorImpl *a)
{
    auto fut1 = this->ag.addActorAsync(a);
    auto fut2 = pg.insertNodeAsync(a->getName(), upcxx::rank_me());
    // std::cout << "add " << a->getName() << std::endl;
    return upcxx::when_all(fut1, fut2);
}

template <class A> upcxx::future<> DynamicActorGraph<A>::addActorAsync(Actor *a)
{
    upcxx::future<> fut = this->ag.addActorAsync(a);
    pg.insertNode(a->getName(), upcxx::rank_me());
    // std::cout << "add " << a->getName() << std::endl;
    return fut;
}

template <class A> void DynamicActorGraph<A>::disconnectActors(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    for (auto &pr : migList)
    {
        GlobalActorRef ref = ag.getActor(pr.first);

        if (ref.where() == upcxx::rank_me())
        {
            disconnectFromNeighboursAsync(pr.first).wait();
        }
    }
}

template <typename A> AcquisitionResult DynamicActorGraph<A>::tryToAcquireLockForActor(const std::string &name, upcxx::intrank_t marker)
{
    if (this->ag.locallyActiveActors.size() <= 1)
    {
        return AcquisitionResult::NotEnoughActors;
    }

    if (moving.find(name) != moving.end())
    {
        return AcquisitionResult::ActorBeingMigrated;
    }

#ifndef NDEBUG
    assert(moving.size() <= config::maxMigrationCountPerRank);
#endif

    if (moving.size() == config::maxMigrationCountPerRank)
    {
        return AcquisitionResult::RankMigrationOnLimit;
    }

    GlobalActorRef ref = this->ag.getActorNoThrow(name);

    if (ref == nullptr)
    {
        return AcquisitionResult::ActorBeingMigrated;
    }

    if (ref.where() == marker)
    {
        return AcquisitionResult::ActorBeingMigrated;
    }

    ActorImpl *ai = *ref.local();

    /*
    Running,
    NeedsActivation,
    TemporaryStoppedForMigration,
    Terminated,
    UnmigratableAndStopped,
    Transitioning
    */

    if (ai->getRunningState() == ActorState::Terminated || ai->getRunningState() == ActorState::WantsToTerminate)
    {
        return AcquisitionResult::ActorTerminated;
    }

    bool could = ai->mark(marker);

    if (!could)
    {
        if (ai->getRunningState() == ActorState::Terminated || ai->getRunningState() == ActorState::WantsToTerminate)
        {
            return AcquisitionResult::ActorTerminated;
        }
        else if (ai->getRunningState() == ActorState::TemporaryStoppedForMigration || ai->markedForMigration())
        {
            return AcquisitionResult::ActorBeingMigrated;
        }
        else if (ai->isMarked())
        {
            return AcquisitionResult::LostLockingRace;
        }
        else if (ai->isPinned())
        {
            return AcquisitionResult::UnmigrateableNeighbor;
        }

        return AcquisitionResult::UnmigrateableNeighbor;
    }

    if (!this->ag.taskDeque.hasExecutableTasks(name))
    {
        ai->unmark(marker);
        return AcquisitionResult::HasNoTasks;
    }

    switch (ai->getRunningState())
    {
    case ActorState::TemporaryStoppedForMigration:
    {
        assert(false);
        // std::cout << this->asp.as2str(ai->getRunningState()) << std::endl;
        return AcquisitionResult::LostLockingRace;
    }
    case ActorState::WantsToTerminate:
    {
        assert(false);
        return AcquisitionResult::ActorTerminated;
    }
    case ActorState::Terminated:
    {
        assert(false);
        return AcquisitionResult::ActorTerminated;
    }
    case ActorState::Transitioning:
    {
#ifdef PARALLEL
        // maybe it could happen i am not
#else
        assert(false);
#endif
        // std::cout << this->asp.as2str(ai->getRunningState()) << std::endl;
        return AcquisitionResult::LostLockingRace;
    }
    case ActorState::NeedsActivation:
    case ActorState::Running:
    {
        this->moving.insert(name);
        return AcquisitionResult::Ok;
    }
    default:
    {
        throw std::runtime_error("Unhandled case in try lock");
    }
    }
}

template <typename A> upcxx::future<AcquisitionResult> DynamicActorGraph<A>::tryLockActorForMigration(const std::string &name)
{
    GlobalActorRef ref = this->ag.getActorNoThrow(name);

    if (ref == nullptr)
    {
        return upcxx::make_future(AcquisitionResult::ActorBeingMigrated);
    }

    int at = ref.where();

    if (at == upcxx::rank_me())
    {
        throw std::runtime_error("Oh no!");
        AcquisitionResult h = tryToAcquireLockForActor(name, upcxx::rank_me());
        upcxx::future<AcquisitionResult> locked = upcxx::make_future(std::move(h));
        return locked;
    }
    else
    {
        upcxx::future<AcquisitionResult> locked = upcxx::rpc(
            at,
            [](upcxx::dist_object<DynamicActorGraph<A> *> &rdag, std::string name, upcxx::intrank_t marker)
            { return (*rdag)->tryToAcquireLockForActor(name, marker); },
            remoteComponents, name, upcxx::rank_me());
        return locked;
    }
}

// Temporary Termination with state
template <typename A> upcxx::future<bool> DynamicActorGraph<A>::tryStopSelfAndMarkNeighborhood(const std::string &name)
{

    GlobalActorRef ref = this->ag.getActor(name);
    int from = ref.where();
#ifndef NDEBUG
    assert(ref.where() != upcxx::rank_me());
#endif
    // stop all neighbors
    std::set<std::string> neighbors_ = this->pg.getNeighbors(name);
    std::vector<std::string> neighbors;
    std::copy(neighbors_.begin(), neighbors_.end(), std::back_inserter(neighbors));

#ifndef NDEBUG
    assert(neighbors.size() == neighbors_.size());
    assert(neighbors.size() > 0);
#endif
    size_t t = getMarkOffset();

    upcxx::promise<> try_mark_all_neighbors;
    try_mark_all_neighbors.require_anonymous(neighbors.size());

    promises[t] = std::move(try_mark_all_neighbors);
    this->pin_attempts[t].resize(neighbors.size());
    this->pins[t].resize(neighbors.size());
    std::fill(pins[t].begin(), pins[t].end(), false);
    std::fill(pin_attempts[t].begin(), pin_attempts[t].end(), upcxx::make_future(false));

    for (size_t i = 0; i < neighbors.size(); i++)
    {
        GlobalActorRef neighbor = this->ag.getActorNoThrow(neighbors[i]);

        if (neighbor == nullptr)
        {
            upcxx::future<bool> u = upcxx::make_future(false);
            this->pin_attempts[t][i] = u;
        }
        else
        {

            if (neighbor.where() == upcxx::rank_me())
            {
                ActorImpl *ai = *neighbor.local();
                bool could = ai->pin(upcxx::rank_me());
                upcxx::future<bool> u = upcxx::make_future(std::move(could));
                this->pin_attempts[t][i] = u;
            }
            else
            {

                upcxx::future<bool> could = upcxx::rpc(
                    neighbor.where(),
                    [](GlobalActorRef m, upcxx::dist_object<DynamicActorGraph<A> *> &remoteComponents, upcxx::intrank_t rank)
                    {
                        ActorImpl *ai = *m.local();
                        bool could = ai->pin(rank);
                        return could;
                    },
                    neighbor, remoteComponents, upcxx::rank_me());

                this->pin_attempts[t][i] = could;
            }
        }
    }

    for (size_t i = 0; i < neighbors.size(); i++)
    {
        pin_attempts[t][i].then(
            [this, t, i](bool pinned)
            {
                this->pins[t][i] = pinned;
                this->promises[t].fulfill_anonymous(1);
            });
    }
    // try_mark_all_neighbors.fulfill_anonymous(1);
    upcxx::future<> fut = this->promises[t].finalize();

    upcxx::future<bool> need_unmark = fut.then(
        [this, t, neighbors]() -> bool
        {
            bool tmp = true;
#ifndef NDEBUG
            assert(pins[t].size() == neighbors.size());
#endif
            for (size_t i = 0; i < neighbors.size(); i++)
            {
                tmp &= this->pins[t][i];
            }
            return !tmp;
        });

    upcxx::future<bool> should_i_return_now = need_unmark.then(
        [t, neighbors, name, ref, this](bool needs) -> upcxx::future<bool>
        {
            if (needs)
            {
                // could not mark at least one neighbor, so we unmark the ones we stopped
                upcxx::promise<> allDone;

                // last one could not be stopped so <
                for (size_t i = 0; i < neighbors.size(); i++)
                {
                    const std::string &neighborname = neighbors[i];

                    // unmark only if we have marked it
                    if (this->pins[t][i] == true)
                    {
                        GlobalActorRef neighbor = this->ag.getActor(neighborname);

                        if (neighbor.where() == upcxx::rank_me())
                        {

                            ActorImpl *ai = *neighbor.local();
                            bool could = ai->unpin(upcxx::rank_me());

                        // maybe somebody else has marked it in the meanwhile
#ifndef NDEBUG
                            if (!could)
                            {
                                if (ai->getRunningState() != ActorState::Terminated && ai->getRunningState() != ActorState::WantsToTerminate)
                                {
                                    throw std::runtime_error("I stopped it, I should be able to start? 1 " + neighborname + " state " +
                                                             this->asp.as2str(ai->getRunningState()));
                                }
                            }

#endif
                        }
                        else
                        {
                            auto cx = upcxx::operation_cx::as_promise(allDone);
                            upcxx::rpc(
                                neighbor.where(), cx,
                                [this](GlobalActorRef m, upcxx::intrank_t rank)
                                {
                                    ActorImpl *ai = *m.local();
                                    bool could = ai->unpin(rank);

#ifndef NDEBUG
                                    if (!could)
                                    {
                                        if (ai->getRunningState() != ActorState::Terminated && ai->getRunningState() != ActorState::WantsToTerminate)
                                        {
                                            throw std::runtime_error("I stopped it, I should be able to start? 1 " + ai->getName() + " state " +
                                                                     this->asp.as2str(ai->getRunningState()));
                                        }
                                    }
#endif
                                },
                                neighbor, upcxx::rank_me());
                        }
                    }
                    else
                    {
                        continue;
                    }
                }

                // since we have failed we need to unmark our original actor too
                if (ref.where() != upcxx::rank_me())
                {
                    auto cx = upcxx::operation_cx::as_promise(allDone);
                    upcxx::rpc(
                        ref.where(), cx,
                        [this](GlobalActorRef ref, upcxx::intrank_t rank)
                        {
                            ActorImpl *ai = *ref.local();
                            bool could = ai->unmark(rank);

#ifndef NDEBUG
                            if (!could)
                            {
                                if (ai->getRunningState() != ActorState::Terminated && ai->getRunningState() != ActorState::WantsToTerminate)
                                {
                                    throw std::runtime_error("I stopped it, I should be able to start? 2 " + ai->getName() + " state " +
                                                             this->asp.as2str(ai->getRunningState()));
                                }
                            }
#endif
                        },
                        ref, upcxx::rank_me());
                }
                else
                {
                    ActorImpl *ai = *ref.local();
                    bool could = ai->unmark(upcxx::rank_me());

#ifndef NDEBUG
                    if (!could)
                    {
                        if (ai->getRunningState() != ActorState::Terminated && ai->getRunningState() != ActorState::WantsToTerminate)
                        {
                            throw std::runtime_error("I stopped it, I should be able to start? 2 " + ai->getName() + " state " +
                                                     this->asp.as2str(ai->getRunningState()));
                        }
                    }
#endif
                }

                upcxx::future<> l = allDone.finalize();
                upcxx::future<bool> m = upcxx::make_future(true);
                upcxx::future<bool> r = upcxx::when_all(l, m);
                return r;
            }
            else
            {
                // then there is no problem, continue officer
                return upcxx::make_future(false);
            }
        });

    // try to stop the main actor now, since neighbors are marked
    upcxx::future<std::tuple<bool, bool, ActorState>> to_ret = should_i_return_now.then(
        [this, ref](bool failed) -> upcxx::future<std::tuple<bool, bool, ActorState>>
        {
            if (failed)
            {
                // the implementation should never return Acting so return acting to not unmark neighbors
                std::tuple<bool, bool, ActorState> tup = {false, true, ActorState::Running};
                upcxx::future<std::tuple<bool, bool, ActorState>> ret = upcxx::make_future(tup);
                return ret;
            }
            else
            {
                if (ref.where() != upcxx::rank_me())
                {
                    upcxx::future<std::tuple<bool, bool, ActorState>> mainstopped = upcxx::rpc(
                        ref.where(),
                        [](GlobalActorRef ref, upcxx::intrank_t rank) -> std::tuple<bool, bool, ActorState>
                        {
                            ActorImpl *ai = *ref.local();
                            return {ai->stopWithCaller(ActorState::TemporaryStoppedForMigration, rank), false, ai->getRunningState()};
                        },
                        ref, upcxx::rank_me());

                    return mainstopped;
                }
                else
                {
                    ActorImpl *ai = *ref.local();
                    bool mainstopped = ai->stopWithCaller(ActorState::TemporaryStoppedForMigration, upcxx::rank_me());
                    bool stopped = mainstopped;
                    ActorState as = ai->getRunningState();
                    std::tuple<bool, bool, ActorState> tup = {mainstopped, false, as};
                    upcxx::future<std::tuple<bool, bool, ActorState>> ret = upcxx::make_future(tup);
                    return ret;
                }
            }
        });

    upcxx::future<bool> to_ret2 = to_ret.then(
        [neighbors, ref, name, this](std::tuple<bool, bool, ActorState> tup) -> upcxx::future<bool>
        {
            bool stopped = std::get<0>(tup);
            bool marking_failed = std::get<1>(tup);
            ActorState as = std::get<2>(tup);

            if (!stopped)
            {
                upcxx::promise<> allDone;

                // unmark all of the neighbors again because we could not stop the main actor and stuff

                if (!marking_failed)
                {
                    for (auto neighbor_name : neighbors)
                    {
                        GlobalActorRef neighbor = this->ag.getActor(neighbor_name);

                        if (neighbor.where() == upcxx::rank_me())
                        {
                            ActorImpl *ai = *neighbor.local();
                            bool could = ai->unpin(upcxx::rank_me());
#ifndef NDEBUG
                            if (!could)
                            {
                                if ((ai->getRunningState() != ActorState::Terminated && ai->getRunningState() != ActorState::WantsToTerminate))
                                {
                                    throw std::runtime_error("I stopped it, I should be able to start? " + name + " state " +
                                                             this->asp.as2str(ai->getRunningState()));
                                }
                            }
#endif
                        }
                        else
                        {

                            auto cx = upcxx::operation_cx::as_promise(allDone);
                            upcxx::rpc(
                                neighbor.where(), cx,
                                [](GlobalActorRef m, upcxx::intrank_t rank, std::string name)
                                {
                                    ActorImpl *ai = *m.local();
                                    bool could = ai->unpin(rank);
#ifndef NDEBUG
                                    if (!could)
                                    {
                                        if ((ai->getRunningState() != ActorState::Terminated && ai->getRunningState() != ActorState::WantsToTerminate))
                                        {
                                            throw std::runtime_error("I stopped it, I should be able to start? " + name + " state ");
                                        }
                                    }
#endif
                                },
                                neighbor, upcxx::rank_me(), name);
                        }
                    }

                    if (as != ActorState::Terminated && as != ActorState::WantsToTerminate)
                    {
                        if (ref.where() == upcxx::rank_me())
                        {
                            ActorImpl *ai = *ref.local();
                            bool could = ai->unmark(upcxx::rank_me());
#ifndef NDEBUG
                            assert(could);
#endif
                        }
                        else
                        {
                            auto cx = upcxx::operation_cx::as_promise(allDone);
                            upcxx::rpc(
                                ref.where(), cx,
                                [](GlobalActorRef ref, int rank)
                                {
                                    ActorImpl *ai = *ref.local();
                                    bool could = ai->unmark(rank);
#ifndef NDEBUG
                                    assert(could);
#endif
                                },
                                ref, upcxx::rank_me());
                        }
                    }
                }

                if (!stopped || marking_failed)
                {
                    auto cx = upcxx::operation_cx::as_promise(allDone);
                    upcxx::rpc(
                        ref.where(), cx,
                        [](upcxx::dist_object<DynamicActorGraph<A> *> &remoteComponents, std::string &&name) { (*remoteComponents)->moving.erase(name); },
                        this->remoteComponents, name);
                }

                upcxx::future<> l = allDone.finalize();
                upcxx::future<bool> m = upcxx::make_future(false);
                upcxx::future<bool> r = upcxx::when_all(l, m);
                return r;
            }
            else
            {
                return upcxx::make_future(true);
            }
        });

    return to_ret2;
}

template <typename A> upcxx::future<> DynamicActorGraph<A>::severeConnections(const std::string &name)
{
    const Node &middle = this->pg.getNode(name);
    const std::vector<std::tuple<std::string, std::string, std::string>> &outgoing = middle.outgoing; // connection of type op -> ip|B (saved as op,B,ip)
    const std::vector<std::tuple<std::string, std::string, std::string>> &incoming = middle.incoming; // connection of type C|op -> ip (saved as ip,C,op)

    upcxx::promise<> allDone;
    for (const auto &el : outgoing)
    {

        GlobalActorRef ref = nullptr;
        ref = this->ag.getActor(std::get<1>(el));

        if (ref.where() == upcxx::rank_me())
        {
            ActorImpl *ai = *ref.local();
            ai->severeConnectionIn(std::get<2>(el));
        }
        else
        {
            auto cx = upcxx::operation_cx::as_promise(allDone);
            upcxx::rpc(
                ref.where(), cx,
                [](GlobalActorRef ref, std::string &&portName)
                {
                    ActorImpl *ai = *ref.local();
                    ai->severeConnectionIn(portName);
                },
                ref, std::get<2>(el));
        }
    }
    for (const auto &el : incoming)
    {
        GlobalActorRef ref = nullptr;
        ref = this->ag.getActor(std::get<1>(el));

        if (ref.where() == upcxx::rank_me())
        {
            ActorImpl *ai = *ref.local();
            ai->severeConnectionOut(std::get<2>(el));
            this->ag.triggeredFlushBuffer(ai->getName());
        }
        else
        {
            auto cx = upcxx::operation_cx::as_promise(allDone);
            upcxx::rpc(
                ref.where(), cx,
                [](GlobalActorRef ref, std::string &&portName, upcxx::dist_object<DynamicActorGraph<A> *> &rdag)
                {
                    ActorImpl *ai = *ref.local();
                    ai->severeConnectionOut(portName);
                    (*rdag)->ag.triggeredFlushBuffer(ai->getName());
                },
                ref, std::get<2>(el), remoteComponents);
        }
    }

    upcxx::future<> fut = allDone.finalize();
    return fut;
}

// the difference between the single severeConnections is that
// this is called during the migration phase, so if we have a neighbor that is not in the graph due to any possible reason, we just continue
template <typename A> upcxx::future<> DynamicActorGraph<A>::severeConnections(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    std::vector<upcxx::future<>> futs;
    futs.reserve(migList.size());

    for (const auto &el : migList)
    {
        upcxx::future<> f = severeConnections(el.first);
        futs.emplace_back(std::move(f));
    }

    upcxx::future<> combined = util::combineFutures(futs);
    return combined;
}

template <typename A> upcxx::future<> DynamicActorGraph<A>::resurrectConnections(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    std::vector<upcxx::future<>> futs;
    futs.reserve(migList.size());

    for (const auto &el : migList)
    {
        const std::string &name = el.first;
        upcxx::intrank_t t = el.second;

        GlobalActorRef r = this->ag.getActor(name);
        assert(r.where() == el.second);
        assert(r.where() != upcxx::rank_me());

        upcxx::future<> f = upcxx::rpc(
            r.where(),
            [](upcxx::dist_object<DynamicActorGraph<A> *> &rdag, GlobalActorRef ref, std::string name) -> upcxx::future<> {
                return (*rdag)->resurrectConnections(name, ref);
            },
            remoteComponents, r, el.first);

        futs.emplace_back(std::move(f));
    }

    upcxx::future<> combined = util::combineFutures(futs);
    return combined;
}

template <typename A> upcxx::future<> DynamicActorGraph<A>::resurrectConnections(const std::string &name, GlobalActorRef ref)
{
    const Node &middle = this->pg.getNode(name);
    const std::vector<std::tuple<std::string, std::string, std::string>> &outgoing = middle.outgoing; // connection of type op -> ip|B (saved as op,B,ip)
    const std::vector<std::tuple<std::string, std::string, std::string>> &incoming = middle.incoming; // connection of type C|op -> ip (saved as ip,C,op)

    GlobalActorRef mainref = this->ag.getActorNoThrow(name);

    assert(mainref == ref);

#ifndef NDEBUG
    if (mainref.where() != upcxx::rank_me())
    {
        throw std::runtime_error("The main actor should be on my rank right now! (my rank,actors rank)" + std::to_string(upcxx::rank_me()) + " " +
                                 std::to_string(mainref.where()));
    }
#endif

    ActorImpl *main = *mainref.local();

    main->set_port_size();

    upcxx::promise<> allDone;

    // bind the ports from other actor to the main actor
    for (const auto &el : outgoing)
    {
        // connection of type op -> ip|B (saved as op,B,ip)
        GlobalActorRef ref = this->ag.getActor(std::get<1>(el));

        AbstractOutPort *out = main->getOutPort(std::get<0>(el));
        if (ref.where() == upcxx::rank_me())
        {
            ActorImpl *ai = *ref.local();
            ai->resurrectConnection(std::get<2>(el), upcxx::rank_me(), out);
        }
        else
        {
            auto cx = upcxx::operation_cx::as_promise(allDone);
            upcxx::rpc(
                ref.where(), cx,
                [](upcxx::dist_object<DynamicActorGraph<A> *> &rdag, GlobalActorRef ref, std::string &&portName, upcxx::intrank_t rank, AbstractOutPort *outptr)
                {
                    ActorImpl *ai = *ref.local();
                    ai->resurrectConnection(portName, rank, outptr);
                },
                remoteComponents, ref, std::get<2>(el), upcxx::rank_me(), out);
        }
    }
    for (const auto &el : incoming)
    {
        // C|op -> ip (saved as ip,C,op)
        GlobalActorRef ref = this->ag.getActor(std::get<1>(el));

        AbstractInPort *ip = main->getInPort(std::get<0>(el));
        GlobalChannelRef channelptr = ip->getChannelPtr();

        if (ref.where() == upcxx::rank_me())
        {
            ActorImpl *other = *ref.local();
            AbstractOutPort *out = other->getOutPort(std::get<2>(el));

            other->resurrectConnection(std::get<2>(el), channelptr);
        }
        else
        {
            auto cx = upcxx::operation_cx::as_promise(allDone);
            upcxx::rpc(
                ref.where(), cx,
                [](GlobalActorRef ref, std::string &&portName, GlobalChannelRef channelptr)
                {
                    ActorImpl *other = *ref.local();
                    AbstractOutPort *out = other->getOutPort(portName);
                    other->resurrectConnection(portName, channelptr);
                },
                ref, std::get<2>(el), channelptr);
        }
    }

    std::vector<upcxx::future<>> futs;
    for (const auto &el : outgoing)
    {
        // op -> ip|B (saved as op,B,ip)
        GlobalActorRef ref = this->ag.getActorNoThrow(std::get<1>(el));
        while (ref == nullptr)
        {
            ref = this->ag.getActorNoThrow(std::get<1>(el));
        }

        AbstractOutPort *out = main->getOutPort(std::get<0>(el));

        if (ref.where() == upcxx::rank_me())
        {
            ActorImpl *ai = *ref.local();
            GlobalChannelRef chptr = ai->getInPort(std::get<2>(el))->getChannelPtr();

            main->resurrectConnection(std::get<0>(el), chptr);
        }
        else
        {
            upcxx::future<GlobalChannelRef> lm = upcxx::rpc(
                ref.where(),
                [](GlobalActorRef ref, std::string &&name)
                {
                    ActorImpl *ai = *ref.local();
                    GlobalChannelRef chptr = ai->getInPort(name)->getChannelPtr();
                    return chptr;
                },
                ref, std::get<2>(el));

            upcxx::future<> a = lm.then([this, main, el](GlobalChannelRef chptr) { main->resurrectConnection(std::get<0>(el), chptr); });
            futs.emplace_back(std::move(a));
        }
    }

    for (const auto &el : incoming)
    {
        //// C|op -> ip (saved as ip,C,op)
        GlobalActorRef ref = this->ag.getActorNoThrow(std::get<1>(el));
        while (ref == nullptr)
        {
            ref = this->ag.getActorNoThrow(std::get<1>(el));
        }

        AbstractInPort *in = main->getInPort(std::get<0>(el));

        if (ref.where() == upcxx::rank_me())
        {
            ActorImpl *ai = *ref.local();
            AbstractOutPort *out = ai->getOutPort(std::get<2>(el));

            main->resurrectConnection(std::get<0>(el), upcxx::rank_me(), out);
        }
        else
        {
            upcxx::future<AbstractOutPort *> fut = upcxx::rpc(
                ref.where(),
                [](GlobalActorRef ref, upcxx::intrank_t rank, std::string &&name)
                {
                    ActorImpl *ai = *ref.local();
                    AbstractOutPort *out = ai->getOutPort(name);
                    return out;
                },
                ref, upcxx::rank_me(), std::get<2>(el));

            upcxx::future<> a = fut.then([this, main, el, ref](AbstractOutPort *aptr) { main->resurrectConnection(std::get<0>(el), ref.where(), aptr); });
            futs.emplace_back(std::move(a));
        }
    }

    upcxx::future<> fut = allDone.finalize();
    upcxx::future<> tmp = util::combineFutures(futs);
    upcxx::future<> resurrected = upcxx::when_all(std::move(fut), std::move(tmp));

    upcxx::future<> toret = resurrected.then(
        [this, name]() -> upcxx::future<>
        {
            upcxx::promise<> allDone;

            const Node &middle = this->pg.getNode(name);
            const std::vector<std::tuple<std::string, std::string, std::string>> &outgoing =
                middle.outgoing; // connection of type op -> ip|B (saved as op,B,ip)
            const std::vector<std::tuple<std::string, std::string, std::string>> &incoming =
                middle.incoming; // connection of type C|op -> ip (saved as ip,C,op)

            for (const auto &el : incoming)
            {
                //// C|op -> ip (saved as ip,C,op)
                GlobalActorRef ref = this->ag.getActorNoThrow(std::get<1>(el));
                while (ref == nullptr)
                {
                    ref = this->ag.getActorNoThrow(std::get<1>(el));
                }

                if (ref.where() == upcxx::rank_me())
                {
                    this->ag.triggeredFlushBuffer((*ref.local())->getName());
                    this->ag.triggeredNotify((*ref.local())->getName());
                }
                else
                {
                    auto cx = upcxx::operation_cx::as_promise(allDone);
                    upcxx::rpc(
                        ref.where(), cx,
                        [](GlobalActorRef ref, upcxx::dist_object<DynamicActorGraph<A> *> &remoteComponents)
                        {
                            (*remoteComponents)->ag.triggeredFlushBuffer((*ref.local())->getName());
                            (*remoteComponents)->ag.triggeredNotify((*ref.local())->getName());
                        },
                        ref, remoteComponents);
                }
            }

            for (const auto &el : outgoing)
            {
                //// C|op -> ip (saved as ip,C,op)
                GlobalActorRef ref = this->ag.getActorNoThrow(std::get<1>(el));
                while (ref == nullptr)
                {
                    ref = this->ag.getActorNoThrow(std::get<1>(el));
                }

                if (ref.where() == upcxx::rank_me())
                {
                    this->ag.triggeredFlushBuffer((*ref.local())->getName());
                    this->ag.triggeredNotify((*ref.local())->getName());
                }
                else
                {
                    auto cx = upcxx::operation_cx::as_promise(allDone);
                    upcxx::rpc(
                        ref.where(), cx,
                        [](GlobalActorRef ref, upcxx::dist_object<DynamicActorGraph<A> *> &remoteComponents)
                        {
                            (*remoteComponents)->ag.triggeredFlushBuffer((*ref.local())->getName());
                            (*remoteComponents)->ag.triggeredNotify((*ref.local())->getName());
                        },
                        ref, remoteComponents);
                }
            }

            this->ag.triggeredFlushBuffer(name);
            this->ag.triggeredNotify(name);

            upcxx::future<> f = allDone.finalize();
            return f;
        });

    return toret;
}

template <typename A> upcxx::future<GlobalActorRef> DynamicActorGraph<A>::stealActorAsync(const std::string &name)
{
#ifndef NDEBUG
    assert(!name.empty());
#endif
    upcxx::intrank_t myrank = upcxx::rank_me();

    GlobalActorRef ref = this->ag.getActor(name);

    upcxx::intrank_t from = ref.where();

#ifdef REPORT_MAIN_ACTIONS
    std::cout << "Steal " << name << " from rank: " << from << " to: " << upcxx::rank_me() << std::endl;
#endif

    if (ref.where() != upcxx::rank_me())
    {
#ifndef NDEBUG
        assert(!name.empty());
#endif

#ifndef NDEBUG
        assert(ref.where() != upcxx::rank_me());
#endif

        // indicate a migration is occuring on rank this

        // std::set<int> borderingranks = this->pg.borderingRanks(name);

        // stop all neighbors
        std::set<std::string> acts = this->pg.getNeighbors(name);
        std::vector<GlobalActorRef> actrefs;
        actrefs.reserve(acts.size());
        // save globalactorrefs for furter use
        for (const std::string &s : acts)
        {
            GlobalActorRef neighbor = this->ag.getActor(s);
            actrefs.push_back(neighbor);
        }
        // wait, such that no messages are on the wire

        // stop and prepare actor for migration, stop the actor to be migrated after the others actor stopped because no act method is called when actor is
        // stopped but non-stopped actors can send it data (if user defines in such a way, that is the case in simulation actor)
        // stop act, wait later

        upcxx::future<> cs = this->severeConnections(name);

        upcxx::future<> send_tasks = upcxx::rpc(
            ref.where(),
            [](upcxx::dist_object<DynamicActorGraph<A> *> &remoteComponents, std::string name, int rank)
            { (*remoteComponents)->ag.taskDeque.going_away.insert(std::make_pair(name, rank)); },
            remoteComponents, name, upcxx::rank_me());

        upcxx::future<> connections_severed = upcxx::when_all(cs, send_tasks);

        upcxx::future<> uwu = connections_severed.then([name, this]() -> upcxx::future<> { return this->pg.changeRankAsyncFF(name); });

        upcxx::future<> prepared = uwu.then(
            [ref, this]() -> upcxx::future<>
            {
                return upcxx::rpc(
                    ref.where(),
                    [](GlobalActorRef ref)
                    {
                        ActorImpl *ai = *ref.local();
                        ai->prepare();
                        ai->prepareDerived();
                    },
                    ref);
            });

        upcxx::future<> rmed_from_map = prepared.then([name, this]() -> upcxx::future<> { return std::get<0>(this->ag.rmActorAsync(name)); });

        upcxx::future<GlobalActorRef> sent = prepared.then([ref, this]() -> upcxx::future<GlobalActorRef> { return sendActorToAsync(upcxx::rank_me(), ref); });

        upcxx::future<GlobalActorRef> rdy_to_reinsert = upcxx::when_all(rmed_from_map, sent);

        upcxx::future<GlobalActorRef> reinserted = rdy_to_reinsert.then(
            [name, this](GlobalActorRef ref) -> upcxx::future<GlobalActorRef>
            {
                assert(ref.where() == upcxx::rank_me());
                assert((*ref.local())->getName() == name);
                upcxx::future<> added = this->ag.addActorToAnotherAsync(ref, upcxx::rank_me());
                upcxx::future<GlobalActorRef> r = upcxx::make_future(ref);
                upcxx::future<GlobalActorRef> rr = upcxx::when_all(r, added);
                return rr;
                // return this->ag.changeRef(name, ref);
            });

        // upcxx::future<GlobalActorRef> old2 = upcxx::when_all(reinserted, old);

        // delete old actor
        upcxx::future<> deleted = sent.then( // old2.then(
            [ref](GlobalActorRef sent) -> upcxx::future<>
            {
#ifndef NDEBUG
                assert(ref != sent);
#endif
                GlobalActorRef old = ref;
                return upcxx::rpc(
                    old.where(),
                    [](GlobalActorRef old)
                    {
                        ActorImpl *aimpl = *old.local();
                        delete aimpl;
                        upcxx::delete_(old);
                    },
                    old);
            });

        // refill ports of the actor
        upcxx::future<GlobalActorRef> refilled = reinserted.then(
            [name, this](GlobalActorRef uwu) -> GlobalActorRef
            {
                GlobalActorRef ref = this->ag.getActor(name);
                if (ref != uwu)
                {
                    throw std::runtime_error("sent gut the ref in actorgraph is different");
                }
                ActorImpl *a = *ref.local();
                a->refillPorts();
                a->broadcastTaskDeque(this->ag.getTaskDeque());
                return ref;
            });

        upcxx::future<> reconn = refilled.then([name, this](GlobalActorRef uwu) { this->resurrectConnections(name, uwu); });

        // upcxx::future<> simp = refilled.then([name, this]() { this->ag.subscribe(name); });

        // restart the migrated actor
        upcxx::future<> started_main = reconn.then(
            [name, actrefs, myrank, this]()
            {
                GlobalActorRef ref = this->getActor(name);
                ActorImpl *aimpl = *ref.local();
                aimpl->startWithCaller(myrank); // this unpins
            });

        // unmark neighbors after reconnection
        upcxx::future<> neighborsback = started_main.then(
            [myrank, name, this]() -> upcxx::future<>
            {
                std::set<std::string> neighbors = this->pg.getNeighbors(name);
                std::vector<GlobalActorRef> actrefs;
                actrefs.reserve(neighbors.size());

                for (auto &&n : neighbors)
                {
                    actrefs.push_back(this->ag.getActor(n));
                }

                upcxx::promise<> allDone;
                for (GlobalActorRef neighbor : actrefs)
                {
                    if (neighbor.where() == upcxx::rank_me())
                    {
                        ActorImpl *ai = *neighbor.local();
                        ai->unpin(myrank);
                    }
                    else
                    {
                        int at = neighbor.where();
                        auto cx = upcxx::operation_cx::as_promise(allDone);
                        upcxx::rpc(
                            at, cx,
                            [](GlobalActorRef ref, upcxx::dist_object<DynamicActorGraph<A> *> &remoteComponents, upcxx::intrank_t rank)
                            {
                                ActorImpl *ai = *ref.local();
                                ai->unpin(rank);
                            },
                            neighbor, this->remoteComponents, myrank);
                    }
                }
                upcxx::future<> all = allDone.finalize();
                return all;
            });

        // migration ends so we need to indicate end for this rank
        upcxx::future<> ret = neighborsback.then(
            [this, name, from]()
            {
                upcxx::rpc_ff(
                    from,
                    [](upcxx::dist_object<DynamicActorGraph<A> *> &remoteComponents, std::string &&name, int rank)
                    {
                        (*remoteComponents)->moving.erase(name);
                        //(*remoteComponents)->ag.taskDeque.going_away.erase(std::make_pair(name, rank));
                    },
                    this->remoteComponents, name, upcxx::rank_me());
            });

        upcxx::future<GlobalActorRef> ret2 = ret.then([this, name]() -> GlobalActorRef { return this->ag.getActor(name); });
        return ret2;
    }
    else
    {
        throw std::runtime_error("no reason to steal an Actor that is on the same rank, stealActorAsyncImpl");
    }
}

template <class A> std::vector<std::string> DynamicActorGraph<A>::findAnActorToStealFromTheCrowdiestRank()
{

    std::vector<std::string> bordering_and_active_actors = pg.getActorsFromCrowdiestRank(1);

    while (moving.find(bordering_and_active_actors[0]) != moving.end())
    {
        bordering_and_active_actors = pg.getActorsFromCrowdiestRank(1);
    }

    return bordering_and_active_actors;
}

template <class A> std::vector<std::string> DynamicActorGraph<A>::findRandomActorsToSteal(size_t count)
{
    return this->pg.getRandomActors(count, upcxx::rank_me());
}

template <class A> std::vector<std::string> DynamicActorGraph<A>::findActorsToSteal()
{
    std::vector<std::string> bordering_and_active_actors{};
    // std::set<std::string> tmp;

    if constexpr (config::migration_strategy == config::MigrationStrategy::Expansion)
    {
        if (this->ag.actorCount == 0)
        {
            bordering_and_active_actors = this->pg.getRandomActors(20, upcxx::rank_me());
        }
        else
        {
            bordering_and_active_actors = pg.borderingActors(upcxx::rank_me());
        }
    }
    else
    {

        bordering_and_active_actors = this->pg.getRandomActors(20, upcxx::rank_me());
    }

    // all actors that border this rank

    int myself = upcxx::rank_me();

    if (bordering_and_active_actors.empty())
    {
        return {};
    }
    else
    {
        std::vector<std::string> diff;

        std::unique_lock<std::shared_mutex> diffLock(migrationMutex);

        GlobalActorRef ref = nullptr;
        int tries = 0;

        std::set_difference(bordering_and_active_actors.begin(), bordering_and_active_actors.end(), moving.begin(), moving.end(), std::back_inserter(diff));

        // remove actors that are on the same rank
        diff.erase(std::remove_if(diff.begin(), diff.end(),
                                  [this](const std::string &name) -> bool
                                  {
                                      GlobalActorRef ref = this->ag.getActorNoThrow(name);
                                      return (ref == nullptr) || ref.where() == upcxx::rank_me();
                                  }),
                   diff.end());
        // std::array<std::string, 1> out;

        return diff;
    }
}

template <class A> void DynamicActorGraph<A>::stopActors(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    for (auto &pr : migList)
    {
        GlobalActorRef ref = ag.getActor(pr.first);
        assert(ref.where() == upcxx::rank_me());
        ActorImpl *ai = *ref.local();
        bool st = ai->stopWithCaller(ActorState::TemporaryStoppedForMigration, util::rank_n() + 4);
        assert(st);
    }
}

/*
template <class A> upcxx::future<> DynamicActorGraph<A>::remoteRestartActors(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    std::vector<upcxx::future<>> futs;

    for (auto &pr : migList)
    {
        GlobalActorRef ref = ag.getActor(pr.first);

        if (ref.where() == upcxx::rank_me())
        {

            ActorImpl *ai = *ref.local();
            bool st = ai->startWithCaller(upcxx::rank_n() + 4);

#ifndef NDEBUG
            if (!st)
            {
                std::cout << ai->getName() << " is marked by and cannot unmark: " << ai->getMark() << std::endl;
                assert(false);
            }
#endif
            assert(st);
        }
    }

    return util::combineFutures(futs);
}
*/

template <class A> upcxx::future<> DynamicActorGraph<A>::restartActors(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    std::vector<upcxx::future<>> futs;

    for (auto &pr : migList)
    {
        GlobalActorRef ref = ag.getActor(pr.first);

        if (ref.where() == upcxx::rank_me())
        {

            ActorImpl *ai = *ref.local();

            if (ai->getRunningState() != ActorState::Terminated && ai->getRunningState() != ActorState::WantsToTerminate)
            {

                bool st = ai->startWithCaller(upcxx::rank_n() + 4);

#ifndef NDEBUG
                if (!st)
                {
                    std::cout << ai->getName() << " is marked by and cannot unmark: " << ai->getMark() << std::endl;
                    assert(false);
                }
#endif
                assert(st);
            }
        }
    }

    return util::combineFutures(futs);
}

template <typename A>
std::tuple<upcxx::future<>, std::vector<GlobalActorRef>> DynamicActorGraph<A>::rmOldActors(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    std::vector<upcxx::future<>> futs;
    std::vector<GlobalActorRef> v;
    for (auto &pr : migList)
    {
        GlobalActorRef r = this->ag.getActor(pr.first);
        assert(r.where() == upcxx::rank_me());

        ActorImpl *act = *r.local();
        auto tup = this->ag.rmActorAsync(act->getName());
        futs.push_back(std::get<0>(tup));
        v.push_back(std::get<1>(tup));
    }

    if (!futs.empty())
    {
        return {util::combineFutures(futs), v};
    }

    return {upcxx::make_future(), v};
}

template <typename A> void DynamicActorGraph<A>::delRemains(const std::vector<GlobalActorRef> &l)
{
    for (auto &el : l)
    {
        assert(el.where() == upcxx::rank_me());
        ActorImpl *a = *(el.local());
        delete a;
        upcxx::delete_(el);
    }
}

template <typename A>
std::vector<GlobalActorRef> DynamicActorGraph<A>::sendActors(const std::vector<GlobalActorRef> &trigs,
                                                             const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{

    assert(actors_after_mig.empty());
    std::vector<GlobalActorRef> refs;
    refs.reserve(trigs.size());
    size_t offset = 0;
    for (size_t i = 0; i < trigs.size(); i++)
    {
        auto &el = trigs[i];
        assert(el.where() == upcxx::rank_me());

        ActorImpl *ai = *el.local();
        std::string name = ai->getName();
        auto pr = *migList.find(name);
        GlobalActorRef nref = this->sendActorToAsync(pr.second, el).wait();
        refs.emplace_back(nref);
    }

    return refs;
}

template <typename A>
upcxx::future<> DynamicActorGraph<A>::sendActorsAsync(const std::vector<GlobalActorRef> &trigs,
                                                      const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    assert(actors_after_mig.empty());
    std::vector<upcxx::future<>> futs;
    futs.reserve(migList.size());

    for (size_t i = 0; i < trigs.size(); i++)
    {
        auto &el = trigs[i];
        assert(el.where() == upcxx::rank_me());
    }
    actors_after_mig.resize(trigs.size());

    size_t cur_iter = this->iter;
    this->iter++;
    size_t offset = 0;
    for (size_t i = 0; i < trigs.size(); i++)
    {
        auto &el = trigs[i];
        assert(el.where() == upcxx::rank_me());

        ActorImpl *ai = *el.local();
        std::string name = ai->getName();
        auto pr = *migList.find(name);
        upcxx::future<GlobalActorRef> nref = this->sendActorToAsync(pr.second, el);
        upcxx::future<> f = nref.then(
            [cur_iter, offset, this](GlobalActorRef r)
            {
                if (this->actors_after_mig[offset] != nullptr)
                {
                    throw std::runtime_error("Fix this: bulk sending iteration difference");
                }

                this->actors_after_mig[offset] = r;
                return;
            });

        offset += 1;
        futs.emplace_back(f);
    }

    return util::combineFutures(futs);
}

template <typename A> void DynamicActorGraph<A>::refillPorts(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    if (migList.empty())
    {
        return;
    }

    for (auto &pr : migList)
    {
        GlobalActorRef ref = this->ag.getActor(pr.first);
        if (ref.where() == upcxx::rank_me())
        {
            ActorImpl *ai = *ref.local();

            if (ai->getRunningState() != ActorState::Terminated && ai->getRunningState() != ActorState::WantsToTerminate)
            {
                ai->refillPorts();
            }
        }
    }
}

// reinserts the actors in the vector to the actorgraph of the other rank
template <typename A> upcxx::future<> DynamicActorGraph<A>::reinsert(const std::vector<GlobalActorRef> &refs)
{
    std::vector<upcxx::future<>> futs;
    for (auto &el : refs)
    {
        if (el.where() == upcxx::rank_me())
        {
            throw std::runtime_error("Actor was at rank:" + std::to_string(el.where()) + " my rank: " + std::to_string(upcxx::rank_me()) +
                                     ", sent to your own rank wut?");
        }
        else
        {
#ifndef NDEBUG
            std::cout << "reinsert to " << el.where() << std::endl;
#endif
            futs.push_back(this->ag.addActorToAnotherAsync(el, el.where()));
        }
    }

    return util::combineFutures(futs);
}

template <typename A>
std::unordered_map<std::string, upcxx::intrank_t> DynamicActorGraph<A>::cleanseMigrationList(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    std::unordered_map<std::string, upcxx::intrank_t> cleansed;

    for (const auto &pr : migList)
    {
        GlobalActorRef ar = this->ag.getActor(pr.first);
        if (ar.where() != pr.second)
        {
            cleansed.insert(pr);
        }
    }

    return cleansed;
}

template <typename A>
std::unordered_map<std::string, upcxx::intrank_t>
DynamicActorGraph<A>::createLocalMigrationList(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    std::unordered_map<std::string, upcxx::intrank_t> cleansed;

    for (const auto &pr : migList)
    {

        GlobalActorRef ar = this->ag.getActor(pr.first);
        if (ar.where() != pr.second && ar.where() == upcxx::rank_me())
        {
            ActorImpl *ai = *ar.local();
            std::cout << ai->getName() << " is at " << ar.where() << " should go to " << pr.second << std::endl;
            if (!ai->isMarked() && !ai->isPinned() && ai->getRunningState() != ActorState::Terminated && ai->getRunningState() != ActorState::WantsToTerminate)
            {
                std::cout << ai->getName() << " can be migrated to " << pr.second << std::endl;
                cleansed.insert(pr);
            }
            else
            {
                std::cout << ai->getName() << " is marked | pinned | terminated, can't be migrated " << pr.second << std::endl;
            }
        }
    }

    return cleansed;
}

template <typename A> void DynamicActorGraph<A>::migrateActorsDiscretePhases(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    auto allActorsToMigrate = cleanseMigrationList(migList);

    auto localActorsToMigrate = createLocalMigrationList(allActorsToMigrate);

    // clist contains only the actors that should be sent from my rank to other ranks

    stopActors(localActorsToMigrate);

    upcxx::barrier();

    upcxx::future<> l = disconnectFromNeighboursAsync(localActorsToMigrate);
    l.wait();

    prepareActors(localActorsToMigrate);

    upcxx::barrier();

    std::tuple<upcxx::future<>, std::vector<GlobalActorRef>> rmedTup = rmActorsAsync(localActorsToMigrate);

    upcxx::future<> rmed = std::get<0>(rmedTup);
    std::vector<GlobalActorRef> refs = std::move(std::get<1>(rmedTup));
    rmed.wait();

    upcxx::barrier();

    std::vector<GlobalActorRef> sentrefs = sendActors(refs, localActorsToMigrate);

    upcxx::barrier();

    upcxx::future<> added = addActorsToAnotherAsync(sentrefs);
    added.wait();

    upcxx::barrier();

    auto resurrected = reconnectToNeighboursAsync(allActorsToMigrate);
    resurrected.wait();

    refillPorts(allActorsToMigrate);

    upcxx::barrier();

    sendTasks(localActorsToMigrate).wait();

    auto fk = restartActors(allActorsToMigrate);
    fk.wait();

    upcxx::barrier();
}

template <typename A> void DynamicActorGraph<A>::clearSentActorReferences() { actors_after_mig.clear(); }

template <typename A>
std::tuple<upcxx::future<>, std::vector<GlobalActorRef>> DynamicActorGraph<A>::rmActorsAsync(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    std::vector<upcxx::future<>> futs;
    futs.reserve(migList.size());
    std::vector<GlobalActorRef> refs;
    refs.reserve(migList.size());

    for (const auto &el : migList)
    {
        GlobalActorRef ref = this->ag.getActor(el.first);

        if (ref.where() == upcxx::rank_me())
        {
            std::tuple<upcxx::future<>, GlobalActorRef> tup = rmActorAsync(el.first);
            futs.emplace_back(std::move(std::get<0>(tup)));
            refs.emplace_back(std::move(std::get<1>(tup)));
        }
    }

    upcxx::future<> all = util::combineFutures(futs);

    return {all, refs};
}

template <typename A> upcxx::future<> DynamicActorGraph<A>::addActorsToAnotherAsync(const std::vector<GlobalActorRef> &refs)
{
    std::vector<upcxx::future<>> l;
    l.reserve(refs.size());

    for (const auto &el : refs)
    {
        assert(el.where() != upcxx::rank_me());
        upcxx::future<> f = addActorToAnotherAsync(el, el.where());
        l.emplace_back(std::move(f));
    }

    upcxx::future<> all = util::combineFutures(l);
    return all;
}

template <typename A> upcxx::future<> DynamicActorGraph<A>::sendTasks(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    return this->ag.sendTasks(migList);
}