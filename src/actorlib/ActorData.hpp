#pragma once
#include "ActorState.hpp"
#include <iostream>
#include <set>
#include <string>
#include <upcxx/upcxx.hpp>
#include <utility>
/*
    Actor and ActorImpl are abstract classes so they are serialized to ActorData class
    which describes the state of an actor, which is used to initialize an ACtor on the
    new rank
*/

class ActorData
{
  public:
    std::string name; // name
    int nomadiness;
    size_t workTokens; // counted tokens till now
    size_t workTime;   // time spent in act() till now
    upcxx::intrank_t mark;
    ActorState state;

    ActorData(std::string &&name, int inomadiness, size_t workTokens, size_t workTime, upcxx::intrank_t &&mark, ActorState state) noexcept
        : name(std::move(name)), nomadiness(inomadiness + 1), workTokens(workTokens), workTime(workTime), mark(std::move(mark)), state(state)
    {
    }

    ActorData(const std::string &name, int inomadiness, size_t workTokens, size_t workTime, const upcxx::intrank_t &mark, ActorState state) noexcept
        : name(name), nomadiness(inomadiness + 1), workTokens(workTokens), workTime(workTime), mark(mark), state(state)
    {
    }

    ActorData(ActorData &&ad) noexcept
        : name(std::move(ad.name)), nomadiness(ad.nomadiness + 1), workTokens(ad.workTokens), workTime(ad.workTime), mark(ad.mark), state(ad.state)
    {
    }

    ActorData(const ActorData &ad) noexcept
        : name(ad.name), nomadiness(ad.nomadiness + 1), workTokens(ad.workTokens), workTime(ad.workTime), mark(ad.mark), state(ad.state)
    {
    }

    ~ActorData() noexcept = default;

        struct upcxx_serialization
    {
        template <typename Writer> static void serialize(Writer &writer, ActorData const &object)
        {
            writer.write(object.name);
            writer.write(object.nomadiness);
            writer.write(object.workTokens);
            writer.write(object.workTime);
            writer.write(object.mark);
            writer.write(object.state);

            return;
        }

        template <typename Reader> static ActorData *deserialize(Reader &reader, void *storage)
        {
            std::string name = reader.template read<std::string>();
            int nomadiness = reader.template read<int>();
            uint64_t workTokens = reader.template read<uint64_t>();
            uint64_t workTime = reader.template read<uint64_t>();
            auto rc = reader.template read<upcxx::intrank_t>();
            ActorState as = reader.template read<ActorState>();

            ActorData *v =
                ::new (storage) ActorData(std::move(name), nomadiness, workTokens, workTime, std::move(rc), as);

            return v;
        }
    };
};