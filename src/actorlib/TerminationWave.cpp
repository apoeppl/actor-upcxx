#include "TerminationWave.hpp"
#include "ActorGraph.hpp"
#include "TaskDeque.hpp"

TerminationWave::TerminationWave(const ActorGraph *pag, TaskDeque *ptd) noexcept
    : checked(0), n(util::rank_n()), remoteSources(this), pag(pag), completed_count(0), total_msg_received(0), msg_id(0), received{}, ptd(ptd)
{
    received.resize(util::rank_n());
    std::fill(received.begin(), received.end(), std::make_pair(0, -1));
}

bool TerminationWave::terminated() { return this->pag->getActiveActors() == 0 && this->ptd->hasNoEmigrantTasks() == 0 && this->ptd->nothingIsComing(); }

bool TerminationWave::otherHasFullTerminated()
{
    for (auto &el : received)
    {
        if (el.first == ~(size_t)0 && el.second == 1)
        {
            return true;
        }
    }
    return false;
}

bool TerminationWave::checkTermination()
{
    checked += 1;

    if (otherHasFullTerminated())
    {
        if (!terminated())
        {
            throw std::runtime_error("Somebody else terminated, even though I am not?");
        }
        else
        {
            return true;
        }
    }

#ifdef REPORT_MAIN_ACTIONS
    // std::cout << upcxx::rank_me() << " is checking for global termination" << std::endl;
#endif

    if (!terminated())
    {
#ifdef REPORT_MAIN_ACTIONS
        /*
        if (this->pag->getActiveActors() != 0)
        {
            this->pag->printActorStates();
        }

        if (this->ptd->tasks.size() != 0)
        {
            {
                std::cout << upcxx::rank_me() << "{";
                for (auto &el : this->ptd->tasks)
                {
                    std::cout << "(" << el.source << ", " << el.finished << ",  {";

                    for (auto &x : el.uniques)
                    {
                        std::cout << x << ", ";
                    }
                    std::cout << "}, " << el.unique_needed << ") ";
                }
                std::cout << "}" << std::endl;
            }
        }
        */

        // std::cout << upcxx::rank_me() << " Self is not terminated, active actor count:" << this->pag->getActiveActors()
        //          << " task count: " << this->ptd->tasks.size() << " needs to send " << this->ptd->hasNoEmigrantTasks() << " tasks "
        //          << ", and coming status: " << !this->ptd->nothingIsComing() << std::endl;
#endif
        return false;
    }
    else
    {
#ifdef REPORT_MAIN_ACTIONS
        std::cout << upcxx::rank_me() << " is termianted, check others" << std::endl;
#endif
    }

    msg_id += 1;

    received[upcxx::rank_me()] = std::make_pair(msg_id, 1);

    for (int i = 0; i < util::rank_n(); i++)
    {
        if (upcxx::rank_me() == i)
        {
            continue;
        }
        else
        {
            this->ptd->parentActorGraph->rpcsInFlight += 1;
            upcxx::future<bool> rank_i_terminated = upcxx::rpc(
                i, [](upcxx::dist_object<TerminationWave *> &tw) { return (*tw)->terminated(); }, remoteSources);

            rank_i_terminated.then(
                [this, i](bool terminated)
                {
                    auto &pr = this->received[i];

                    if (pr.first < this->msg_id)
                    {
                        int l = terminated ? 1 : 0;
                        pr.second = l;
                        pr.first = msg_id;
                    }

                    this->ptd->parentActorGraph->rpcsInFlight -= 1;
                });
        }
    }
    upcxx::discharge();

    do
    {
        upcxx::progress();

        int got = 0;
        for (int i = 0; i < util::rank_n(); i++)
        {
            if (i == upcxx::rank_me())
            {
                continue;
            }

            if (received[i].first >= msg_id && received[i].second == 0)
            {
#ifdef PREPORT_MAIN_ACTIONS
                std::cout << "not globally terminated" << std::endl;
#endif
                return false;
            }

            if (received[i].first >= msg_id && received[i].second == 1)
            {
                got += 1;
            }
        }

        if (got == util::rank_n() - 1)
        {
            upcxx::promise<> all;
            for (int i = 0; i < util::rank_n(); i++)
            {
                if (upcxx::rank_me() == i)
                {
                    continue;
                }
                else
                {
                    auto cx = upcxx::operation_cx::as_promise(all);
                    upcxx::rpc(
                        i, cx, [](upcxx::dist_object<TerminationWave *> &tw, size_t i) { (*tw)->received[i] = std::make_pair(~(size_t)0, 1); }, remoteSources,
                        i);
                }
            }
            upcxx::future<> a = all.finalize();
            upcxx::discharge();
            a.wait();
            return true;
        }

    } while (true);
}