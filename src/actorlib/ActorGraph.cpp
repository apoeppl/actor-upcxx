/*
    X       x
        X
    X       X

*/

/**
 * @file
 * This file is part of actorlib.
 *
 * @author Alexander Pöppl (poeppl AT in.tum.de,
 * https://www5.in.tum.de/wiki/index.php/Alexander_P%C3%B6ppl,_M.Sc.)
 *
 * @section LICENSE
 *
 * actorlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * actorlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with actorlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @section DESCRIPTION
 *
 * TODO
 */

#include "ActorGraph.hpp"
#include "AbstractInPort.hpp"
#include "AbstractOutPort.hpp"
#include "ActorImpl.hpp"
#include "Channel.hpp"
#include "InPort.hpp"
#include "OutPort.hpp"
#include "SerializeOptional.hpp"
#include "config.hpp"
#include <chrono>

//#define DEBUG_ACTOR_TERMINATION
#define ACTOR_PARALLEL_TERMINATION_HACK

using namespace std;

GlobalChannelRef connectDestination(GlobalActorRef destinationActor, std::string destinationPortName);
upcxx::future<> connectSource(GlobalActorRef sourceActor, std::string sourcePortName, GlobalChannelRef channelRef);
GlobalChannelRef disconnectDestination(GlobalActorRef destinationActor, std::string destinationPortName);
upcxx::future<> disconnectSource(GlobalActorRef sourceActor, std::string sourcePortName);

// ActorGraph instance methods

ActorGraph::ActorGraph(MigrationDispatcher *dag, PortGraph *pg)
    : actorCountOnRanks(util::rank_n()), actors(), migcaller(dag), taskDeque(this), asp(), remoteGraphComponents(this), masterPersona(upcxx::master_persona()),
      totalTime(0), totalTokens(0), pg(pg), locallyActiveActors{},

#ifdef PARALLEL
      actorLock(), activeActorCount(0), rpcsInFlight(0), lpcsInFlight(0)
#else
      activeActorCount(0), rpcsInFlight(0), lpcsInFlight(0)
#endif
{
    if (!upcxx::rank_me())
    {
        std::cout << config::configToString();
    }
}

void ActorGraph::addActor(Actor *a) { addActor(&(a->asBase())); }

void ActorGraph::addActor(ActorImpl *a) { addActorAsync(a).wait(); }

upcxx::future<> ActorGraph::addActorAsync(Actor *a) { return addActorAsync(&(a->asBase())); }

upcxx::future<> ActorGraph::addActorAsync(ActorImpl *a)
{
    auto aGPtr = upcxx::new_<ActorImpl *>(a);
    a->parentActorGraph = this;
    a->broadcastTaskDeque(&(this->taskDeque));
    locallyActiveActors.insert(a);
    actorCount += 1;
    checkInsert(a->name, aGPtr);

    upcxx::promise<> allDone;
    for (int i = 0; i < upcxx::rank_n(); i++)
    {
        if (i == upcxx::rank_me())
        {
            continue;
        }
        auto cx = upcxx::operation_cx::as_promise(allDone);
        upcxx::rpc(
            i, cx,
            [](upcxx::dist_object<ActorGraph *> &rag, GlobalActorRef newActorRef, std::string &&newActorName)
            { (*rag)->checkInsert(newActorName, newActorRef); },
            remoteGraphComponents, aGPtr, a->name);
    }
    upcxx::future<> fut = allDone.finalize();
    return fut;
}

// ActorImpl must be initialized at the rank "rank"
upcxx::future<> ActorGraph::addActorToAnotherAsync(GlobalActorRef a, upcxx::intrank_t rank)
{
    if (rank != a.where())
    {
        throw std::runtime_error("precondition: ActorImpl must be initialized at "
                                 "destination rank no fulfilled");
    }

    if (rank == upcxx::rank_me())
    {
        ActorImpl *act = *a.local();
        act->parentActorGraph = this;
        act->broadcastTaskDeque(&(this->taskDeque));
        locallyActiveActors.insert(act);
        this->checkInsert(act->getName(), a);
        actorCount += 1;

        upcxx::promise<> allDone;
        for (int i = 0; i < upcxx::rank_n(); i++)
        {
            if (i == upcxx::rank_me())
            {
                continue;
            }
            auto cx = upcxx::operation_cx::as_promise(allDone);
            upcxx::rpc(
                i, cx,
                [](upcxx::dist_object<ActorGraph *> &rag, GlobalActorRef newActorRef, std::string &&newActorName)
                { (*rag)->checkInsert(newActorName, newActorRef); },
                remoteGraphComponents, a, act->getName());
        }
        return allDone.finalize();
    }
    else
    {
        upcxx::future<std::string> nm = upcxx::rpc(
            a.where(),
            [](GlobalActorRef ref)
            {
                assert(ref != nullptr);
                ActorImpl *ai = *ref.local();
                return ai->getName();
            },
            a);

        upcxx::future<> lk = nm.then([this, a](const std::string &name) { this->checkInsert(name, a); });

        upcxx::future<> xcc = nm.then(
            [this, a](const std::string &name)
            {
                std::vector<upcxx::future<>> xf;
                xf.reserve(util::rank_n());
                upcxx::future<> t = upcxx::rpc(
                    a.where(),
                    [](upcxx::dist_object<ActorGraph *> &rag, GlobalActorRef ref)
                    {
                        assert(ref != nullptr);
                        ActorImpl *a = *ref.local();
                        a->parentActorGraph = (*rag);
                        //(*rag)->locallyActiveActors.insert(a);
                        a->broadcastTaskDeque(&(*rag)->taskDeque);
                        (*rag)->checkInsert(a->getName(), ref);
                        (*rag)->actorCount += 1;
                        assert(a->state != ActorState::Running);
                    },
                    remoteGraphComponents, a);
                xf.emplace_back(std::move(t));

                for (int i = 0; i < upcxx::rank_n(); i++)
                {
                    if (i == upcxx::rank_me() || i == a.where())
                    {
                        continue;
                    }
                    upcxx::future<> v = upcxx::rpc(
                        i,
                        [](upcxx::dist_object<ActorGraph *> &rag, GlobalActorRef newActorRef, std::string &&newActorName)
                        { (*rag)->checkInsert(newActorName, newActorRef); },
                        remoteGraphComponents, a, name);
                    xf.emplace_back(std::move(v));
                }

                return util::combineFutures(xf);
            });

        return upcxx::when_all(lk, xcc);
    }
}

std::tuple<upcxx::future<>, GlobalActorRef> ActorGraph::rmActorAsync(const std::string &name) { return rmActorAsync(name, ActorState::Terminated); }

std::tuple<upcxx::future<>, GlobalActorRef> ActorGraph::rmActorAsync(const std::string &name, ActorState as)
{
    GlobalActorRef a = actors.find(name)->second;

    if (a.where() == upcxx::rank_me())
    {
        ActorImpl *x = *a.local();

        // should already be stopped
        // x->stopWithCaller(x, as);
        locallyActiveActors.erase(x);
        bool couldrm = checkRm(x->getName());
        actorCount -= 1;

        if (!couldrm)
        {
            throw std::runtime_error("ActorImpl not in actorgraph, same rank, rmActor");
        }

        upcxx::promise<> allDone;
        for (int i = 0; i < upcxx::rank_n(); i++)
        {
            if (i == upcxx::rank_me())
            {
                continue;
            }
            auto cx = upcxx::operation_cx::as_promise(allDone);
            upcxx::rpc(
                i, cx, [](upcxx::dist_object<ActorGraph *> &rag, const std::string &actorName) { (*rag)->checkRm(actorName); }, remoteGraphComponents,
                x->getName());
        }
        upcxx::future<> fut = allDone.finalize();
        return {fut, a};
    }
    else
    {
        upcxx::promise<> allDone;
        bool couldrm = checkRm(name);
        if (!couldrm)
        {
            throw std::runtime_error("ActorImpl not in actorgraph, diff rank, rmActor");
        }

        auto cx = upcxx::operation_cx::as_promise(allDone);
        for (int i = 0; i < upcxx::rank_n(); i++)
        {
            if (i == upcxx::rank_me())
            {
                continue;
            }

            if (i == a.where())
            {
                upcxx::rpc(
                    i, cx,
                    [](upcxx::dist_object<ActorGraph *> &rag, std::string &&actorName)
                    {
                        GlobalActorRef aref = (*rag)->getActor(actorName);
                        ActorImpl *a = *(aref.local());
                        (*rag)->checkRm(actorName);
                        (*rag)->actorCount -= 1;
                    },
                    remoteGraphComponents, name);
            }
            else
            {
                upcxx::rpc(
                    i, cx, [](upcxx::dist_object<ActorGraph *> &rag, std::string &&actorName) { (*rag)->checkRm(actorName); }, remoteGraphComponents, name);
            }
        }
        auto fut = allDone.finalize();
        return {fut, a};
    }
}

void ActorGraph::rmActor(const std::string &name)
{
    auto tup = rmActorAsync(name);
    std::get<0>(tup).wait();
}

bool ActorGraph::checkRm(const std::string &actorName)
{
    if (this->actors.find(actorName) == this->actors.end())
    {
        throw std::runtime_error("Cannot rm what does not exist");
    }
#ifdef PARALLEL
    this->actorLock.lock();
#endif

    this->actorCountOnRanks[this->actors.find(actorName)->second.where()] -= 1;
    this->actors.erase(actorName);

#ifdef PARALLEL
    this->actorLock.unlock();
#endif

    return true;
}

void ActorGraph::checkInsert(const string &actorName, GlobalActorRef actorRef)
{
    if (this->actors.find(actorName) != this->actors.end())
    {
        throw std::runtime_error("Cannot add what does already exist");
    }
#ifdef PARALLEL
    this->actorLock.lock();
#endif

    this->actorCountOnRanks[actorRef.where()] += 1;
    this->actors.emplace(actorName, actorRef);

#ifdef PARALLEL
    this->actorLock.unlock();
#endif
}

void ActorGraph::connectPorts(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                              const std::string &destinationPortName)
{
    assert(!sourcePortName.empty());
    assert(!destinationPortName.empty());
    if (destinationActor.where() == upcxx::rank_me())
    {
        connectFromDestination(sourceActor, sourcePortName, destinationActor, destinationPortName).wait();
    }
    else if (sourceActor.where() == upcxx::rank_me())
    {
        connectFromSource(sourceActor, sourcePortName, destinationActor, destinationPortName).wait();
    }
    else
    {
        connectFromThird(sourceActor, sourcePortName, destinationActor, destinationPortName).wait();
    }
}

void ActorGraph::disconnectPorts(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                 const std::string &destinationPortName)
{

    disconnectPortsAsync(sourceActor, sourcePortName, destinationActor, destinationPortName).wait();
}

upcxx::future<> ActorGraph::disconnectPortsAsync(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                                 const std::string &destinationPortName)
{
    assert(!sourcePortName.empty());
    assert(!destinationPortName.empty());
    if (destinationActor.where() == upcxx::rank_me())
    {
        return disconnectFromDestination(sourceActor, sourcePortName, destinationActor, destinationPortName);
    }
    else if (sourceActor.where() == upcxx::rank_me())
    {
        return disconnectFromSource(sourceActor, sourcePortName, destinationActor, destinationPortName);
    }
    else
    {
        return disconnectFromThird(sourceActor, sourcePortName, destinationActor, destinationPortName);
    }
}

upcxx::future<> ActorGraph::connectPortsAsync(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                              const std::string &destinationPortName)
{
    assert(!sourcePortName.empty());
    assert(!destinationPortName.empty());

    if (destinationActor.where() == upcxx::rank_me())
    {
        return connectFromDestination(sourceActor, sourcePortName, destinationActor, destinationPortName);
    }
    else if (sourceActor.where() == upcxx::rank_me())
    {
        return connectFromSource(sourceActor, sourcePortName, destinationActor, destinationPortName);
    }
    else
    {
        return connectFromThird(sourceActor, sourcePortName, destinationActor, destinationPortName);
    }
}

upcxx::future<> ActorGraph::connectFromDestination(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                                   const std::string &destinationPortName)
{
    GlobalChannelRef c = connectDestination(destinationActor, destinationPortName);
    auto srcFut = upcxx::rpc(sourceActor.where(), connectSource, sourceActor, sourcePortName, c);
    return srcFut;
}

upcxx::future<> ActorGraph::connectFromSource(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                              const std::string &destinationPortName)
{
    upcxx::future<GlobalChannelRef> channelFut = upcxx::rpc(destinationActor.where(), connectDestination, destinationActor, destinationPortName);
    upcxx::future<> sourceDone = channelFut.then(
        [=](GlobalChannelRef c)
        {
            upcxx::future<> sourceRpcDone = upcxx::rpc(sourceActor.where(), connectSource, sourceActor, sourcePortName, c);
            return sourceRpcDone;
        });
    return sourceDone;
}

upcxx::future<> ActorGraph::connectFromThird(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                             const std::string &destinationPortName)
{
    upcxx::future<> allDone = upcxx::rpc(
        sourceActor.where(),
        [](upcxx::dist_object<ActorGraph *> &rag, GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
           std::string destinationPortName) { return (*rag)->connectFromSource(sourceActor, sourcePortName, destinationActor, destinationPortName); },
        this->remoteGraphComponents, sourceActor, sourcePortName, destinationActor, destinationPortName);
    return allDone;
}

upcxx::future<> ActorGraph::disconnectFromDestination(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                                      const std::string &destinationPortName)
{
    auto srcFut = upcxx::rpc(sourceActor.where(), disconnectSource, sourceActor, sourcePortName);
    return srcFut;
}

upcxx::future<> ActorGraph::disconnectFromSource(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                                 const std::string &destinationPortName)
{
    upcxx::future<GlobalChannelRef> channelFut = upcxx::rpc(destinationActor.where(), disconnectDestination, destinationActor, destinationPortName);
    upcxx::future<> sourceDone = channelFut.then(
        [sourceActor, sourcePortName](GlobalChannelRef c)
        {
            upcxx::future<> sourceRpcDone = upcxx::rpc(sourceActor.where(), disconnectSource, sourceActor, sourcePortName);
            return sourceRpcDone;
        });
    return sourceDone;
}

upcxx::future<> ActorGraph::disconnectFromThird(GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
                                                const std::string &destinationPortName)
{
    upcxx::future<> allDone = upcxx::rpc(
        sourceActor.where(),
        [](upcxx::dist_object<ActorGraph *> &rag, GlobalActorRef sourceActor, const std::string &sourcePortName, GlobalActorRef destinationActor,
           std::string destinationPortName) { return (*rag)->disconnectFromSource(sourceActor, sourcePortName, destinationActor, destinationPortName); },
        this->remoteGraphComponents, sourceActor, sourcePortName, destinationActor, destinationPortName);
    return allDone;
}

size_t ActorGraph::getTotalActorCount() const { return actors.size(); }

GlobalActorRef ActorGraph::getActor(const string &name) const
{
    auto entry = actors.find(name);
    if (entry != actors.end())
    {
        return entry->second;
    }
    else
    {
        throw std::runtime_error(std::to_string(upcxx::rank_me()) + ": unable to find ActorImpl named "s + name);
    }
}

bool ActorGraph::has(const string &name) const noexcept
{
    auto entry = actors.find(name);
    if (entry != actors.end())
    {
        return true;
    }
    else
    {
        return false;
    }
}

GlobalActorRef ActorGraph::getActorNoThrow(const std::string &name) const noexcept
{
    auto entry = actors.find(name);
    if (entry != actors.end())
    {
        return entry->second;
    }
    else
    {
        return nullptr;
    }
}

string ActorGraph::prettyPrint()
{
    stringstream ss;
    ss << "ActorGraph {" << endl;
    ss << "  Actors {" << endl;

    for (auto &kv : this->actors)
    {
        ss << "    " << kv.first << "\t" << kv.second << endl;
    }
    ss << "  }" << endl;
#ifdef PARALLEL
    ss << "ActiveActors: " << activeActorCount.load() << endl;
#else
    ss << "ActiveActors: " << activeActorCount << endl;
#endif
    ss << "TriggerCounts {" << endl;
    ss << " }" << endl;
    ss << "}" << endl;

    return ss.str();
}

std::pair<double, bool> ActorGraph::ompRun(double seconds)
{
    double runTime = 0.0;
    double finished = true;
    if (config::parallelization == config::ParallelizationType::OMP_TASKS)
    {
        auto start = std::chrono::steady_clock::now();

        generateAndAddTasks();
        finished = taskDeque.advance(seconds);

        auto end = std::chrono::steady_clock::now();
        runTime = std::chrono::duration<double, std::ratio<1>>(end - start).count();

#ifdef PARALLEL
        while (rpcsInFlight.load() > 0 || lpcsInFlight.load() > 0)
#else
        while (rpcsInFlight > 0 || lpcsInFlight > 0)
#endif
        {
            for (const auto &at : locallyActiveActors)
            {
                auto scope = upcxx::persona_scope(at->actorPersona);
#pragma omp parallel
                {
                    upcxx::progress();
                }
                upcxx::progress();
            }
            upcxx::progress();
        }
    }
    else
    {
        throw std::runtime_error("OMP run called withput OMP Config!");
    }

    return std::make_pair(runTime, finished);
}

std::pair<double, bool> ActorGraph::serialRun(double seconds)
{
    bool finished = true;
    if (config::parallelization == config::ParallelizationType::UPCXX_RANKS)
    {
#ifdef CENTRALIZED_MIGRATION
        upcxx::barrier();

        if (upcxx::rank_me() < upcxx::rank_n() - 1)
        {
            auto start = std::chrono::steady_clock::now();

            generateAndAddTasks();
            finished = taskDeque.advance(seconds);

            auto end = std::chrono::steady_clock::now();
            double runTime = std::chrono::duration<double, std::ratio<1>>(end - start).count();

            upcxx::rpc_ff(
                upcxx::rank_n() - 1, [](upcxx::dist_object<ActorGraph *> &rag) { (*rag)->terminated += 1; }, remoteGraphComponents);
            //.wait();

            upcxx::discharge();

            return std::make_pair(runTime, finished);
        }
        else
        {
            while (terminated < static_cast<size_t>(util::rank_n()))
            {
                // this is needed for migration master, all calls to migration master are handled in upcxx progress due to their nature

                // std::cout << "uwu" << std::endl;
                upcxx::progress();
            }
            std::cout << "all other ranks termianted, bye" << std::endl;
            return std::make_pair(-1.0, true);
        }
#else
        auto start = std::chrono::steady_clock::now();

        generateAndAddTasks();
        finished = taskDeque.advance(seconds);

        auto end = std::chrono::steady_clock::now();
        double runTime = std::chrono::duration<double, std::ratio<1>>(end - start).count();

        return std::make_pair(runTime, finished);
#endif
    }
    else
    {
        throw std::runtime_error("Serial Run should not be called with parallel UPCXX / OMP Config");
    }
}

std::pair<double, bool> ActorGraph::run(double opt_seconds, bool firsttime)
{
    this->migrationphase = false;
    double runTime = 0.0;
    bool finished = false;

    if (firsttime)
    {
        this->startActors();

        upcxx::barrier();

#ifdef PARALLEL
        assert(rpcsInFlight.load() == 0 && lpcsInFlight.load() == 0);
#else
        assert(rpcsInFlight == 0 && lpcsInFlight == 0);
#endif
        firsttime = false;
    }

    if constexpr (config::parallelization == config::ParallelizationType::UPCXX_RANKS)
    {
        auto pr = serialRun(opt_seconds);
        runTime = pr.first;
        finished = pr.second;
    }
    else
    {
        auto pr = ompRun(opt_seconds);
        runTime = pr.first;
        finished = pr.second;
    }

    if (!finished)
    {
        this->migrationphase = true;
    }

    return std::make_pair(runTime, finished);
}

// Non-instance methods
GlobalChannelRef connectDestination(GlobalActorRef destinationActor, std::string destinationPortName)
{
    auto *dstActorPtr = *(destinationActor.local());
    auto *dstIp = dstActorPtr->getInPort(destinationPortName);
    auto channelRef = dstIp->getChannelPtr();
    dstIp->registerWithChannel();
    return channelRef;
}

upcxx::future<> connectSource(GlobalActorRef sourceActor, std::string sourcePortName, GlobalChannelRef channelRef)
{
    auto *srcActorPtr = *(sourceActor.local());
    auto *srcOp = srcActorPtr->getOutPort(sourcePortName);
    auto regFut = srcOp->registerWithChannel(channelRef);
    return regFut;
}

upcxx::future<> disconnectSource(GlobalActorRef sourceActor, std::string sourcePortName)
{
    auto *srcActorPtr = *(sourceActor.local());
    auto *srcOp = srcActorPtr->getOutPort(sourcePortName);
    auto regFut = srcOp->deregister();
    return regFut;
}

GlobalChannelRef disconnectDestination(GlobalActorRef destinationActor, std::string destinationPortName)
{
    auto *dstActorPtr = *(destinationActor.local());
    auto *dstIp = dstActorPtr->getInPort(destinationPortName);
    auto channelRef = dstIp->getChannelPtr();
    dstIp->deregister();
    return channelRef;
}

ActorGraph::~ActorGraph()
{
    for (std::pair<std::string, GlobalActorRef> kvPair : actors)
    {
        if (kvPair.second.where() == upcxx::rank_me())
        {
            ActorImpl *a = *(kvPair.second.local());
            upcxx::delete_(kvPair.second);
            delete a;
        }
    }
}

const std::unordered_map<std::string, GlobalActorRef> *ActorGraph::getActors() const { return &actors; }

const std::unordered_map<std::string, GlobalActorRef> &ActorGraph::getActorsRef() const { return actors; }

void ActorGraph::startActors()
{
    bool first = true;
    for (auto &actorPairs : actors)
    {
        if (actorPairs.second.where() == upcxx::rank_me())
        {
            auto aRef = *(actorPairs.second.local());
            aRef->start();
            if (first)
            {
                first = false;
            }
            // this->actorCount++; now done in actor::start()
        }
    }
}

void ActorGraph::restartActors()
{
    for (auto &actorPairs : actors)
    {
        if (actorPairs.second.where() == upcxx::rank_me())
        {
            auto aRef = *(actorPairs.second.local());
            if (aRef->needsReactivation())
            {
                aRef->start();
            }
        }
    }
}

unsigned int ActorGraph::getActiveActors() const
{
#ifdef PARALLEL
    return this->activeActorCount.load();
#else
    return this->activeActorCount;
#endif
}

bool ActorGraph::has(const ActorImpl *a) const noexcept
{
    if (actors.find(a->getName()) != actors.end())
    {
        return true;
    }
    else
    {
        return false;
    }
}

void ActorGraph::prepare(const std::unordered_map<std::string, upcxx::intrank_t> &l)
{
    for (const auto &pr : l)
    {
        GlobalActorRef ref = this->getActor(pr.first);
        if (ref.where() == upcxx::rank_me())
        {
            ActorImpl *a = *ref.local();
            a->prepare();
        }
    }
}

double ActorGraph::run() { return run(std::numeric_limits<double>::max(), true).first; }

std::tuple<uint64_t, uint64_t> ActorGraph::calcRankWork() const
{

    std::tuple<uint64_t, uint64_t> work{0, 0};

    for (const auto &a : this->actors)
    {
        if (a.second.where() == upcxx::rank_me())
        {
            ActorImpl *act = *(a.second.local());
            auto tup = act->getWork();
            std::get<0>(work) += std::get<0>(tup);
            std::get<1>(work) += std::get<1>(tup);
        }
    }
    return work;
}

void ActorGraph::resetWork()
{

    for (auto &a : this->actors)
    {
        if (a.second.where() == upcxx::rank_me())
        {
            ActorImpl *act = *(a.second.local());
            act->resetWork();
        }
    }
}

void ActorGraph::addToTotalWork()
{

    std::tuple<uint64_t, uint64_t> tok = calcRankWork();
    totalTime = util::addNoOverflow(totalTime, std::get<1>(tok));
    totalTokens = util::addNoOverflow(totalTokens, std::get<0>(tok));
}

std::tuple<uint64_t, uint64_t> ActorGraph::getTotalWork() const { return {this->totalTokens, this->totalTime}; }

void ActorGraph::addNoOverflow(uint64_t token, uint64_t time)
{
    this->totalTokens = util::addNoOverflow(this->totalTokens, token);
    this->totalTime = util::addNoOverflow(this->totalTime, time);
}

void actAsTask(ActorImpl *ai) { ai->act(); }

void ActorGraph::generateAndAddTasks()
{
    for (const auto &at : actors)
    {
        if (at.second.where() == upcxx::rank_me())
        {
            GlobalActorRef r = at.second;
            ActorImpl *at = *r.local();
            at->set_port_size();
            at->broadcastTaskDeque(&(this->taskDeque));
            Task t(at->getName(), TaskType::Act, 0);
            taskDeque.addTask(std::move(t));
        }
    }
}

void ActorGraph::addPortConnector(const string &connector, std::vector<std::string> connected) { portConnectors.insert(std::make_pair(connector, connected)); }

std::unordered_map<std::string, std::vector<std::string>> ActorGraph::getPortConnectors() { return portConnectors; }

void ActorGraph::triggeredAct(const std::string &name, const std::string &portName)
{
    GlobalActorRef ref = getActor(name);
    ActorImpl *ai = *ref.local();
    Task t(name, TaskType::Act, ai->inPorts.size());
    t.uniques.insert(portName);
    taskDeque.addTask(std::move(t));
}

void ActorGraph::triggeredFlushBuffer(const std::string &name)
{
    Task t(name, TaskType::Flush, 0);
    taskDeque.addTask(std::move(t));
}

void ActorGraph::triggeredNotify(const std::string &name)
{
    Task t(name, TaskType::Notify, 0);
    taskDeque.addTask(std::move(t));
}

void ActorGraph::registerRpc() { this->rpcsInFlight++; }

void ActorGraph::deregisterRpc() { this->rpcsInFlight--; }

TaskDeque *ActorGraph::getTaskDeque() { return &(this->taskDeque); }

upcxx::future<> ActorGraph::changeRef(const std::string &name, GlobalActorRef ref)
{

    upcxx::promise<> allDone;
    for (int i = 0; i < util::rank_n(); i++)
    {
        if (i == upcxx::rank_me())
        {
            this->checkInsert(name, ref);
        }
        auto cx = upcxx::operation_cx::as_promise(allDone);
        upcxx::rpc(
            i, cx,
            [](upcxx::dist_object<ActorGraph *> &rag, GlobalActorRef newActorRef, std::string &&newActorName)
            { (*rag)->checkInsert(newActorName, newActorRef); },
            remoteGraphComponents, ref, name);
    }

    return allDone.finalize();
}

bool ActorGraph::hasTasks(ActorImpl *ai) const { return this->taskDeque.hasTasks(ai->getNameRef()); }

bool ActorGraph::hasNoTasks(ActorImpl *ai) const { return this->taskDeque.hasNoTasks(ai->getNameRef()); }

void ActorGraph::printActorStates() const
{
    std::cout << "{";
    for (const auto *el : locallyActiveActors)
    {
        std::cout << "(" << el->getName() << ", " << asp.as2str(el->getRunningState()) << ") ";
    }
    std::cout << "}" << std::endl;
}

upcxx::future<> ActorGraph::sendTasks(const std::unordered_map<std::string, upcxx::intrank_t> &migList) { return this->taskDeque.sendTasks(migList); }

void ActorGraph::setRunning(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    for (const auto &pr : migList)
    {
        GlobalActorRef ref = this->getActor(pr.first);

        if (ref.where() == upcxx::rank_me())
        {
            ActorImpl *ai = *ref.local();
            ai->setRunning();
        }
    }
}