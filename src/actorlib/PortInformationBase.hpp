#pragma once

#include <iostream>
#include <memory>
#include <string>

/*
  Base classes of portInformations, these are required to have names so that actor can assign
  right information to right ports, also generateCopy functions must be virtual so that the
  base class Actor can assign them without user needing to manually do it
*/

class InPortInformationBase
{
  public:
    std::string name;

    InPortInformationBase(std::string &&name) noexcept;
    InPortInformationBase(const std::string &name) noexcept;
    InPortInformationBase(InPortInformationBase &&other) noexcept = default;
    InPortInformationBase(const InPortInformationBase &other) noexcept = default;
    InPortInformationBase() noexcept = default;
    InPortInformationBase &operator=(const InPortInformationBase &other) noexcept = delete;
    InPortInformationBase &operator=(InPortInformationBase &&other) noexcept = default;
    std::string getName() const;
    virtual ~InPortInformationBase() noexcept = default;
    virtual std::unique_ptr<InPortInformationBase> generateCopy() const = 0;
};

class OutPortInformationBase
{
  public:
    std::string name;

    OutPortInformationBase(std::string &&name) noexcept;
    OutPortInformationBase(OutPortInformationBase &&other) noexcept = default;
    OutPortInformationBase(const OutPortInformationBase &other) noexcept = default;
    OutPortInformationBase(const std::string &name) noexcept;
    OutPortInformationBase() noexcept = default;
    OutPortInformationBase &operator=(const OutPortInformationBase &other) noexcept = delete;
    OutPortInformationBase &operator=(OutPortInformationBase &&other) noexcept = default;
    std::string getName() const;
    virtual ~OutPortInformationBase() noexcept = default;
    virtual std::unique_ptr<OutPortInformationBase> generateCopy() const = 0;
};