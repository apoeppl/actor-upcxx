#include "Distribution.hpp"

extern "C"
{
#include <metis.h>
}

Distribution::Distribution(const std::unordered_map<std::string, GlobalActorRef> *actors)
    : remoteDistribution(this), vertexcount(0), migs(), colIndex(), rowIndex(), partitioned(), vertexWeights()
{
    METIS_SetDefaultOptions(metisOptions);
    metisOptions[METIS_OPTION_PTYPE] = METIS_PTYPE_KWAY;
    metisOptions[METIS_OPTION_NUMBERING] = 0;              /* C-Style numbering starting with 0 */
    metisOptions[METIS_OPTION_IPTYPE] = METIS_IPTYPE_GROW; /* C-Style numbering starting with 0 */
    metisOptions[METIS_OPTION_CONTIG] = 1;                 /* Try to produce contiguous partitions. */
    metisOptions[METIS_OPTION_NCUTS] = 5;                  /* Try 2 times, take the best */
    metisOptions[METIS_OPTION_NITER] = 50;                 /* Try 2 times in each refinement step take the best */
    metisOptions[METIS_OPTION_UFACTOR] = 10;               /* Max imbalance of 5% */
    // metisOptions[METIS_OPTION_COMPRESS] = 0;               /* do not compress */
}

template <typename MetisIdx, typename UpcxxIdx> void moveFrom(std::vector<MetisIdx> &from, std::vector<UpcxxIdx> &to)
{
    std::cout << "Size difference in the size of idx_t and upcxx::intrank_t. Copying and casting." << std::endl;
    for (idx_t i : from)
    {
        to.push_back(static_cast<UpcxxIdx>(i));
    }
}

template <typename Idx> void moveFrom(std::vector<Idx> &from, std::vector<Idx> &to)
{
    std::cout << "Directly moving the data into the destination, as the size is the same" << std::endl;
    to = std::move(from);
}

std::vector<upcxx::intrank_t> Distribution::distribute(double percentage)
{
    double arc = util::rank_n() * percentage;
    arc += 0.5;
    int rankcount = std::min(static_cast<int>(arc), util::rank_n());
    assert(rankcount > 0);
    idx_t numberOfVertices = vertexcount;
    idx_t numberOfConstraints = 1;
    idx_t numberOfPartitions = (percentage == 1.0) ? util::rank_n() : rankcount;
    idx_t finalEdgeCut = 0;
    std::vector<idx_t> partitions(vertexcount, 0);
    int metisResult;
    if (rankcount > 1)
    {
        if (vertexcount == util::rank_n() && percentage == 1.0)
        {
            std::vector<upcxx::intrank_t> res;
            for (int i = 0; i < util::rank_n(); i++)
            {
                res.push_back(i);
            }
            metisResult = METIS_OK;
            return res;
        }
        else
        {
            metisResult = METIS_PartGraphKway(&numberOfVertices, &numberOfConstraints, rowIndex.data(), colIndex.data(), NULL, NULL, NULL, &numberOfPartitions,
                                              NULL, NULL, &metisOptions[0], &finalEdgeCut, partitions.data());
            assert(metisResult == METIS_OK);
        }
    }
    else
    {
        std::fill(partitions.begin(), partitions.end(), 0.0f);
        metisResult = METIS_OK;
    }

    if (metisResult != METIS_OK)
    {
        if (metisResult == METIS_ERROR_INPUT)
        {
            throw std::runtime_error("METIS_ERROR_INPUT occurred.");
        }
        if (metisResult == METIS_ERROR_MEMORY)
        {
            throw std::runtime_error("METIS_ERROR_MEMORY occurred.");
        }
        if (metisResult == METIS_ERROR)
        {
            throw std::runtime_error("METIS_ERROR occurred.");
        }

        throw std::runtime_error("An undocumented METIS error occurred.");
    }
    std::vector<upcxx::intrank_t> res;
    moveFrom(partitions, res);
    return res;
}

std::string Distribution::dist2Str(const std::vector<std::string> &actornames) const
{
    if (partitioned.empty())
    {
        return "Empty";
    }
    else
    {
        std::string s;
        for (size_t i = 0; i < partitioned.size(); i++)
        {
            s += actornames[i] + " to rank: " + std::to_string(partitioned[i]) + "\n";
        }
        return s;
    }
}

std::string Distribution::compressedValsStr() const
{
    if (colIndex.empty() || rowIndex.empty())
    {
        return "";
    }

    std::stringstream ss;
    ss << "Col-Index: [";
    for (size_t i = 0; i < colIndex.size() - 1; i++)
    {
        ss << std::to_string(colIndex[i]);
        ss << ", ";
    }
    ss << std::to_string(colIndex[colIndex.size() - 1]);
    ss << "]";
    ss << std::endl;
    ss << "Row-Index: [";
    for (size_t i = 0; i < rowIndex.size() - 1; i++)
    {
        ss << std::to_string(rowIndex[i]);
        ss << ", ";
    }
    ss << std::to_string(rowIndex[rowIndex.size() - 1]);
    ss << "]";
    // ss << std::endl;
    return ss.str();
}

void Distribution::generateOrdering(const std::vector<std::string> &actornames)
{
    size_t count = actornames.size();
    std::unordered_map<std::string, size_t> ordering;
    for (size_t i = 0; i < count; i++)
    {
        ordering.emplace(actornames[i], i);
    }
    this->orderings = ordering;
    this->vertexcount = ordering.size();
}

// creates the compressedsparserow from the user provided input
// ordering is the list of actor names
// connections are from Actor A -> B. with A|a -> B|b
// actor name, outport name of actor a, inport name of b, name of Actor B
void Distribution::createCompressedSparseRow(const std::vector<std::string> &actornames,
                                             const std::vector<std::tuple<std::string, std::string, std::string, std::string>> &connections)
{
    std::vector<idx_t> rowStarts;
    std::vector<idx_t> colIndices;

    size_t currentrow = 0;
    size_t elcount = 0;
    size_t startoffset = 0;
    rowStarts.push_back(0);
    for (auto &name : actornames)
    {
        auto el = this->orderings.find(name);
        if (el != this->orderings.end())
        {
            if (el->second == currentrow)
            {
                size_t localelcount = 0;
                for (size_t i = 0; i < connections.size(); i++)
                // for (auto &conn : connections)
                {
                    if (std::get<0>(connections[i]) == name)
                    {
                        auto t = std::get<2>(connections[i]);
                        for (auto &l : this->orderings)
                        {
                            if (l.first == t)
                            {
                                localelcount += 1;
                                colIndices.push_back(l.second);
                            }
                        }
                        startoffset += 1;
                    }
                    else
                    {
                    }
                }

                elcount += localelcount;
                rowStarts.push_back(elcount);
                currentrow += 1;
            }
        }
        else
        {
            throw std::runtime_error("illegal input");
        }
    }

    this->rowIndex = rowStarts;
    this->colIndex = colIndices;
}

void Distribution::createCompressedSparseRow(const std::vector<std::string> &nodes, const std::vector<std::tuple<std::string, std::string>> &edges)
{
    assert(!nodes.empty());
    assert(!edges.empty());

    std::vector<idx_t> rowStarts;
    std::vector<idx_t> colIndices;

    size_t currentrow = 0;
    size_t elcount = 0;
    rowStarts.push_back(0);
    for (const auto &node : nodes)
    {
        auto el = this->orderings.find(node);
        if (el != this->orderings.end())
        {
            if (el->second == currentrow)
            {
                size_t localelcount = 0;
                for (size_t i = 0; i < edges.size(); i++)
                // for (auto &edge : edges)
                {
                    if (std::get<0>(edges[i]) == node)
                    {
                        localelcount += 1;
                        auto pr = this->orderings.find(std::get<1>(edges[i]));
                        if (pr != this->orderings.end())
                        {
                            colIndices.push_back(pr->second);
                        }
                        else
                        {
                            throw std::runtime_error("This should not happen.");
                        }
                    }
                }

                elcount += localelcount;
                rowStarts.push_back(elcount);
                currentrow += 1;
            }
        }
        else
        {
            throw std::runtime_error("illegal input");
        }
    }

    this->rowIndex = rowStarts;
    this->colIndex = colIndices;
}

const auto key_selector = [](const auto &pair) { return pair.first; };

void Distribution::generateMigList()
{
    std::unordered_map<std::string, upcxx::intrank_t> migListInternal;

    for (auto &el : orderings)
    {
        size_t ind = el.second;
        size_t at = partitioned[ind];
        migListInternal.emplace(el.first, at);
    }

    migs = migListInternal;
}

void Distribution::redistributeActors(const std::unordered_map<std::string, GlobalActorRef> &actor_map,
                                      const std::vector<std::tuple<std::string, std::string>> &connections)
{
    if (upcxx::rank_me() == 0)
    {
        assert(!actor_map.empty());
        std::vector<std::string> keys(actor_map.size());
        std::transform(actor_map.begin(), actor_map.end(), keys.begin(), key_selector);

        vertexcount = keys.size();

        generateOrdering(keys);
        generateWeights(keys, actor_map);
        createCompressedSparseRow(keys, connections);

        partitioned = distribute();

        generateMigList();

        upcxx::promise<> allDone;
        for (int i = 1; i < std::max(util::rank_n(), upcxx::rank_n()); i++)
        {
            auto cx = upcxx::operation_cx::as_promise(allDone);
            upcxx::rpc(
                i, cx,
                [](upcxx::dist_object<Distribution *> &rd, std::unordered_map<std::string, upcxx::intrank_t> &&migList) { (*rd)->accept(std::move(migList)); },
                remoteDistribution, this->migs);
        }

        allDone.finalize().wait();
    }
}

void Distribution::generateWeights(const std::vector<std::string> &actornames, const std::unordered_map<std::string, GlobalActorRef> &actor_map)
{
    assert(upcxx::rank_me() == 0);

    // set the weights array to right size
    vertexWeights.resize(actornames.size());
    std::fill_n(vertexWeights.begin(), actornames.size(), 1);

    // start getting weights, possible from other ranks
    upcxx::promise<> allDone;
    size_t counter = 0;

    for (size_t i = 0; i < actornames.size(); i++)
    {
        const std::string &an = actornames[i];

        GlobalActorRef ref = actor_map.find(an)->second;

        if (ref.where() == upcxx::rank_me())
        {
            ActorImpl *ai = *ref.local();
            vertexWeights[i] = ai->calc_cost();
        }
        else
        {
            counter += 1;
            // auto cx = upcxx::operation_cx::as_promise(allDone);
            upcxx::future<idx_t> cost = upcxx::rpc(
                ref.where(),
                [](GlobalActorRef ref) -> idx_t
                {
                    ActorImpl *ai = *ref.local();
                    size_t c = ai->calc_cost();

                    if (c > UINT_MAX)
                    {
                        throw std::runtime_error("Downcast from 64bit integer to 32bit integer loses information, fix this");
                    }

                    auto cc = static_cast<idx_t>(c);
                    return cc;
                },
                ref);

            cost.then(
                [i, this, &counter](idx_t cost)
                {
                    this->vertexWeights[i] = cost;
                    counter -= 1;
                });
        }
    }

    while (counter != 0)
    {
        upcxx::progress();
    }
}

void Distribution::distributeActors(const std::vector<std::string> &actornames,
                                    const std::vector<std::tuple<std::string, std::string, std::string, std::string>> &connections)
{
    vertexcount = actornames.size();

    if (upcxx::rank_me() == 0)
    {
        generateOrdering(actornames);
        std::fill_n(std::back_inserter(vertexWeights), actornames.size(), 1);
        createCompressedSparseRow(actornames, connections);

#ifdef REPORT_MAIN_ACTIONS
        std::cout << "=====" << std::endl;
        for (size_t i = 0; i < actornames.size(); i++)
        {
            std::cout << actornames[i] << "-" << i << std::endl;
        }
        std::cout << "=====" << std::endl;
        std::cout << compressedValsStr() << std::endl;
        std::cout << vertexcount << std::endl;
        std::cout << "=====" << std::endl;
#endif

#ifdef INVASION
        partitioned = distribute(0.5);
#else
        partitioned = distribute(1.0);
#endif

        upcxx::promise<> allDone;
        for (int i = 1; i < util::rank_n(); i++)
        {
            auto cx = upcxx::operation_cx::as_promise(allDone);

            upcxx::rpc(
                i, cx, [](upcxx::dist_object<Distribution *> &rd, std::vector<upcxx::intrank_t> &&partitioned) { (*rd)->accept(std::move(partitioned)); },
                remoteDistribution, partitioned);
        }

        allDone.finalize().wait();
    }
}

std::vector<upcxx::intrank_t> Distribution::getPartitioning() const { return partitioned; }

const std::vector<upcxx::intrank_t> &Distribution::getPartitioningRef() const { return partitioned; }

Distribution::~Distribution() = default;

void Distribution::clearData()
{
    int vertexcount = 0;
    migs.clear();
    orderings.clear();
    colIndex.clear();
    rowIndex.clear();
    partitioned.clear();
    vertexWeights.clear();
}

void Distribution::redistributeActors(const PortGraph &pg, const ActorGraph &ag)
{
    clearData();

    if (upcxx::rank_me() == 0)
    {
        std::vector<std::tuple<std::string, std::string>> edges = pg.getEdges();
        /*
        for (auto &tup : edges)
        {
            std::cout << "{" << std::get<0>(tup) << ", " << std::get<1>(tup) << "} ";
        }
        std::cout << std::endl;
        */

        const std::vector<std::tuple<std::string, std::string>> &edgesRef = edges;
        const std::unordered_map<std::string, GlobalActorRef> &vertices = ag.getActorsRef();
        auto str = util::map2Str(vertices);
        // std::cout << str << std::endl;

        redistributeActors(vertices, edgesRef);
        std::cout << util::map2Str(migs) << std::endl;
    }
    else
    {
        return;
    }
}

std::unordered_map<std::string, upcxx::intrank_t> Distribution::getMigrationList() const { return migs; }

const std::unordered_map<std::string, upcxx::intrank_t> &Distribution::getMigrationListRef() const { return migs; }

void Distribution::accept(std::vector<upcxx::intrank_t> &&other) { this->partitioned = std::move(other); }

void Distribution::accept(std::unordered_map<std::string, upcxx::intrank_t> &&other) { this->migs = std::move(other); }