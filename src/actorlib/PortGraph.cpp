#include "PortGraph.hpp"
#include <algorithm>
#include <string>

Node::Node() {}

std::string Node::toStr() const
{
    std::string s;
    s += "From: "; // vec_to_str(edgesFrom)
    s += fromVecToStr(incoming);
    s += ", To: "; //+vec_to_str(edgesTo);
    s += toVecToStr(outgoing);
    return s;
}

std::string Node::fromVecToStr(const std::vector<std::tuple<std::string, std::string, std::string>> &vec) const
{
    std::string s = "{";
    if (vec.size() != 0)
    {
        for (unsigned i = 0; i < vec.size() - 1; i++)
        {
            s += std::get<1>(vec[i]);
            s += "'s ";
            s += std::get<2>(vec[i]);
            s += "->";
            s += std::get<0>(vec[i]);
            s += ", ";
        }

        s += std::get<1>(vec[vec.size() - 1]);
        s += "'s ";
        s += std::get<2>(vec[vec.size() - 1]);
        s += "->";
        s += std::get<0>(vec[vec.size() - 1]);
        s += "}";
    }
    else
    {
        s += " }";
    }
    return s;
}

std::vector<std::string> Node::getReceivers() const
{
    std::vector<std::string> rt;
    rt.reserve(outgoing.size());
    for (const auto &pr : outgoing)
    {
        rt.push_back(std::get<1>(pr));
    }
    return rt;
}

std::vector<std::string> Node::getSenders() const
{
    std::vector<std::string> rt;
    rt.reserve(incoming.size());
    for (const auto &pr : incoming)
    {
        rt.push_back(std::get<1>(pr));
    }
    return rt;
}

std::vector<std::tuple<std::string, std::string>> Node::getSendersWithPortName() const
{
    std::vector<std::tuple<std::string, std::string>> rt;
    rt.reserve(incoming.size());
    for (const auto &pr : incoming)
    {
        rt.emplace_back(std::get<1>(pr), std::get<2>(pr));
    }
    return rt;
}

std::string Node::toVecToStr(const std::vector<std::tuple<std::string, std::string, std::string>> &vec) const
{
    std::string s = "{";
    if (!vec.empty())
    {
        for (unsigned i = 0; i < vec.size() - 1; i++)
        {
            s += std::get<0>(vec[i]);
            s += "->";
            s += std::get<1>(vec[i]);
            s += "'s ";
            s += std::get<2>(vec[i]);
            s += ", ";
        }

        s += std::get<0>(vec[vec.size() - 1]);
        s += "->";
        s += std::get<1>(vec[vec.size() - 1]);
        s += "'s ";
        s += std::get<2>(vec[vec.size() - 1]);
        s += "}";
    }
    else
    {
        s += " }";
    }
    return s;
}

void PortGraph::insertEdgeBothDir(const std::string &from, const std::string &outport, const std::string &to, const std::string &inport)
{
    insertEdgeBothDirAsync(from, outport, to, inport).wait();
}

upcxx::future<> PortGraph::insertEdgeBothDirAsync(const std::string &from, const std::string &outport, const std::string &to, const std::string &inport)
{
    Node *fromnode = &(nodes.find(from)->second);
    Node *tonode = &(nodes.find(to)->second);
#ifdef PARALLEL
    mut.lock();
#endif

    // connection of type op -> ip|B (saved as op,B,ip)
    // connection of type C|op -> ip (saved as ip,C,op)
    fromnode->outgoing.emplace_back(outport, to, inport);
    tonode->incoming.emplace_back(inport, from, outport);
#ifdef PARALLEL
    mut.unlock();
#endif
    upcxx::promise<> allDone;
    for (int i = 0; i < upcxx::rank_n(); i++)
    {
        if (i == upcxx::rank_me())
        {
            continue;
        }
        auto cx = upcxx::operation_cx::as_promise(allDone);
        upcxx::rpc(
            i, cx,
            [](upcxx::dist_object<PortGraph *> &rmp, std::string &&from, std::string &&to, std::string &&inport, std::string &&outport)
            {
                // maybe should check for the rank instead of returning?
                Node *fromnode = &((*rmp)->nodes.find(from)->second);
                Node *tonode = &((*rmp)->nodes.find(to)->second);
#ifdef PARALLEL
                (*rmp)->mut.lock();
#endif
                // connection of type op -> ip|B (saved as op,B,ip)
                // connection of type C|op -> ip (saved as ip,C,op)
                fromnode->outgoing.emplace_back(outport, std::move(to), inport);
                tonode->incoming.emplace_back(std::move(inport), std::move(from), std::move(outport));
#ifdef PARALLEL
                (*rmp)->mut.unlock();
#endif
            },
            remoteGraphComponents, from, to, inport, outport);
    }
    return allDone.finalize();
}

upcxx::future<> PortGraph::insertEdgeAsync(const std::string &from, const std::string &outportname, const std::string &to, const std::string &inportname)
{
    return insertEdgeBothDirAsync(from, outportname, to, inportname);
}

upcxx::future<> PortGraph::insertNodeAsync(const std::string &name, int rank)
{
    // std::cout << "t1" << std::endl;
    Node nd;
// std::cout << "t2" << std::endl;
#ifdef PARALLEL
    mut.lock();
#endif
    nodes.emplace(name, std::move(nd));
    ranks.emplace(name, rank);
    actors_on_rank[rank] += 1;
#ifdef PARALLEL
    mut.unlock();
#endif

    upcxx::promise<> allDone;
    for (int i = 0; i < upcxx::rank_n(); i++)
    {
        if (i == upcxx::rank_me())
        {
            continue;
        }
        auto cx = upcxx::operation_cx::as_promise(allDone);
        upcxx::rpc(
            i, cx,
            [](upcxx::dist_object<PortGraph *> &rmp, std::string &&name, int rank)
            {
                // maybe should check for the rank instead of returning?
                Node nd;
#ifdef PARALLEL
                (*rmp)->mut.lock();
#endif
                (*rmp)->nodes.emplace(name, std::move(nd));
                (*rmp)->ranks.emplace(std::move(name), rank);
                (*rmp)->actors_on_rank[rank] += 1;
#ifdef PARALLEL
                (*rmp)->mut.unlock();
#endif
            },
            remoteGraphComponents, name, rank);
    }
    auto fut = allDone.finalize();
    return fut;
}

PortGraph::PortGraph() : nodes{}, ranks{}, actors_on_rank{}, remoteGraphComponents(this), randomEngine{std::random_device{}()}
{

    actors_on_rank.resize(util::rank_n());
    std::fill(actors_on_rank.begin(), actors_on_rank.end(), 0);
}

bool PortGraph::insertNode(const std::string &name, int rank)
{
    insertNodeAsync(name, rank).wait();
    return true;
}

bool PortGraph::insertEdge(const std::string &from, const std::string &outportname, const std::string &to, const std::string &inportname)
{
    insertEdgeBothDirAsync(from, outportname, to, inportname).wait();
    return true;
}

void PortGraph::removeEdge(const std::string &from, const std::string &outport, const std::string &to, const std::string &inport)
{
    removeEdgeAsync(from, outport, to, inport).wait();
}

upcxx::future<> PortGraph::removeEdgeAsync(const std::string &from, const std::string &outport, const std::string &to, const std::string &inport)
{
    std::string fromcombination = from + "$" + outport;
    std::string tocombination = to + "$" + inport;

    Node *fromnode = &(nodes.find(from)->second);
    Node *tonode = &(nodes.find(to)->second);

    auto fromOut = fromnode->outgoing;
    auto toIn = tonode->incoming;

#ifdef PARALLEL
    mut.lock();
#endif
    for (size_t i = 0; i < fromOut.size(); i++)
    {
        if (std::get<0>(fromOut[i]) == outport && std::get<1>(fromOut[i]) == to && std::get<2>(fromOut[i]) == inport)
            fromnode->outgoing.erase(fromnode->outgoing.begin() + i);
    }

    for (size_t i = 0; i < toIn.size(); i++)
    {
        if (std::get<0>(toIn[i]) == inport && std::get<1>(toIn[i]) == from && std::get<2>(toIn[i]) == outport)
            tonode->incoming.erase(tonode->incoming.begin() + i);
    }

#ifdef PARALLEL
    mut.unlock();
#endif

    upcxx::promise<> allDone;
    for (int i = 0; i < upcxx::rank_n(); i++)
    {
        if (i == upcxx::rank_me())
        {
            continue;
        }
        auto cx = upcxx::operation_cx::as_promise(allDone);
        upcxx::rpc(
            i, cx,
            [](upcxx::dist_object<PortGraph *> &rmp, std::string &&from, std::string &&to, std::string &&inport, std::string &&outport)
            {
                // maybe should check for the rank instead of returning?
                std::string fromcombination = from + "$" + outport;
                std::string tocombination = to + "$" + inport;
                Node *fromnode = &((*rmp)->nodes.find(from)->second);
                Node *tonode = &((*rmp)->nodes.find(to)->second);

                auto fromOut = fromnode->outgoing;
                auto toIn = tonode->incoming;

#ifdef PARALLEL
                (*rmp)->mut.lock();
#endif
                for (size_t i = 0; i < fromOut.size(); i++)
                {
                    if (std::get<0>(fromOut[i]) == outport && std::get<1>(fromOut[i]) == to && std::get<2>(fromOut[i]) == inport)
                        fromnode->outgoing.erase(fromnode->outgoing.begin() + i);
                }

                for (size_t i = 0; i < toIn.size(); i++)
                {
                    if (std::get<0>(toIn[i]) == inport && std::get<1>(toIn[i]) == from && std::get<2>(toIn[i]) == outport)
                        tonode->incoming.erase(tonode->incoming.begin() + i);
                }

#ifdef PARALLEL
                (*rmp)->mut.unlock();
#endif
            },
            remoteGraphComponents, from, to, inport, outport);
    }
    return allDone.finalize();
}

std::string PortGraph::toStr() const
{
    std::string s = "{\n";
    for (std::pair<std::string, Node> el : nodes)
    {
        s += el.first;
        s += ":= ";
        s += (el.second).toStr();
        s += "\n";
    }
    s += "}\n";

    return s;
}

// return order outport of self -> other ActorImpl name, other ActorImpl inport name
std::vector<std::tuple<std::string, std::string, std::string>> PortGraph::getOutgoing(const std::string &name) const
{
    auto indit = nodes.find(name);
    if (indit == nodes.end())
    {
        throw std::runtime_error("Actor not in Graph");
    }
    else
    {
        const Node *nd = &(indit->second);
        if (nd->outgoing.empty())
        {
            throw std::runtime_error("getOugoing malfunctioned");
            return {};
        }
        std::vector<std::tuple<std::string, std::string, std::string>> vec = nd->outgoing;
        return vec;
    }
}

// return order: inportname, otheractor name, other actors ourport name
std::vector<std::tuple<std::string, std::string, std::string>> PortGraph::getIncoming(const std::string &name) const
{
    auto indit = nodes.find(name);
    if (indit == nodes.end())
    {
        throw std::runtime_error("Actor not in Graph");
    }
    else
    {
        const Node *nd = &(indit->second);
        if (nd->incoming.size() == 0)
        {
            throw std::runtime_error("getIncoming malfunctioned");
            return {};
        }
        std::vector<std::tuple<std::string, std::string, std::string>> vec = nd->incoming;
        return vec;
    }
}

std::vector<std::string> PortGraph::getNodes() const
{
    std::vector<std::string> tr;
    tr.reserve(nodes.size());
    for (const auto &pr : nodes)
    {
        tr.push_back(pr.first);
    }
    return tr;
}

std::vector<std::tuple<std::string, std::string>> PortGraph::getEdges() const
{
    std::vector<std::tuple<std::string, std::string>> tr;
    for (const auto &pr : nodes)
    {
        const Node *nd = &(pr.second);
        std::vector<std::string> rcvs = nd->getReceivers();
        std::string name = pr.first;
        for (size_t i = 0; i < rcvs.size(); i++)
        {

            tr.emplace_back(name, rcvs[i]);
        }
    }

    return tr;
}

std::tuple<std::vector<std::string>, std::vector<std::tuple<std::string, std::string>>> PortGraph::getGraph() const
{

    std::vector<std::string> nds;
    std::vector<std::tuple<std::string, std::string>> edgs;
    for (auto &pr : nodes)
    {
        const Node *nd = &(pr.second);
        nds.emplace_back(pr.first);

        std::vector<std::string> rcvs = nd->getReceivers();

        for (size_t i = 0; i < rcvs.size(); i++)
        {
            std::string name = pr.first;
            std::tuple<std::string, std::string> edge(std::move(name), std::move(rcvs[i]));
            edgs.emplace_back(std::move(edge));
        }

        std::vector<std::string> sndrs = nd->getSenders();
        for (size_t i = 0; i < sndrs.size(); i++)
        {
            std::string name = pr.first;
            std::tuple<std::string, std::string> edge(std::move(sndrs[i]), std::move(name));
            edgs.emplace_back(std::move(edge));
        }
    }

    return {nds, edgs};
}

std::vector<std::vector<std::string>> PortGraph::getActorRankDistribution() const
{
    std::vector<std::vector<std::string>> vv;
    for (int i = 0; i < util::rank_n(); i++)
    {
        std::vector<std::string> v;
        vv.emplace_back(v);
    }

    for (int i = 0; i < util::rank_n(); i++)
    {
        for (const auto &el : ranks)
        {
            vv[el.second].emplace_back(el.first);
        }
    }

    return vv;
}

size_t Node::connectionCount() const { return outgoing.size(); }

std::vector<std::string> PortGraph::getReceivers(const std::string &name) const
{
    auto it = nodes.find(name);
    if (it != nodes.end())
    {
        return it->second.getReceivers();
    }
    else
    {
        throw std::runtime_error("could not find");
    }
}

std::vector<std::string> PortGraph::getSenders(const std::string &name) const
{
    auto it = nodes.find(name);
    if (it != nodes.end())
    {
        return it->second.getSenders();
    }
    else
    {
        throw std::runtime_error("could not find");
    }
}

std::vector<std::tuple<std::string, std::string>> PortGraph::getSendersWithPortName(const std::string &name) const
{
    auto it = nodes.find(name);
    if (it != nodes.end())
    {
        return it->second.getSendersWithPortName();
    }
    else
    {
        throw std::runtime_error("could not find");
    }
}

std::set<std::string> PortGraph::getNeighbors(const std::string &name) const
{
    auto it = nodes.find(name);
    if (it != nodes.end())
    {
        auto recv = it->second.getReceivers();
        std::set<std::string> set = std::set<std::string>(std::make_move_iterator(recv.begin()), std::make_move_iterator(recv.end()));
        auto send = it->second.getSenders();
        for (auto &&s : send)
        {
            set.insert(std::move(s));
        }

        return set;
    }
    else
    {
        throw std::runtime_error("could not find" + name);
    }
}

bool PortGraph::isBorderActor(const std::string &name) const
{
    auto it = nodes.find(name);
    if (it != nodes.end())
    {
        auto pr = *it;
        Node &nd = pr.second;
        int rank = ranks.find(name)->second;
        auto snds = nd.getSenders();
        for (auto &snd : snds)
        {
            if (ranks.find(snd)->second != rank)
            {
                return true;
            }
        }
        auto rcvs = nd.getReceivers();
        for (auto &rcv : rcvs)
        {
            if (ranks.find(rcv)->second != rank)
            {
                return true;
            }
        }
    }
    else
    {
        assert(false);
        return false;
    }
    return false;
}

std::set<int> PortGraph::borderingRanks(const std::string &name) const
{
    auto it = nodes.find(name);
    if (it == nodes.end())
    {
        return {};
    }
    const Node &nd = it->second;
    int rank = ranks.find(name)->second;
    std::set<int> ret;
    auto snds = nd.getSenders();
    for (auto &snd : snds)
    {
        int other = ranks.find(snd)->second;
        if (other != rank)
        {
            ret.insert(other);
        }
    }
    auto rcvs = nd.getReceivers();
    for (auto &rcv : rcvs)
    {
        int other = ranks.find(rcv)->second;
        if (other != rank)
        {
            ret.insert(other);
        }
    }

    return ret;
}

std::set<int> PortGraph::borderingRanks(int rank) const
{
    std::set<int> ret;
    for (auto &pr : ranks)
    {
        if (pr.second == rank)
        {
            ret.merge(borderingRanks(pr.first));
        }
    }

    return ret;
}

std::vector<std::string> PortGraph::borderingActors(int rank) const
{
    std::set<std::string> borderactorsofrank;
    // std::copy_if(ranks.begin(), ranks.end(), std::back_inserter(borderactorsofrank),
    //             [this, rank](const std::pair<const std::string, int> &el) -> bool { return el.second == rank && this->isBorderActor(el.first); });

    std::for_each(ranks.begin(), ranks.end(),
                  [this, rank, &borderactorsofrank](const std::pair<const std::string, int> &el)
                  {
                      if (el.second == rank && this->isBorderActor(el.first))
                      {
                          borderactorsofrank.insert(el.first);
                      }
                  });

    std::set<std::string> ret;
    for (auto &el : borderactorsofrank)
    {
        auto s = getNeighbors(el);

        for (auto &n : s)
        {
            if (ranks.find(n)->second != rank)
            {
                ret.insert(n);
            }
        }
    }

    std::vector<std::string> out;
    std::copy(std::make_move_iterator(ret.begin()), std::make_move_iterator(ret.end()), std::back_inserter(out));

    if constexpr (config::prefer_rank_with_maximum_actors)
    {

        // cmp needs to return true if the left element needs to be placed before, this means left element's rank has more actors
        if (out.size() > 1)
        {
            std::sort(out.begin(), out.end(),
                      [this](const std::string &left, const std::string &right) -> bool
                      {
                          assert(!left.empty());
                          assert(!right.empty());

                          assert(this->ranks.find(left) != this->ranks.end());
                          auto &l = *this->ranks.find(left);
                          assert(this->ranks.find(right) != this->ranks.end());
                          auto &r = *this->ranks.find(right);
                          auto ll = this->actors_on_rank[l.second];
                          auto rr = this->actors_on_rank[r.second];

                          return (ll > rr);
                      });
        }
    }

    return out;
}

void PortGraph::propagateRankChange(const std::string &name, int rank)
{
#ifdef PARALLEL
    mut.lock();
#endif

    auto it = ranks.find(name);

    assert(it != ranks.end());
    actors_on_rank[it->second] -= 1;
    ranks[name] = rank;
    actors_on_rank[rank] += 1;

#ifdef PARALLEL
    mut.unlock();
#endif
}

upcxx::future<> PortGraph::changeRankAsyncFF(const std::string &name)
{
    propagateRankChange(name, upcxx::rank_me());

    upcxx::promise<> allDone;
    for (int i = 0; i < upcxx::rank_n(); i++)
    {
        if (i == upcxx::rank_me())
        {
            continue;
        }
        else
        {
            auto cx = upcxx::operation_cx::as_promise(allDone);
            upcxx::rpc(
                i, cx,
                [](upcxx::dist_object<PortGraph *> &remoteGraphComponents, int rank, std::string &&name)
                { (*remoteGraphComponents)->propagateRankChange(name, rank); },
                remoteGraphComponents, upcxx::rank_me(), name);
        }
    }
    return allDone.finalize();
}

std::vector<std::string> PortGraph::actorsOnRank(int rank) const
{
    std::vector<std::string> ret;
    for (auto &el : ranks)
    {
        if (el.second == rank)
        {
            ret.push_back(el.first);
        }
    }
    return ret;
}

std::vector<std::string> PortGraph::getActorsFromNonEmptyRank(unsigned int emptyRankOffset) const
{
    unsigned int totalcount = util::rank_n();
    unsigned int i = (emptyRankOffset + 1) % totalcount;
    do
    {
        // std::cout << upcxx::rank_me() << " try " << i << std::endl;
        auto ret = actorsOnRank(i);
        if (!ret.empty())
        {
            // std::cout << upcxx::rank_me() << " steal from " << i << std::endl;
            if constexpr (config::prefer_rank_with_maximum_actors)
            {
                // std::vector<std::string> out;
                // std::copy(std::make_move_iterator(ret.begin()), std::make_move_iterator(ret.end()), std::back_inserter(out));

                // cmp needs to return true if the left element needs to be placed before, this means left element's rank has more actors
                if (ret.size() > 1)
                {
                    std::sort(ret.begin(), ret.end(),
                              [this](const std::string &left, const std::string &right) -> bool
                              {
                                  assert(!left.empty());
                                  assert(!right.empty());

                                  assert(this->ranks.find(left) != this->ranks.end());
                                  auto &l = *this->ranks.find(left);
                                  assert(this->ranks.find(right) != this->ranks.end());
                                  auto &r = *this->ranks.find(right);
                                  auto ll = this->actors_on_rank[l.second];
                                  auto rr = this->actors_on_rank[r.second];

                                  return (ll > rr);
                              });
                }
            }

            ret.erase(unique(ret.begin(), ret.end()), ret.end());

            return ret;
        }

        i = (i + 1) % totalcount;
    } while (i != emptyRankOffset);

    std::vector<std::string> emptyvector;
    return emptyvector;
}

std::vector<std::string> PortGraph::getRandomActors(size_t count, int forrank) const
{
    // TODO: optimize this by removing unnecessary copies
    std::vector<std::pair<std::string, Node>> out;

    std::sample(nodes.begin(), nodes.end(), std::back_inserter(out), count + 1, randomEngine);

    std::vector<std::pair<std::string, Node>> ret;
    std::copy_if(out.begin(), out.end(), std::back_inserter(ret),
                 [this, forrank](const std::pair<const std::string, const Node> &name) { return this->ranks.find(name.first)->second != forrank; });

    std::vector<std::string> ret2;
    ret2.reserve(ret.size());

    for (const auto &el : ret)
    {
        ret2.push_back(el.first);
    }

    if constexpr (config::prefer_rank_with_maximum_actors)
    {
        if (ret2.size() > 1)
        {
            // cmp needs to return true if the left element needs to be placed before, this means left element's rank has more actors
            std::sort(ret2.begin(), ret2.end(),
                      [this](const std::string &left, const std::string &right) -> bool
                      {
                          assert(!left.empty());
                          assert(!right.empty());

                          assert(this->ranks.find(left) != this->ranks.end());
                          const auto &l = *this->ranks.find(left);
                          assert(this->ranks.find(right) != this->ranks.end());
                          const auto &r = *this->ranks.find(right);
                          auto ll = this->actors_on_rank[l.second];
                          auto rr = this->actors_on_rank[r.second];

                          return (ll > rr);
                      });
        }
    }

    ret2.erase(std::unique(ret2.begin(), ret2.end()), ret2.end());
    return ret2;
}

const Node &PortGraph::getNode(const std::string &name) const
{
    auto it = nodes.find(name);
    return it->second;
}

std::vector<std::string> PortGraph::getActorsFromCrowdiestRank(size_t count) const
{

    std::vector<int> vec;
    vec.resize(util::rank_n());

    for (const auto &pr : ranks)
    {
        vec[pr.second] += 1;
    }

    auto rankit = std::max_element(vec.begin(), vec.end());
    int rank = rankit - vec.begin();

    std::vector<std::string> ret;
    ret.reserve(count);
    size_t tt = 0;
    for (const auto &pr : ranks)
    {
        if (pr.second == rank)
        {
            ret.push_back(pr.first);
            tt++;
        }

        if (tt == count)
        {
            return ret;
        }
    }

    return ret;
}

bool PortGraph::noActorsOnRank(upcxx::intrank_t rank) const { return actors_on_rank[rank] == 0; }