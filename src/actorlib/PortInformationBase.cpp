#include "PortInformationBase.hpp"

InPortInformationBase::InPortInformationBase(std::string &&name) noexcept : name(std::move(name)) {}

InPortInformationBase::InPortInformationBase(const std::string &name) noexcept : name(name) {}

std::string InPortInformationBase::getName() const { return name; }

OutPortInformationBase::OutPortInformationBase(std::string &&name) noexcept : name(std::move(name)) {}

OutPortInformationBase::OutPortInformationBase(const std::string &name) noexcept : name(name) {}

std::string OutPortInformationBase::getName() const { return name; }
