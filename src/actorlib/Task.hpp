#pragma once

#include "AcquisitonResult.hpp"
#include "ActorState.hpp"
#include "Utility.hpp"
#include <deque>
#include <set>
#include <string>
#include <upcxx/upcxx.hpp>

class ActorImpl;

class Task
{
  public:
    bool finished;
    std::string source;
    TaskType tasktype;
    std::set<std::string> uniques;
    size_t unique_needed;

    Task() noexcept;
    Task(const std::string &source, TaskType tt, size_t unique_needed) noexcept;
    Task(std::string &&source, TaskType tt, size_t unique_needed) noexcept;
    Task &operator=(Task &&t) noexcept;
    Task &operator=(const Task &t) noexcept;
    Task(const Task &t) noexcept;
    Task(Task &&t) noexcept;
    ~Task() noexcept = default;
    void execute(ActorImpl *ai);
    void reset() noexcept;
    bool none() const noexcept;
    std::string print() const noexcept;
    bool toTerminated() const noexcept;

    struct upcxx_serialization
    {
        template <typename Writer> static void serialize(Writer &writer, Task const &object)
        {
            writer.write(object.finished);
            writer.write(object.source);
            writer.write(object.tasktype);
            writer.write(object.unique_needed);
            writer.write(object.uniques);
            return;
        }

        template <typename Reader> static Task *deserialize(Reader &reader, void *storage)
        {
            bool finished = reader.template read<bool>();
            std::string source = reader.template read<std::string>();
            TaskType tasktype = reader.template read<TaskType>();
            size_t unique_needed = reader.template read<size_t>();
            std::set<std::string> uniques = reader.template read<std::set<std::string>>();

            Task *v = ::new (storage) Task(std::move(source), tasktype, unique_needed);
            v->finished = finished;
            v->unique_needed = unique_needed;
            v->uniques = std::move(uniques);
            return v;
        }
    };
};