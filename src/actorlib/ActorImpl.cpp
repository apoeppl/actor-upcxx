/**
 * @file
 * This file is part of actorlib.
 *
 * @author Alexander Pöppl (poeppl AT in.tum.de,
 * https://www5.in.tum.de/wiki/index.php/Alexander_P%C3%B6ppl,_M.Sc.)
 *
 * @section LICENSE
 *
 * actorlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * actorlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with actorlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @section DESCRIPTION
 *
 * TODO
 */

#include "ActorImpl.hpp"

#include "InPort.hpp"
#include "OutPort.hpp"
#include "config.hpp"

#include "ActorGraph.hpp"

#include <sstream>
#include <string>

#ifdef TRACE
int ActorImpl::event_act = -1;
#endif

const ASPrinter ActorImpl::asp;

using namespace std::string_literals;

ActorImpl::ActorImpl(const std::string &name) noexcept
    : name(name), nomadiness(0), parentActorGraph(nullptr), inPorts(), outPorts(), actorPersona(), state(ActorState::NeedsActivation), portsInformation(),
      portsInfoApplicable(false), workTokens(0), workTime(0), tbegin(), tend(), markmutex(), markedBy{-1}, pinnedBy{0}, pin_count{}, actor_cost(),
      index_for_actor_cost(0)
{
#ifdef TRACE
    std::string event_act_name = "act";
    if (ActorImpl::event_act == -1)
        VT_funcdef(event_act_name.c_str(), VT_NOCLASS, &ActorImpl::event_act);
#endif
    initPinCount();
}

ActorImpl::ActorImpl(std::string &&name) noexcept
    : name(std::move(name)), nomadiness(0), parentActorGraph(nullptr), inPorts(), outPorts(), actorPersona(), state(ActorState::NeedsActivation),
      portsInformation(), portsInfoApplicable(false), workTokens(0), workTime(0), tbegin(), tend(), markmutex(), markedBy{-1}, pinnedBy{0}, pin_count{},
      actor_cost(), index_for_actor_cost(0)
{
#ifdef TRACE
    std::string event_act_name = "act";
    if (ActorImpl::event_act == -1)
        VT_funcdef(event_act_name.c_str(), VT_NOCLASS, &ActorImpl::event_act);
#endif
    initPinCount();
}

ActorImpl::ActorImpl(const ActorImpl &act) noexcept
    : name(act.name), nomadiness(act.nomadiness + 1), parentActorGraph(nullptr), actorPersona(), state(act.state.load()),
      portsInformation(act.copyPortsInformation()), portsInfoApplicable(true), workTokens(0), workTime(0), tbegin(), tend(), markedBy(act.markedBy),
      pin_count(act.pin_count), actor_cost(act.actor_cost), index_for_actor_cost(act.index_for_actor_cost)
{
    called = act.called;
    stealable = act.stealable;
#ifdef VARYING_JOB
    init_random_variable();
#endif
}

ActorImpl::ActorImpl(ActorImpl &&act) noexcept
    : name(std::move(act.name)), nomadiness(act.nomadiness + 1), parentActorGraph(nullptr), inPorts(), outPorts(), actorPersona(), state(act.state.load()),
      portsInformation(std::move(act.portsInformation)), portsInfoApplicable(act.portsInfoApplicable), workTokens(0), workTime(0), tbegin(), tend(),
      markedBy(std::move(act.markedBy)), pinnedBy{0}, pin_count(std::move(act.pin_count)), actor_cost(std::move(act.actor_cost)),
      index_for_actor_cost(act.index_for_actor_cost)
{
    called = act.called;
    stealable = act.stealable;
}

ActorImpl::ActorImpl(ActorData &&data) noexcept
    : name(std::move(data.name)), nomadiness(data.nomadiness + 1), parentActorGraph(nullptr), inPorts(), outPorts(), actorPersona(), state(data.state),
      portsInformation(), portsInfoApplicable(false), workTokens(0), workTime(0), tbegin(), tend(), markedBy(std::move(data.mark)), pinnedBy{0}, pin_count{},
      actor_cost(), index_for_actor_cost(0)
{
// maybe it is the first actor in this rank
#ifdef TRACE
    std::string event_act_name = "act";
    if (ActorImpl::event_act == -1)
        VT_funcdef(event_act_name.c_str(), VT_NOCLASS, &ActorImpl::event_act);
#endif
    // called = -1;
    stealable = false;
    initPinCount();
}

ActorImpl::ActorImpl(
    ActorData &&data,
    std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> &&portsInformation) noexcept
    : name(std::move(data.name)), nomadiness(data.nomadiness + 1), parentActorGraph(nullptr), inPorts(), outPorts(), actorPersona(), state(data.state),
      portsInformation(std::move(portsInformation)), portsInfoApplicable(true), workTokens(data.workTokens), workTime(data.workTime), tbegin(), tend(),
      markedBy(std::move(data.mark)), pinnedBy{}, pin_count{}, actor_cost(), index_for_actor_cost(0)
{
#ifdef TRACE
    std::string event_act_name = "act";
    if (ActorImpl::event_act == -1)
        VT_funcdef(event_act_name.c_str(), VT_NOCLASS, &ActorImpl::event_act);
#endif
    // called = -1;
    stealable = false;
    initPinCount();
}

void ActorImpl::initPinCount()
{
    pin_count.resize(util::rank_n());
    std::fill(pin_count.begin(), pin_count.end(), 0);
}

ActorImpl::~ActorImpl() noexcept
{
    for (auto &aip : this->inPorts)
    {
        delete aip.second;
    }
    for (auto &aop : this->outPorts)
    {
        assert(aop.second->bufferSize() == 0);
        delete aop.second;
    }
}

std::string ActorImpl::toString()
{
    std::stringstream ss;
    ss << "Actor( " << name << " ) { ";
    for (auto &aip : this->inPorts)
    {
        ss << aip.second->toString() << " ";
    }
    for (auto &aop : this->outPorts)
    {
        ss << aop.second->toString() << " ";
    }
    ss << "}";
    return ss.str();
}

std::pair<std::vector<std::pair<std::string, AbstractInPort *>>, std::vector<std::pair<std::string, AbstractOutPort *>>> ActorImpl::getPorts() const
{
    std::pair<std::vector<std::pair<std::string, AbstractInPort *>>, std::vector<std::pair<std::string, AbstractOutPort *>>> x(getAllInPorts(),
                                                                                                                               getAllOutPorts());
    return x;
}

// std::unordered_map<std::string, AbstractInPort *> inPorts;
// std::unordered_map<std::string, AbstractOutPort *> outPorts;
// transforms the unordered map of inports to a vector of pairs
// and returns the vector
std::vector<std::pair<std::string, AbstractInPort *>> ActorImpl::getAllInPorts() const
{
    std::vector<std::pair<std::string, AbstractInPort *>> v;
    for (auto it = inPorts.begin(); it != inPorts.end(); ++it)
    {
        v.emplace_back(std::pair<std::string, AbstractInPort *>(it->first, it->second));
    }
    return v;
}
// transforms the unordered map of outports to a vector of pairs
// and returns the vector
std::vector<std::pair<std::string, AbstractOutPort *>> ActorImpl::getAllOutPorts() const
{
    std::vector<std::pair<std::string, AbstractOutPort *>> v;
    for (auto it = outPorts.begin(); it != outPorts.end(); ++it)
    {
        v.emplace_back(std::pair<std::string, AbstractOutPort *>(it->first, it->second));
    }
    return v;
}

std::vector<std::string> ActorImpl::getAllInPortNames() const
{
    std::vector<std::string> v;
    for (std::unordered_map<std::string, AbstractInPort *>::const_iterator it = inPorts.begin(); it != inPorts.end(); ++it)
    {
        v.push_back(std::string(it->first));
    }
    return v;
}
// transforms the unordered map of outports to a vector of pairs
// and returns the vector
std::vector<std::string> ActorImpl::getAllOutPortNames() const
{
    std::vector<std::string> v;
    for (auto it = outPorts.begin(); it != outPorts.end(); ++it)
    {
        v.emplace_back(std::string(it->first));
    }
    return v;
}

void ActorImpl::rmInPort(const std::string &name)
{
    auto res = inPorts.find(name);
    if (res != inPorts.end())
    {
        inPorts.erase(name);
    }
    else
    {
        std::cout << "no such inport" << std::endl;
    }
}

void ActorImpl::rmOutPort(const std::string &name)
{
    auto res = outPorts.find(name);
    if (res != outPorts.end())
    {
        outPorts.erase(name);
    }
    else
    {
        std::cout << "no such outport" << std::endl;
    }
}

AbstractInPort *ActorImpl::getInPort(std::string &name)
{
    auto res = inPorts.find(name);
    if (res != inPorts.end())
    {
        return res->second;
    }
    else
    {
        throw std::runtime_error("Actor "s + this->toString() + "has no InPort with id "s + name);
    }
}

AbstractOutPort *ActorImpl::getOutPort(std::string &name)
{
    auto res = outPorts.find(name);
    if (res != outPorts.end())
    {
        return res->second;
    }
    throw std::runtime_error("Actor "s + this->toString() + "has no OutPort with name "s + name);
}

void ActorImpl::setPersonas()
{
    for (auto &ip : this->inPorts)
    {
        ip.second->setActorPersona(&(this->actorPersona));
    }
    for (auto &op : this->outPorts)
    {
        op.second->setActorPersona(&(this->actorPersona));
    }
}

bool ActorImpl::start() { return startWithCaller(-1); }

bool ActorImpl::startWithCaller(upcxx::intrank_t caller)
{
#ifndef NDEBUG
    if (this->state.load() == ActorState::Terminated || this->state.load() == ActorState::WantsToTerminate)
    {
        throw std::runtime_error("should not heppen");
        return false;
    }
#endif

    // std::unique_lock<std::mutex> lk(markmutex);
    if (caller == -1)
    {
        if (!this->parentActorGraph->migrationphase)
        {
            assert(state == ActorState::NeedsActivation);
        }
    }

    // try to change an actor marked with caller
    // shpould have stopped only for migration only check that

    if (caller != markedBy)
    {
        return false;
    }

    ActorState nac = ActorState::NeedsActivation;
    ActorState tsfm = ActorState::TemporaryStoppedForMigration;

    if (this->state.compare_exchange_strong(nac, ActorState::Transitioning) ||
        (nac != ActorState::NeedsActivation && this->state.compare_exchange_strong(tsfm, ActorState::Transitioning)))
    {
#if defined(REPORT_MAIN_ACTIONS)
        std::cout << "Starting actor " << this->name << " on rank " << std::to_string(upcxx::rank_me()) << " by " << caller << std::endl;
#endif

        this->parentActorGraph->activeActorCount += 1;
        this->parentActorGraph->locallyActiveActors.insert(this);
        setPersonas();
        this->state = ActorState::Running;
        // started actors should be automatically unmarked
        this->markedBy = -1;
        this->pinnedBy = 0;

        return true;
    }

    // we failed so we set it to the original value
#ifndef NDEBUG
    throw std::runtime_error("should not happen!");
#endif
    return false;
}

bool ActorImpl::stop() { return this->stopFromAct(); }

bool ActorImpl::tryLock(ActorState expected, ActorState desired) { return this->state.compare_exchange_strong(expected, desired); }

bool ActorImpl::mark(upcxx::intrank_t marker)
{
    // std::unique_lock<std::mutex> lk(markmutex);
    if (stealable && pinnedBy == 0 && (state != ActorState::Terminated && state != ActorState::WantsToTerminate) &&
        ((called > 0) || ((called == 0) && state == ActorState::NeedsActivation)) &&
        (config::steal_if_has_terminated_actors || (!config::steal_if_has_terminated_actors && !this->parentActorGraph->has_a_terminated_actor)) &&
        noNeedToNotify() && buffersEmpty() && connected())
    {
        if (markedBy == -1)
        {
            markedBy = marker;
            return true;
        }
        else
        {
            assert(markedBy != marker);
            return false;
        }
    }
    else
    {
        return false;
    }
}

bool ActorImpl::unmark(upcxx::intrank_t marker)
{
    // std::unique_lock<std::mutex> lk(markmutex);
    if (pinnedBy == 0)
    {
        if (markedBy == marker)
        {
            markedBy = -1;
            return true;
        }
        else
        {
#ifndef NDEBUG
            if (markedBy != util::rank_n())
            {
                assert(false);
            }
#endif
        }
    }
    else
    {
#ifndef NDEBUG
        if (markedBy != util::rank_n())
        {
            assert(false);
        }
#endif
    }
    return false;
}

bool ActorImpl::pin(upcxx::intrank_t pinner)
{
    // std::unique_lock<std::mutex> lk(markmutex);
    if (markedBy >= 0)
    {
        return false;
    }
    else
    {
        if (state == ActorState::WantsToTerminate)
        {
            return false;
        }

        pinnedBy += 1;
        pin_count[pinner] += 1;

        return true;
    }
}

bool ActorImpl::unpin(upcxx::intrank_t pinner)
{
    // std::unique_lock<std::mutex> lk(markmutex);
    //-1 unmarked, 0-(n-1) marked by a rank, n marked to be not marked again
    if (markedBy >= 0 && markedBy != util::rank_n())
    {
        return false;
    }
    else
    {
        if (pinnedBy < 1)
        {
            throw std::runtime_error("don't call unpin if you have not pinned");
        }

        if (pinnedBy > 0 && pin_count[pinner] <= 0)
        {
            throw std::runtime_error("again, don't call unpin if you have not pinned");
        }

        if (pin_count[pinner] <= 0)
        {
            throw std::runtime_error("unpinned too many times!");
        }

        pin_count[pinner] -= 1;
        pinnedBy -= 1;

        return true;
    }
}

bool ActorImpl::isPinned() const { return pinnedBy > 0; }

bool ActorImpl::isMarked() const { return markedBy >= 0; }

// rank_n() stops unmarked, rank stops an actor marked by the caller
bool ActorImpl::stopWithCaller(ActorState as, upcxx::intrank_t caller)
{
#ifndef NDEBUG
    assert(as != ActorState::Terminated && as != ActorState::WantsToTerminate);
#endif

    // std::unique_lock<std::mutex> lk(markmutex);
    bool was = false;
    // try to change an actor marked with caller
    if (caller != util::rank_n() + 4 && markedBy != caller && (markedBy != -1 || state != ActorState::NeedsActivation) && markedBy != util::rank_n())
    {
        throw std::runtime_error("Oh shit!");
    }

    if (markedBy == -1)
    {
        assert(state == ActorState::NeedsActivation || this->parentActorGraph->migrationphase);
        was = true;
        markedBy = caller;
    }

    if (!noNeedToNotify() || !buffersEmpty() || !connected())
    {
        return false;
    }

    if (markedBy == caller)
    {
        // stop only if the actor is unmarked by me
        ActorState run = ActorState::Running;
        // ActorState tsfm = ActorState::NeedsActivation;

        if (this->state.compare_exchange_strong(run, ActorState::Transitioning)
            /*||(run != ActorState::NeedsActivation && this->state.compare_exchange_strong(tsfm, ActorState::Transitioning)) */)
        {
#if defined(REPORT_MAIN_ACTIONS)
            std::cout << "Stopping actor:" << this->getName() << " on rank: " << upcxx::rank_me() << " by rank: " << caller << std::endl;
#endif

            this->parentActorGraph->activeActorCount -= 1;
            this->parentActorGraph->locallyActiveActors.erase(this);
            this->state = as;
            return true;
        }
    }
    // need to revert marking
    // the value we have is old so
    // this mean it is in wanting to terminate or terminated state

    if (was)
    {
        markedBy = -1;
    }

    return false;
}

bool ActorImpl::stopFromAct()
{
    // std::unique_lock<std::mutex> lk(markmutex);

    // stop only if the actor is unmarked by me
    // we should not see wantstoterminate or terminated since it is called only once
    // this will prevent any migration attempts
    ActorState run = ActorState::Running;
    markedBy = upcxx::rank_n();
    this->state.store(ActorState::Transitioning);

    if (buffersEmpty())
    {
#if defined(REPORT_MAIN_ACTIONS)
        std::cout << "Terminating actor: " << this->getName() << " on rank: " << upcxx::rank_me() << std::endl;
#endif
        this->parentActorGraph->activeActorCount -= 1;
        this->parentActorGraph->locallyActiveActors.erase(this);
        this->state = ActorState::Terminated;
        this->parentActorGraph->has_a_terminated_actor = true;
        // if it is stopped then it should have the mark of the caller
    }
    else
    {
#if defined(REPORT_MAIN_ACTIONS)
        std::cout << "Terminating actor (wantstoterminate): " << this->getName() << " on rank: " << upcxx::rank_me() << std::endl;
#endif

        this->parentActorGraph->activeActorCount -= 1;
        this->parentActorGraph->locallyActiveActors.erase(this);
        this->parentActorGraph->triggeredFlushBuffer(this->getName());
        this->state = ActorState::WantsToTerminate;
        this->parentActorGraph->has_a_terminated_actor = true;
        this->ret_to_wants_to_terminate = true;
    }

    return true;
}

std::string ActorImpl::getName() const { return name; }

void ActorImpl::triggeredAct(const std::string &portName) const { this->parentActorGraph->triggeredAct(this->getNameRef(), portName); }

void ActorImpl::triggeredFlushBuffer() { this->parentActorGraph->triggeredFlushBuffer(this->getNameRef()); }

void ActorImpl::b_act()
{
    // std::cout << upcxx::rank_me() << ": execute b_act for " << name << std::endl;

    workTokens += 1;
    tbegin = std::chrono::steady_clock::now();

#ifdef TRACE
    VT_begin(ActorImpl::event_act);
#endif

    sendNotifications();
    flushBuffers();
    act();

#ifdef TRACE
    VT_end(ActorImpl::event_act);
#endif

    tend = std::chrono::steady_clock::now();
    size_t elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(tend - tbegin).count();
#ifndef NDEBUG
    assert(elapsed != 0);
#endif
    workTime += elapsed;

    this->portsInfoApplicable = false;
    called += 1;

    actor_cost[index_for_actor_cost] = elapsed;
    index_for_actor_cost += 1;
    if (index_for_actor_cost == 32)
    {
        index_for_actor_cost = 0;
    }
}

void ActorImpl::resetWork()
{
    this->workTokens = 0;
    this->workTime = 0;
}

bool ActorImpl::needsReactivation() { return this->state == ActorState::NeedsActivation; }

bool ActorImpl::isRunningGlobal(GlobalActorRef ref)
{
    if (ref.where() == upcxx::rank_me())
    {
        ActorImpl *a = *(ref.local());
        return a->state == ActorState::Running;
    }
    else
    {
        bool isrunning = upcxx::rpc(
                             ref.where(),
                             [](GlobalActorRef ref)
                             {
                                 ActorImpl *a = *(ref.local());
                                 return a->state == ActorState::Running;
                             },
                             ref)
                             .wait();
        return isrunning;
    }
}

/*
    generates ports information from in and out ports, copies the portsinfo data
   directly if the portsInformation struct in an applicable state
*/
std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>>
ActorImpl::generatePortsInformation() const
{
    if (!this->portsInfoBadState())
    {
        auto tup = this->copyPortsInformation();
        return tup;
    }
    else
    {

        size_t sizeOfIn = inPorts.size();
        std::vector<std::unique_ptr<InPortInformationBase>> inPortInformations;
        int curindex = 0;
        for (auto &&pr : inPorts)
        {
            auto *ptr = pr.second;
            std::unique_ptr<InPortInformationBase> inPortInfo(ptr->generatePortInfo());
            inPortInformations.emplace_back(std::move(inPortInfo));
            curindex += 1;
        }

        size_t sizeOfOut = outPorts.size();
        std::vector<std::unique_ptr<OutPortInformationBase>> outPortInformations;
        curindex = 0;
        for (auto &&pr : outPorts)
        {
            auto *ptr = pr.second;
            std::unique_ptr<OutPortInformationBase> outPortInfo(ptr->generatePortInfo());
            outPortInformations.emplace_back(std::move(outPortInfo));
            curindex += 1;
        }

        return std::make_pair(std::move(inPortInformations), std::move(outPortInformations));
    }
}

/*
    refills ports with data, uses portsInformation member variable for that
*/
void ActorImpl::refillPorts()
{
    if (!portsInfoApplicable)
    {
        // assert(false);
        return;
    }

    auto inarr = std::move(std::get<0>(portsInformation));
    for (auto &&el : inarr)
    {
        std::string name = el->getName();

        auto it = inPorts.find(name);
#ifndef NDEBUG
        if (it == inPorts.end())
        {
            throw std::runtime_error("Copied inport is not a member inport of the actor!");
        }
#endif

        AbstractInPort *ip = it->second;
        ip->applyPortInfo(std::move(el));
    }

    auto outarr = std::move(std::get<1>(portsInformation));
    for (auto &&el : outarr)
    {
        std::string name = el->getName();

        auto it = outPorts.find(name);

#ifndef NDEBUG
        if (it == outPorts.end())
        {
            throw std::runtime_error("Copied outport is not a member outport of the actor!");
        }
#endif

        AbstractOutPort *op = it->second;
        op->applyPortInfo(std::move(el));
    }

    // apply port info uses move operations. also we need to change to state
    // after
    this->portsInfoApplicable = false;
}

/*
    moves port information from actor a to this actor
*/
void ActorImpl::movePortsInformation(ActorImpl &&a)
{
    if (a.portsInfoBadState())
    {
        auto tup = a.generatePortsInformation();
        assignPortsInformation(std::move(tup));
        return;
    }
    else
    {
        assignPortsInformation(std::move(a.portsInformation));
        a.portsInfoApplicable = false;
        return;
    }
}

/*
    generates port information and assigns to portsInformation member variable,
    to be called before serializing actors
*/
void ActorImpl::generateAndAssignPortsInformation()
{
    if (this->portsInfoBadState())
    {
        std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> tup(
            this->generatePortsInformation());
        this->assignPortsInformation(std::move(tup));
    }
}

/*
    assigns ports information, before assigning frees the data pointer by the
   old struct
*/
void ActorImpl::assignPortsInformation(
    std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> &&tup)
{
    this->portsInformation = std::move(tup);
    this->portsInfoApplicable = true;
}

/*
    copies portinformation of an actor from it's portsinfo class
*/
std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> ActorImpl::copyPortsInformation() const
{
    std::vector<std::unique_ptr<InPortInformationBase>> inPortInformations;
    auto inarr = std::move(std::get<0>(portsInformation));
    for (const auto &el : inarr)
    {
        std::unique_ptr<InPortInformationBase> inPortInfo(el->generateCopy());
        inPortInformations.emplace_back(std::move(inPortInfo));
    }
    std::get<0>(portsInformation) = std::move(inarr);

    std::vector<std::unique_ptr<OutPortInformationBase>> outPortInformations;
    auto outarr = std::move(std::get<1>(portsInformation));
    for (const auto &el : outarr)
    {
        std::unique_ptr<OutPortInformationBase> outPortInfo(el->generateCopy());
        outPortInformations.emplace_back(std::move(outPortInfo));
    }
    std::get<1>(portsInformation) = std::move(outarr);

    return std::make_pair(std::move(inPortInformations), std::move(outPortInformations));
}

std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> ActorImpl::moveTuple() const
{
#ifndef NDEBUG
    assert(!portsInfoBadState());
#endif
    std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> tmp(std::move(portsInformation));
    return tmp;
}

/*
    copies port info from another actor, if the other actor does not have
   portsInformation in a valid state generates the ports from otheractorsports
*/

void ActorImpl::copyPortsInformation(const ActorImpl &a)
{
    if (a.portsInfoBadState())
    {
        std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> tup(a.generatePortsInformation());
        this->assignPortsInformation(std::move(tup));
        return;
    }
    else
    {
        std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> tup(a.copyPortsInformation());

        this->assignPortsInformation(std::move(tup));
        return;
    }
}

// checks if the portsInformation is in a moved state, if so you probably need
// to regenerate it also returns true if the other actors portInformation is not
// applicable, meaning the ports hav echanged their values
bool ActorImpl::portsInfoBadState() const
{
    return (!this->portsInfoApplicable || (std::get<0>(portsInformation).empty() && std::get<1>(portsInformation).empty()));
}

void ActorImpl::prepare()
{
    // this function is called, therefore this actor will be migrated we can move the port information instead of copying them
    this->generateAndAssignPortsInformation();
    // parentActorGraph->unsubscribe(this->name);
    this->flushBuffers();
    prepareDerived();
}

void ActorImpl::prepareDerived()
{
    /*
    std::cout << "You might want to implement this function!" << std::endl;
    */
}

std::tuple<uint64_t, uint64_t> ActorImpl::getWork() const { return {this->workTokens, this->workTime}; }

bool ActorImpl::getIsRunning() const { return this->state.load() == ActorState::Running; }

ActorState ActorImpl::getRunningState() const { return this->state.load(); }

const std::string &ActorImpl::getNameRef() const { return name; }

void ActorImpl::broadcastTaskDeque(TaskDeque *tdq)
{
    assert(!inPorts.empty());
    for (const auto &ip : inPorts)
    {
        ip.second->setTaskDequeOfChannel(tdq);
    }
}

bool ActorImpl::messageOnWire() const
{
    bool ret = false;
    for (const auto &pr : outPorts)
    {
        auto *out = pr.second;
        ret |= out->messageOnWire();
    }
    return ret;
}

bool ActorImpl::messageOnWire(const std::string &outportName) const
{
    auto &pr = *outPorts.find(outportName);
    return pr.second->messageOnWire();
}

void ActorImpl::severeConnectionIn(const std::string &portName)
{
    auto it = inPorts.find(portName);
#ifndef NDEBUG
    if (it == inPorts.end())
    {
        throw std::runtime_error("oh no");
    }
#endif

    bool rt = it->second->severeConnection();
#ifndef NDEBUG
    assert(rt);
#endif
}

void ActorImpl::severeConnectionOut(const std::string &portName)
{
    auto qit = outPorts.find(portName);
#ifndef NDEBUG
    if (qit == outPorts.end())
    {
        throw std::runtime_error("oh no");
    }
#endif

    bool rt = qit->second->severeConnection();
#ifndef NDEBUG
    assert(rt);
#endif
}

void ActorImpl::resurrectConnection(const std::string &inPortName, upcxx::intrank_t rank, AbstractOutPort *outptr)
{
    auto it = inPorts.find(inPortName);
#ifndef NDEBUG
    if (it == inPorts.end())
    {
        throw std::runtime_error("oh god no! resurrect in");
    }

#endif

    bool rt = it->second->resurrectConnection(rank, outptr);
#ifndef NDEBUG
    // if two neighbors are migrated at the same time then it could be the case that we will have attempts to resurrect twice,
    // thus resulting with the value false being returned
    if constexpr (config::migrationtype == config::MigrationType::ASYNC)
    {
        assert(rt);
    }
#endif
}
void ActorImpl::resurrectConnection(const std::string &outPortName, GlobalChannelRef channelptr)
{
    auto it = outPorts.find(outPortName);
#ifndef NDEBUG
    if (it == outPorts.end())
    {
        throw std::runtime_error("oh god no! resurrect out");
    }
#endif

    bool rt = it->second->resurrectConnection(channelptr);
#ifndef NDEBUG
    // if two neighbors are migrated at the same time then it could be the case that we will have attempts to resurrect twice,
    // thus resulting with the value false being returned
    if constexpr (config::migrationtype == config::MigrationType::ASYNC)
    {
        assert(rt);
    }
#endif
}

AbstractInPort *ActorImpl::getInPort(const std::string &name)
{
    auto it = inPorts.find(name);
#ifndef NDEBUG
    if (it == inPorts.end())
    {
        throw std::runtime_error("oh god no! severe in");
    }
#endif
    return it->second;
}
AbstractOutPort *ActorImpl::getOutPort(const std::string &name)
{
    auto it = outPorts.find(name);
#ifndef NDEBUG
    if (it == outPorts.end())
    {
        throw std::runtime_error("oh god no! severe out");
    }
#endif

    return it->second;
}

void ActorImpl::sendNotifications()
{
    bool empty = true;
    for (auto &it : inPorts)
    {
        AbstractInPort *in = it.second;
        in->notify();
    }
}

void ActorImpl::sendNotification(const std::string &portName)
{
    auto &it = *inPorts.find(portName);

    AbstractInPort *in = it.second;
    in->notify();
}

bool ActorImpl::noNeedToNotify() const noexcept
{
    bool empty = true;
    for (auto &it : inPorts)
    {
        AbstractInPort *in = it.second;
        empty &= in->noNeedToNotify();
    }
    return empty;
}

bool ActorImpl::noNeedToNotify(const std::string &portName) const noexcept
{
    auto &it = *inPorts.find(portName);

    AbstractInPort *in = it.second;
    bool empty = in->noNeedToNotify();

    return empty;
}

bool ActorImpl::buffersEmpty() const noexcept
{
    bool empty = true;
    for (const auto &it : outPorts)
    {
        AbstractOutPort *out = it.second;
        empty &= out->bufferSize() == 0;
    }
    return empty;
}

bool ActorImpl::bufferEmpty(const std::string &portName) const noexcept
{
    bool empty = true;
    auto *out = outPorts.find(portName)->second;
    empty &= out->bufferSize() == 0;

    return empty;
}

void ActorImpl::flushBuffer(const std::string &portName)
{
    auto *out = outPorts.find(portName)->second;
    out->writeBuffer();

    if (ret_to_wants_to_terminate)
    {
        if (buffersEmpty())
        {
            state = ActorState::Terminated;
            this->ret_to_wants_to_terminate = false;
        }
    }

    // called_once = true;
}

void ActorImpl::flushBuffers()
{
    for (const auto &it : outPorts)
    {
        AbstractOutPort *out = it.second;
        out->writeBuffer();
    }

    if (ret_to_wants_to_terminate)
    {
        if (buffersEmpty())
        {
            state = ActorState::Terminated;
            this->ret_to_wants_to_terminate = false;
        }
    }
    // called_once = true;
}

bool ActorImpl::markableForMigration(int caller) const { return pinnedBy == 0 && markedBy == -1; }

bool ActorImpl::markedForMigration() const { return markedBy >= 0; }

bool ActorImpl::centralStealable() const { return state != ActorState::Terminated && state != ActorState::WantsToTerminate && markedBy != upcxx::rank_n(); }

void ActorImpl::set_port_size()
{
    for (auto &ip : inPorts)
    {
        ip.second->total = inPorts.size();
    }
}

bool ActorImpl::connected()
{
    bool a = true;
    for (auto &ip : inPorts)
    {
        a &= ip.second->connected();
    }
    for (auto &op : outPorts)
    {
        a &= op.second->connected();
    }
    return a;
}

uint64_t ActorImpl::calc_cost()
{
    // ind points to the current index to overwrite so we will begin
    // with i-1 th index (if ind is 0 then start at 32)
    // calculate the number a/i and create to cost of the actor

    if (index_for_actor_cost == 0 && actor_cost[history - 1] == 0)
    {
        return 1;
    }

    size_t beg = index_for_actor_cost == 0 ? (history - 1) : index_for_actor_cost - 1;
    size_t i = beg;
    uint64_t sum = 0;
    size_t diff = 1;

    while (i != index_for_actor_cost)
    {
        sum += (actor_cost.at(i) / util::intPow(2, diff));

        i += 1;
        if (i == history)
        {
            i = 0;
        }
        diff += 1;
    }

    return util::nanoToSec(sum);
}

size_t ActorImpl::getMark() const { return markedBy; }

void ActorImpl::setRunning() { this->state = ActorState::Running; }

bool ActorImpl::isTerminated() { return this->state == ActorState::Terminated; }
