#pragma once
#include "DynamicActorGraph.hpp"
#include <array>
#include <cstddef>
#include <mutex>
#include <optional>
#include <tuple>
#include <type_traits>
#include <upcxx/upcxx.hpp>
#include <utility>

/*
    Intern functions to concatenate tuples
*/
template <typename F, typename Tuple, std::size_t... I> auto apply_tup_impl(F &&f, Tuple &&t, std::index_sequence<I...>)
{
    return std::forward<F>(f)(std::get<I>(std::forward<Tuple>(t))...);
}
template <typename F, typename Tuple> auto apply_tup(F &&f, Tuple &&t)
{
    using Indices = std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>;
    return apply_tup_impl(std::forward<F>(f), std::forward<Tuple>(t), Indices());
}

template <typename A> class SingleActorAGraph : public DynamicActorGraph<SingleActorAGraph<A>>
{
  private:
    upcxx::future<GlobalActorRef> retrieveFromOutsideAsyncImpl(GlobalActorRef r); // insert an Actor r into the AG

  public:
    upcxx::future<GlobalActorRef> sendActorToAsyncImpl(int rank, std::string const &name); // sent actor with name name to rank rank

    upcxx::future<GlobalActorRef> sendActorToAsyncImpl(int rank, GlobalActorRef ref); // sent actor pointed by ref name to rank rank

    // static_cast<SingleActorAGraph<A> *>(
    SingleActorAGraph() : DynamicActorGraph<SingleActorAGraph<A>>() /*,migration(static_cast<DynamicActorGraph<SingleActorAGraph<A>> *>(this))*/ {}

    // inits actors from given names vector, connection vector and input arguments
    // distributes and creates actors,constructors must have type A(name,T,Ts...)
    // connections are also init after the actors are inserted to the graph
    template <typename T, typename Z, typename... Ts>
    void initFromList(std::vector<std::string> &&actorNames, std::vector<std::tuple<std::string, std::string, std::string, std::string>> &&connections,
                      std::vector<std::tuple<T, Z, Ts...>> &&inputArguments)
    {
        // delegate actors for distribution
        this->actorDistribution->distributeActors(actorNames, connections);
        upcxx::barrier();

        // initialize actors of your rank
        const auto& partitioning = this->actorDistribution->getPartitioningRef();
        int myrank = upcxx::rank_me();
        upcxx::barrier();

        size_t size = partitioning.size();
        for (size_t i = 0; i < size; i++)
        {
            if (partitioning[i] == myrank)
            {
                std::tuple<std::string> tmptup = std::make_tuple(actorNames[i]);
                std::tuple<std::string, T, Z, Ts...> tmp = std::tuple_cat(std::move(tmptup), std::move(inputArguments[i]));
                A *a = apply_tup([](auto &&...args) -> A * { return ::new A(std::move(args)...); }, std::move(tmp));
                ActorImpl *ab = this->upcastActor(a);
                this->addActor(ab);
            }
        }
        upcxx::barrier();

        this->initConnections(std::forward<std::vector<std::tuple<std::string, std::string, std::string, std::string>>>(connections));
        upcxx::barrier();
    }

    // inits actors from given names vector, connection vector and input arguments
    // distributes and creates actors,constructors must have type A(name,T)
    // connections are also init after the actors are inserted to the graph
    template <typename T>
    void initFromList(std::vector<std::string> &&actorNames, std::vector<std::tuple<std::string, std::string, std::string, std::string>> &&connections,
                      std::vector<T> &&inputArguments)
    {

        // delegate actors for distribution
        this->actorDistribution->distributeActors(actorNames, connections);
        upcxx::barrier();

        // initialize actors of your rank
        auto partitioning = this->actorDistribution->getPartitioning();
        int myrank = upcxx::rank_me();
        upcxx::barrier();

        size_t size = partitioning.size();
        for (size_t i = 0; i < size; i++)
        {
            if (partitioning[i] == myrank)
            {
                A *aptr = ::new A(std::forward<std::string>(actorNames[i]), std::forward<T>(inputArguments[i]));
                ActorImpl *a = this->upcastActor(aptr);
                this->addActor(a);
            }
        }
        upcxx::barrier();

        this->initConnections(std::forward<std::vector<std::tuple<std::string, std::string, std::string, std::string>>>(connections));
        upcxx::barrier();
    }

    // inits actors from given names vector, connection vector and input arguments
    // distributes and creates actors,constructors must have type A(name)
    // connections are also init after the actors are inserted to the graph
    void initFromList(std::vector<std::string> &&actorNames, std::vector<std::tuple<std::string, std::string, std::string, std::string>> &&connections);

    ~SingleActorAGraph() {}
};

/*

    Implemation begins here

*/

template <typename A>
void SingleActorAGraph<A>::initFromList(std::vector<std::string> &&actorNames,
                                        std::vector<std::tuple<std::string, std::string, std::string, std::string>> &&connections)
{
    // delegate actors for distribution

    // auto fp = std::bind(&this->actorDistribution::equal, this->actorDistribution.colIndex, this->actorDistribution.rowIndex)
    // auto fp = std::bind(&Distribution::equal);
    // std::cout << "1" << std::endl;

    this->actorDistribution->distributeActors(std::forward<std::vector<std::string>>(actorNames),
                                              std::forward<std::vector<std::tuple<std::string, std::string, std::string, std::string>>>(connections));

    // initialize actors of your rank
    // std::cout << "2" << std::endl;
    // auto partitioning = this->actorDistribution->getPartitioning();
    const std::vector<upcxx::intrank_t> &partitioning = this->actorDistribution->getPartitioningRef();

    // need to wait for arrival
    upcxx::barrier();
    int myrank = upcxx::rank_me();

    // std::cout << "3" << std::endl;
    for (size_t i = 0; i < actorNames.size(); i++)
    {
        // std::cout << "loop-" << i << std::endl;
        if (partitioning[i] == myrank)
        {
            // std::cout << "loop-samerank" << i << std::endl;
            A *atmp = ::new A(std::forward<std::string>(actorNames[i]));
            ActorImpl *a = this->upcastActor(atmp);
            this->addActor(a);
        }
    }
    // std::cout << "4" << std::endl;
    upcxx::barrier();

    this->initConnections(std::forward<std::vector<std::tuple<std::string, std::string, std::string, std::string>>>(connections));
    upcxx::barrier();
}

template <typename A> upcxx::future<GlobalActorRef> SingleActorAGraph<A>::retrieveFromOutsideAsyncImpl(GlobalActorRef r)
{
    if (r.where() == upcxx::rank_me())
    {
        upcxx::future<GlobalActorRef> ref = upcxx::make_future(r);
        return ref;
    }
    else
    {
        // upcxx::make_view(&value, &value + 1)
        upcxx::future<A *> a = upcxx::rpc(
            r.where(),
            [](upcxx::intrank_t callee, GlobalActorRef r) -> upcxx::future<A *>
            {
                ActorImpl *a = *r.local();
                A *downed = dynamic_cast<A *>(a);
                return upcxx::rpc(
                    callee,
                    [](upcxx::view<A> a) -> A *
                    {
                        auto storage = new typename std::aligned_storage<sizeof(A), alignof(A)>::type;
                        A *aptr = a.begin().deserialize_into(storage);
                        return aptr;
                    },
                    upcxx::make_view(std::make_move_iterator(downed), std::make_move_iterator(downed + 1)));
            },
            upcxx::rank_me(), r);
        upcxx::future<ActorImpl *> ar = a.then([this](A *aptr) -> ActorImpl * { return this->upcastActor(aptr); });
        upcxx::future<GlobalActorRef> gar = ar.then([](ActorImpl *ai) -> GlobalActorRef { return upcxx::new_<ActorImpl *>(ai); });
        return gar;
    }
}

template <typename A> upcxx::future<GlobalActorRef> SingleActorAGraph<A>::sendActorToAsyncImpl(int rank, const std::string &name)
{
    GlobalActorRef ref = this->ag.getActor(name);
    return sendActorToAsyncImpl(rank, ref);
}
template <typename A> upcxx::future<GlobalActorRef> SingleActorAGraph<A>::sendActorToAsyncImpl(int rank, GlobalActorRef ref)
{
    if (ref.where() == upcxx::rank_me())
    {
        if (rank == upcxx::rank_me())
        {
            throw std::runtime_error("ActorImpl already on your rank but you "
                                     "try to send it to yourself?!");
        }
        else
        {

            ActorImpl *act = *ref.local();

            A *downcasted = dynamic_cast<A *>(act);

            upcxx::future<GlobalActorRef> fu = upcxx::rpc(
                rank,
                [](upcxx::view<A> a, upcxx::dist_object<DynamicActorGraph<SingleActorAGraph<A>> *> &rsaag)
                {
                    auto storage = new typename std::aligned_storage<sizeof(A), alignof(A)>::type;
                    A *acttmp = a.begin().deserialize_into(storage);
                    ActorImpl *act = (*rsaag)->upcastActor(acttmp);
                    GlobalActorRef r = upcxx::new_<ActorImpl *>(act);
                    return r;
                },
                upcxx::make_view(std::make_move_iterator(downcasted), std::make_move_iterator(downcasted + 1)),
                DynamicActorGraph<SingleActorAGraph<A>>::remoteComponents);

            // Do not delete "del remains" delete them
            // and act should be deleted after the future is completed
            return fu;
        }
    }
    else
    {
        if (rank == upcxx::rank_me())
        {
            return retrieveFromOutsideAsyncImpl(ref);
        }
        else
        {
            upcxx::future<GlobalActorRef> aglb = retrieveFromOutsideAsyncImpl(ref);
            upcxx::future<A *> act = aglb.then([](GlobalActorRef ref) -> A * { return dynamic_cast<A *>(*(ref.local())); });

            upcxx::future<GlobalActorRef> fu = act.then(
                [rank, this](A *act) -> upcxx::future<GlobalActorRef>
                {
                    upcxx::future<GlobalActorRef> tmp = upcxx::rpc(
                        rank,
                        [](upcxx::view<A> a, upcxx::dist_object<DynamicActorGraph<SingleActorAGraph<A>> *> &rsaag) -> GlobalActorRef
                        {
                            auto storage = new typename std::aligned_storage<sizeof(A), alignof(A)>::type;
                            A *acttmp = a.begin().deserialize_into(storage);
                            ActorImpl *act = (*rsaag)->upcastActor(acttmp);
                            GlobalActorRef r = upcxx::new_<ActorImpl *>(act);
                            return r;
                        },
                        upcxx::make_view(std::make_move_iterator(act), std::make_move_iterator(act + 1)),
                        DynamicActorGraph<SingleActorAGraph<A>>::remoteComponents);
                    return tmp;
                });

            upcxx::future<GlobalActorRef, A *> sent = upcxx::when_all(fu, act);
            upcxx::future<GlobalActorRef> all = sent.then(
                [](GlobalActorRef ref, A *act) -> GlobalActorRef
                {
                    delete act;
                    return ref;
                });
            return all;
        }
    }
}
