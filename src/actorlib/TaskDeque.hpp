#pragma once
#include "AcquisitonResult.hpp"
#include "ActorImpl.hpp"
#include "ActorState.hpp"
#include "Task.hpp"
#include "TerminationWave.hpp"
#include "Utility.hpp"
#include "config.hpp"
#include <algorithm>
#include <array>
#include <chrono>
#include <deque>
#include <functional>
#include <iterator>
#include <memory>
#include <mutex>
#include <optional>
#include <random>
#include <set>
#include <shared_mutex>
#include <stdlib.h>
#include <string>
#include <upcxx/upcxx.hpp>

class ActorGraph;
class AbstractInPort;

constexpr size_t buflen = 64;

class TaskDeque
{
  public:
    ActorGraph *parentActorGraph;
    std::vector<Task> tasks;
    mutable std::shared_mutex runningWriteLock;
    mutable std::shared_mutex taskDequeModifyLock;
    mutable std::shared_mutex migrationLock;
    mutable std::shared_mutex bufferLock;
    upcxx::dist_object<TaskDeque *> remoteTaskque;
    TerminationWave tw;
    std::chrono::steady_clock::time_point tbegin;
    std::chrono::steady_clock::time_point tend;
    double executingTime;
    std::mt19937 randomEngine;
    bool try_to_terminate = false;
    size_t tries = 0;
    std::vector<std::string> to_steal{};
    bool find_new_actors_to_steal = true;
    size_t stole = 0;
    size_t tried_to_steal = 0;
    int termtrial = 0;
    std::string coming{};
    bool changed = false;
    std::array<Task, buflen> buffer;
    upcxx::future<bool> f;
    std::set<std::pair<std::string, int>> going_away{};
    bool routine = false;
#ifdef CHAOS
  public:
    std::uniform_int_distribution<int> rdm;
    std::bernoulli_distribution distribution;
    double waitingTime = 0;
    size_t blackscrawl = 0;
#endif

  public:
    TaskDeque(ActorGraph *ag) noexcept;
    ~TaskDeque() noexcept = default;
    bool advance(double timeout = -1.0);
    void addTask(Task &&task);
    bool empty();
    bool nothingIsComing() const;
    void insertLingeringTask(Task name);
    bool sendingTasks() const;
    bool hasTasks(const std::string &name) const;
    bool hasExecutableTasks(const std::string &name) const;
    bool hasExecutableTasks() const;
    bool hasNoTasks(const std::string &name) const;
    bool hasActTasks() const;
    bool noExecutableTasks() const;
    size_t hasNoEmigrantTasks() const;
    std::vector<Task> moveTasks(std::string &name);
    upcxx::future<> sendTasks(const std::unordered_map<std::string, upcxx::intrank_t> &migList);

  private:
    upcxx::future<> steal(std::string nameOfActorToSteal);
    void cleanTasks();
    void inline printTaskDecomposition();
    bool checkTermination();
    bool bufferHasActTasks();
};
