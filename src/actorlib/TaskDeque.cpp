#include "TaskDeque.hpp"

static ARPrinter arp;
static ASPrinter asp;

void inline TaskDeque::printTaskDecomposition()
{
    size_t buf = 0;
    size_t act = 0;
    size_t none = 0;
    for (auto &el : this->tasks)
    {
        if (!el.none() && el.tasktype == TaskType::Act)
        {
            act += 1;
        }
        else if (!el.none())
        {
            buf += 1;
        }
        else
        {
            none += 1;
        }
    }

    std::cout << upcxx::rank_me() << ": There are tasks (" << this->tasks.size() << ") (Act: " << act << " , Buffer: " << buf << ", None: " << none << ")"
              << std::endl;
}

#include "ActorGraph.hpp"

int generate_seed()
{
    char *env_var = std::getenv("SEED");
    int seed = 0;
    if (env_var)
    {
        seed = atoi(env_var);
    }

    return seed + upcxx::rank_me();
}

TaskDeque::TaskDeque(ActorGraph *ag) noexcept
    : parentActorGraph(ag), tasks(), runningWriteLock(), taskDequeModifyLock(), migrationLock(), bufferLock(), remoteTaskque(this), tw(ag, this), tbegin(),
      tend(), executingTime(0), randomEngine
{
    static_cast<unsigned long int>(generate_seed())
}
#ifdef CHAOS
, rdm(1, 5), distribution(1.0 / (double)(rdm(randomEngine) * 100.0))
#endif
{
#ifdef PARALLEL
    static_assert(config::parallelization == config::ParallelizationType::OMP_TASKS);
#endif
}

#ifdef PARALLEL
bool TaskDeque::advance(double timeout) { throw std::runtime_error("not yet implemented"); }
#else
bool TaskDeque::advance(double timeout)
{
    upcxx::barrier();
    this->coming.clear();
    auto l = std::chrono::steady_clock::now();
    cleanTasks();

    while (true)
    {
        auto k = std::chrono::steady_clock::now();
        size_t dur = std::chrono::duration_cast<std::chrono::seconds>(k - l).count();

        if (timeout > 0 && dur > timeout && (config::migrationtype == config::MigrationType::HYBRID || config::migrationtype == config::MigrationType::BULK))
        {
            bool should_termiante = checkTermination();

            if (should_termiante)
            {
                goto ret_true;
            }
            else
            {
                goto ret_false;
            }
        }

        upcxx::progress();
        upcxx::discharge();

        // copy the first couple of executable tasks to the buffer, this is done because progress may add new tasks to the que
        // it can also remove some tasks if they are to go to a new rank, this means it will change the size of the taskque
        // this invalidating the iterators, it may also make you remain in the loop if you constantly get new tasks, but if they
        // are not executable, to ensure we have an finite loop, we copy to the buffer that does not get touched by progress
        // iterate through it and add it back to the real thing
        size_t j = 0;
        size_t bo = 0;
        while (j < buffer.size() && j < tasks.size() && bo < buffer.size())
        {
            if (!tasks[j].none() && !tasks[j].finished && !tasks[j].source.empty() &&
                (tasks[j].unique_needed == 0 || tasks[j].uniques.size() == tasks[j].unique_needed))
            {
                buffer[bo] = std::move(tasks[j]);
                bo += 1;
                tasks[j].reset();
            }
            j += 1;
        }

        // execute them (giotine)
        for (size_t i = 0; i < bo; i++)
        {
            // std::cout << upcxx::rank_me() << " execute stuff" << std::endl;

            upcxx::progress();
            // cleanTasks();

            if (!buffer[i].none())
            {
                Task &t = buffer[i];

                // std::cout << upcxx::rank_me() << " Task: " << t.source << std::endl;

                // The actor could be being migrated right now

                GlobalActorRef ref = this->parentActorGraph->getActorNoThrow(t.source);

                // int rank = ref == nullptr ? -1 : ref.where();
                // std::cout << upcxx::rank_me() << " Ref: " << ref << " rank: " << rank << std::endl;

                if (ref != nullptr)
                {
                    if (!t.finished && ref != nullptr && ref.where() == upcxx::rank_me())
                    {
                        ActorImpl *ai = *ref.local();
#ifndef NDEBUG
                        if (ai->getRunningState() != ActorState::Terminated && ai->getRunningState() != ActorState::WantsToTerminate &&
                            ai->getRunningState() != ActorState::TemporaryStoppedForMigration)
                        {
                            // std::cout << asp.as2str(ai->getRunningState()) << std::endl;
                            assert(this->parentActorGraph->locallyActiveActors.find(ai) != this->parentActorGraph->locallyActiveActors.end());
                        }
#endif
                        // std::cout << upcxx::rank_me() << " Task: " << t.source << " unique needed: " << t.unique_needed << " has: " << t.uniques.size()
                        //          << std::endl;
                        if (t.unique_needed == 0 || (t.unique_needed == t.uniques.size()))
                        {
#ifdef CHAOS
                            if (t.tasktype == TaskType::Act)
                            {
                                if (distribution(randomEngine))
                                {
#ifdef REPORT_MAIN_ACTIONS
                                    std::cout << upcxx::rank_me() << " is infected with black scrawl" << std::endl;
#endif
                                    blackscrawl = 100;
                                }
                            }
#endif

                            tbegin = std::chrono::steady_clock::now();
                            // std::cout << upcxx::rank_me() << ": execute exec for " << t.source << std::endl;
                            t.execute(ai);
                            tend = std::chrono::steady_clock::now();
                            size_t dur = std::chrono::duration_cast<std::chrono::nanoseconds>(tend - tbegin).count();
                            executingTime += dur;

#ifdef CHAOS
                            if (t.tasktype == TaskType::Act)
                            {
                                if (blackscrawl > 0)
                                {
                                    std::this_thread::sleep_for(std::chrono::nanoseconds(dur));
                                    waitingTime += static_cast<double>(dur);
                                    blackscrawl -= 1;
                                }
                            }
#endif
                        }
                    }
                }

                upcxx::discharge();
            }
        }

        // copy back unfinished tasks
        for (size_t i = 0; i < buffer.size(); i++)
        {
            if (!buffer[i].none() && !buffer[i].finished)
            {
                tasks.push_back(std::move(buffer[i]));
            }
            buffer[i].reset();
        }

        upcxx::progress();
        cleanTasks();

        if constexpr (config::migrationtype == config::MigrationType::ASYNC || config::migrationtype == config::MigrationType::HYBRID)
        {

            if (!hasExecutableTasks() && coming.empty())
            {

                if (to_steal.empty() || find_new_actors_to_steal)
                {
                    to_steal = this->parentActorGraph->migcaller->findActorsToSteal();
                }

                if (to_steal.empty())
                {
                    tries += 1;
                }
                else
                {

                    to_steal.erase(std::remove_if(to_steal.begin(), to_steal.end(),
                                                  [this](std::string &name)
                                                  {
                                                      GlobalActorRef r = this->parentActorGraph->getActorNoThrow(name);
                                                      return r == nullptr || r.where() == upcxx::rank_me();
                                                  }),
                                   to_steal.end());

                    if constexpr (!config::prefer_rank_with_maximum_actors)
                    {
                        std::shuffle(to_steal.begin(), to_steal.end(), randomEngine);
                    }

                    std::string victim = to_steal[0];
                    assert(coming.empty());
                    coming = victim;
                    tried_to_steal += 1;

                    this->parentActorGraph->rpcsInFlight += 1;
                    upcxx::future<AcquisitionResult> locked = this->parentActorGraph->migcaller->tryLockActorForMigration(victim);
                    upcxx::future<bool> stopped = locked.then(
                        [this, victim](AcquisitionResult res) -> upcxx::future<bool>
                        {
                            if (res == AcquisitionResult::Ok)
                            {
                                return this->parentActorGraph->migcaller->tryStopSelfAndMarkNeighborhood(victim);
                            }
                            else if (res == AcquisitionResult::ActorTerminated)
                            {
                                termtrial += 1;
                                try_to_terminate = true;
                                tries += 1;
#ifdef REPORT_MAIN_ACTIONS
                                std::cout << upcxx::rank_me() << " tried to steal an actor, can't steal is terminated" << std::endl;
#endif
                            }
                            return upcxx::make_future(false);
                        });

                    upcxx::future<bool> stole = stopped.then(
                        [this, victim](bool stopped) -> upcxx::future<bool>
                        {
                            if (stopped)
                            {
#ifdef REPORT_MAIN_ACTIONS
                                std::cout << upcxx::rank_me() << " tried, will steal: " << victim << std::endl;
#endif

                                upcxx::future<> a = steal(victim);
                                upcxx::future<bool> b = upcxx::make_future(true);
                                find_new_actors_to_steal = true;

                                return upcxx::when_all(a, b);
                            }
                            else
                            {
#ifdef REPORT_MAIN_ACTIONS
                                std::cout << upcxx::rank_me() << " tried to steal an actor, can't steal" << std::endl;
#endif

                                tries += 1;

                                to_steal.erase(std::remove_if(to_steal.begin(), to_steal.end(),
                                                              [victim](const std::string &t)
                                                              {
                                                                  return t == victim; // put your condition here
                                                              }),
                                               to_steal.end());

                                assert(coming == victim);
                                coming.clear();
                                routine = true;
                                upcxx::future<bool> b = upcxx::make_future(false);

                                return b;
                            }
                        });

                    f = stole;
                    stole.then([this](bool val) { this->parentActorGraph->rpcsInFlight -= 1; });
                }
            }

            upcxx::discharge();
            upcxx::progress();

            // try to terminate
            if ((this->parentActorGraph->locallyActiveActors.size() == 0 && this->coming.empty()) ||
                (tries > this->parentActorGraph->actorCount || try_to_terminate))
            {
                if (this->tw.otherHasFullTerminated())
                {
                    goto ret_true;
                }

                if (checkTermination())
                {
                    goto ret_true;
                }

                if (tries > this->parentActorGraph->actorCount)
                {
                    tries = 0;
                }

                if (try_to_terminate)
                {
                    try_to_terminate = false;
                }

#ifdef REPORT_MAIN_ACTIONS
                // std::cout << upcxx::rank_me() << " has " << this->parentActorGraph->locallyActiveActors.size() << std::endl;
#endif

                upcxx::discharge();
                upcxx::progress();
                cleanTasks();
                upcxx::progress();
            }
        }
    }

    if constexpr (config::migrationtype == config::MigrationType::NO)
    {
        if (tasks.empty())
        {
            goto ret_true;
        }
    }

    if constexpr (config::migrationtype != config::MigrationType::NO)
    {
        if (tasks.empty())
        {
            if (tw.checkTermination())
            {
                goto ret_true;
            }
        }
        else
        {
#ifdef REPORT_MAIN_ACTIONS
            std::cout << upcxx::rank_me() << ": we have tasks" << std::endl;
#endif
        }
    }

ret_true:
    std::cout << "Rank-" << upcxx::rank_me() << " spent " << util::nanoToSec(executingTime)
#ifdef CHAOS
              << " + " << util::nanoToSec(waitingTime) << " = " << util::nanoToSec(executingTime + waitingTime)
#endif
              << " seconds executing actors" << std::endl
              << "Rank-" << upcxx::rank_me() << " checked " << this->tw.checked << " times for termination, stole: " << stole
              << " actors, tried: " << tried_to_steal << " times, has " << this->parentActorGraph->actorCount << std::endl;
    upcxx::barrier();
    return true;

ret_false:
    upcxx::barrier();
    return false;
}
#endif

void TaskDeque::addTask(Task &&task)
{
#ifdef PARALLEL
    std::unique_lock<std::shared_mutex> l(taskDequeModifyLock);
#endif

    if (task.unique_needed == 0 || task.unique_needed == 1)
    {
        tasks.push_back(std::move(task));
        return;
    }

    for (const auto &el : task.uniques)
    {
        bool inserted = false;
        for (size_t i = 0; i < tasks.size(); i++)
        {
            Task &t = tasks[i];

            if (!t.none() && t.source == task.source && t.finished == false && t.unique_needed == task.unique_needed)
            {
                auto pr = t.uniques.insert(el);
                if (pr.second == 1)
                {
                    inserted = true;
                    break;
                }
            }
        }

        if (!inserted)
        {
            Task t2 = task;
            t2.uniques.clear();
            t2.uniques.insert(el);
            tasks.push_back(std::move(t2));
        }
    }
}

bool TaskDeque::empty()
{
#ifdef PARALLEL
    std::shared_lock<std::shared_mutex> l(taskDequeModifyLock);
#endif
    return tasks.empty();
}

bool TaskDeque::nothingIsComing() const
{
#ifdef PARALLEL
    std::shared_lock<std::shared_mutex> l(migrationLock);
#endif

    return this->coming.empty();
}

upcxx::future<> TaskDeque::steal(std::string nameOfActorToSteal)
{
    this->tries = 0;
    upcxx::intrank_t s = this->parentActorGraph->getActor(nameOfActorToSteal).where();

    upcxx::future<GlobalActorRef> a = this->parentActorGraph->migcaller->stealActorAsync(nameOfActorToSteal);

    upcxx::future<std::vector<Task>> d = a.then(
        [this, s, nameOfActorToSteal](GlobalActorRef r) -> upcxx::future<std::vector<Task>>
        {
            upcxx::future<std::vector<Task>> tmp = upcxx::rpc(
                s,
                [](std::string &&name, upcxx::dist_object<TaskDeque *> &rmt, int rank) -> std::vector<Task>
                {
                    (*rmt)->going_away.erase(std::make_pair(name, rank));
                    return (*rmt)->moveTasks(name);
                },
                nameOfActorToSteal, remoteTaskque, upcxx::rank_me());
            return tmp;
        });

    upcxx::future<> f = d.then(
        [this](std::vector<Task> t)
        {
            for (auto &&el : t)
            {
                addTask(std::move(el));
            }
        });

    upcxx::future<> h = f.then(
        [this, nameOfActorToSteal]()
        {
            assert(nameOfActorToSteal == this->coming);
            this->stole += 1;
            this->coming.clear();
            GlobalActorRef ref = this->parentActorGraph->getActor(nameOfActorToSteal);
            assert(ref.where() == upcxx::rank_me());
            ActorImpl *ai = *ref.local();
        });

    return h;
}

void TaskDeque::cleanTasks()
{
    int sent = 0;
    for (auto &t : tasks)
    {
        GlobalActorRef ref = this->parentActorGraph->getActorNoThrow(t.source);
        if (ref != nullptr && ref.where() != upcxx::rank_me())
        {
#ifdef REPORT_MAIN_ACTIONS
            std::cout << upcxx::rank_me() << " sending a lingering task for: " << t.source << " to rank " << ref.where() << " (during cleanTasks())"
                      << std::endl;
#endif
            this->parentActorGraph->rpcsInFlight += 1;
            upcxx::future<> d = upcxx::rpc(
                ref.where(), [](upcxx::dist_object<TaskDeque *> &rmt, Task t) { (*rmt)->addTask(std::move(t)); }, remoteTaskque, t);
            t.reset();
            upcxx::future<> sfs = d.then([this]() { this->parentActorGraph->rpcsInFlight -= 1; });
            sent += 1;
        }
    }

    for (size_t k = 0; k < buffer.size(); k++)
    {
        if (!buffer[k].none())
        {
            Task &t = buffer[k];

            GlobalActorRef ref = this->parentActorGraph->getActorNoThrow(t.source);
            if (ref.where() != upcxx::rank_me())
            {
#ifdef REPORT_MAIN_ACTIONS
                std::cout << upcxx::rank_me() << " sending a lingering task for: " << t.source << " to rank " << ref.where() << " (during cleanTasks())"
                          << std::endl;
#endif

                this->parentActorGraph->rpcsInFlight += 1;
                upcxx::future<> d = upcxx::rpc(
                    ref.where(), [](upcxx::dist_object<TaskDeque *> &rmt, Task t) { (*rmt)->addTask(std::move(t)); }, remoteTaskque, t);
                t.reset();
                upcxx::future<> sfs = d.then([this]() { this->parentActorGraph->rpcsInFlight -= 1; });
                sent += 1;
            }
        }
    }

    if (sent > 0)
    {
        upcxx::discharge();
    }

    tasks.erase(std::remove_if(tasks.begin(), tasks.end(),
                               [this](const Task &t)
                               {
                                   bool cc = false;

                                   // check if the actor is terminated, flush should be called even if the actor is terminated
                                   if (t.tasktype == TaskType::Act)
                                   {
                                       GlobalActorRef ref = this->parentActorGraph->getActorNoThrow(t.source);

                                       if (ref != nullptr && ref.where() == upcxx::rank_me())
                                       {
                                           ActorImpl *ai = *ref.local();
                                           cc = ai->isTerminated();
                                       }
                                   }

                                   return (t.none() || t.finished || cc); // put your condition here
                               }),
                tasks.end());
}

bool TaskDeque::hasActTasks() const
{
    for (const auto &el : tasks)
    {
        if (!el.none() && el.tasktype == TaskType::Act)
        {
            return true;
        }
    }
    return false;
}

bool TaskDeque::hasTasks(const std::string &name) const
{
    for (const auto &el : tasks)
    {
        if (!el.none() && el.source == name)
        {
            return true;
        }
    }
    return false;
}

bool TaskDeque::hasExecutableTasks(const std::string &name) const
{
    for (const auto &el : tasks)
    {
        if (!el.none() && el.source == name && (el.unique_needed == 0 || el.uniques.size() == el.unique_needed))
        {
            return true;
        }
    }
    return false;
}

bool TaskDeque::hasExecutableTasks() const
{
    for (const auto &el : tasks)
    {
        if (!el.none() && (el.unique_needed == 0 || el.uniques.size() == el.unique_needed))
        {
            return true;
        }
    }
    for (const auto &el : buffer)
    {
        if (!el.none() && (el.unique_needed == 0 || el.uniques.size() == el.unique_needed))
        {
            return true;
        }
    }
    return false;
}

bool TaskDeque::hasNoTasks(const std::string &name) const
{
    for (const auto &el : tasks)
    {
        if (!el.none() && el.source == name)
        {
            return false;
        }
    }
    for (const auto &el : buffer)
    {
        if (!el.none() && el.source == name)
        {
            return false;
        }
    }
    return false;
}

size_t TaskDeque::hasNoEmigrantTasks() const
{
    size_t ret = 0;
    for (const auto &el : tasks)
    {
        if (!el.none())
        {
            GlobalActorRef r = this->parentActorGraph->getActorNoThrow(el.source);
            if (r == nullptr || r.where() != upcxx::rank_me())
            {
                ret += 1;
            }
        }
    }
    for (const auto &el : buffer)
    {
        if (!el.none())
        {
            GlobalActorRef r = this->parentActorGraph->getActorNoThrow(el.source);
            if (r == nullptr || r.where() != upcxx::rank_me())
            {
                ret += 1;
            }
        }
    }
    return ret;
}

bool TaskDeque::checkTermination()
{
    if (this->tw.checkTermination())
    {
        return true;
    }

    return false;
}

void TaskDeque::insertLingeringTask(Task name) { addTask(std::move(name)); }

bool TaskDeque::noExecutableTasks() const
{

    for (auto &el : tasks)
    {
        if (!el.none() && !el.finished && (el.unique_needed == 0 || el.uniques.size() == el.unique_needed))
        {
            return true;
        }
    }

    for (auto &el : buffer)
    {
        if (!el.none() && !el.finished && (el.unique_needed == 0 || el.uniques.size() == el.unique_needed))
        {
            return true;
        }
    }
    return false;
}

std::vector<Task> TaskDeque::moveTasks(std::string &name)
{
    std::vector<Task> v;
    for (auto &&el : tasks)
    {
        if (el.source == name)
        {
            v.push_back(std::move(el));
            el.reset();
        }
    }

    for (auto &&el : buffer)
    {
        if (el.source == name)
        {
            v.push_back(std::move(el));
            el.reset();
        }
    }

    return v;
}

upcxx::future<> TaskDeque::sendTasks(const std::unordered_map<std::string, upcxx::intrank_t> &migList)
{
    std::vector<upcxx::future<>> futs;

    int sent = 0;
    for (auto &t : tasks)
    {
        if (migList.find(t.source) != migList.end())
        {
            GlobalActorRef ref = this->parentActorGraph->getActorNoThrow(t.source);
            if (ref != nullptr && ref.where() != upcxx::rank_me())
            {
#ifdef REPORT_MAIN_ACTIONS
                std::cout << upcxx::rank_me() << " sending a lingering task for: " << t.source << " to rank " << ref.where() << " (during sendTasks())"
                          << std::endl;
#endif
                this->parentActorGraph->rpcsInFlight += 1;
                upcxx::future<> d = upcxx::rpc(
                    ref.where(), [](upcxx::dist_object<TaskDeque *> &rmt, Task t) { (*rmt)->addTask(std::move(t)); }, remoteTaskque, t);
                t.reset();
                upcxx::future<> sfs = d.then([this]() { this->parentActorGraph->rpcsInFlight -= 1; });
                sent += 1;
                futs.emplace_back(std::move(sfs));
            }
        }
    }

    for (size_t k = 0; k < buffer.size(); k++)
    {
        if (!buffer[k].none())
        {
            Task &t = buffer[k];

            GlobalActorRef ref = this->parentActorGraph->getActorNoThrow(t.source);
            if (migList.find(t.source) != migList.end() && ref.where() != upcxx::rank_me())
            {
#ifdef REPORT_MAIN_ACTIONS
                std::cout << upcxx::rank_me() << " sending a lingering task for: " << t.source << " to rank " << ref.where() << " (during sendTasks())"
                          << std::endl;
#endif

                this->parentActorGraph->rpcsInFlight += 1;
                upcxx::future<> d = upcxx::rpc(
                    ref.where(), [](upcxx::dist_object<TaskDeque *> &rmt, Task t) { (*rmt)->addTask(std::move(t)); }, remoteTaskque, t);
                t.reset();
                upcxx::future<> sfs = d.then([this]() { this->parentActorGraph->rpcsInFlight -= 1; });
                sent += 1;
                futs.emplace_back(std::move(sfs));
            }
        }
    }

    if (sent > 0)
    {
        upcxx::discharge();
    }

    tasks.erase(std::remove_if(tasks.begin(), tasks.end(),
                               [](const Task &t)
                               {
                                   return (t.none() || t.finished); // put your condition here
                               }),
                tasks.end());

    return util::combineFutures(futs);
}