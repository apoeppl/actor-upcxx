#pragma once

enum class SimulationActorState
{
    INITIAL,
#ifdef LAZY_ACTIVATION
    RESTING,
#endif
    RUNNING,
    FINISHED,
    TERMINATED
};
