#include "actor/SimulationActor.hpp"

SimulationActor::SimulationActor(std::string &&name, std::tuple<size_t, size_t> &&coordinates) noexcept
    : SimulationActorBase(std::move(name), std::move(coordinates))
{
    makePorts();
}

SimulationActor::SimulationActor(const std::string &name, const std::tuple<size_t, size_t> &coordinates) noexcept : SimulationActorBase(name, coordinates)
{
    makePorts();
}

SimulationActor::SimulationActor(ActorData &&ad, size_t xPos, size_t yPos, SWE_WaveAccumulationBlock &&block, SimulationActorState currentState,
                                 float currentTime, float timestepBaseline, float outputDelta, float nextWriteTime, float endTime, uint64_t patchUpdates,
#if defined(WRITENETCDF)
                                 io::NetCdfWriter &&writer,
#elif defined(WRITEVTK)
                                 io::VtkWriter &&writer,
#endif
                                 SimulationArea &&sa, bool actv /*,std::vector<std::string> &&activeNeighbors*/) noexcept
    : SimulationActorBase(std::move(ad), false, xPos, yPos, std::move(block), currentState, currentTime, timestepBaseline, outputDelta, nextWriteTime, endTime,
                          patchUpdates,
#if defined(WRITEVTK) || defined(WRITENETCDF)
                          std::move(writer),
#endif
                          std::move(sa), actv)
{
    makePorts();
    reInitializeBlock();
}

SimulationActor::SimulationActor(ActorData &&ad,
                                 std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> &&tup,
                                 size_t xPos, size_t yPos, SWE_WaveAccumulationBlock &&block, SimulationActorState currentState, float currentTime,
                                 float timestepBaseline, float outputDelta, float nextWriteTime, float endTime, uint64_t patchUpdates,
#if defined(WRITENETCDF)
                                 io::NetCdfWriter &&writer,
#elif defined(WRITEVTK)
                                 io::VtkWriter &&writer,
#endif
                                 SimulationArea &&sa, bool actv /*, std::vector<std::string> &&activeNeighbors*/) noexcept
    : SimulationActorBase(std::move(ad), std::move(tup), false, xPos, yPos, std::move(block), currentState, currentTime, timestepBaseline, outputDelta,
                          nextWriteTime, endTime, patchUpdates,
#if defined(WRITEVTK) || defined(WRITENETCDF)
                          std::move(writer),
#endif
                          std::move(sa), actv)
{
    makePorts();
    reInitializeBlock();
}

SimulationActor::SimulationActor(const SimulationActor &sa) noexcept : SimulationActorBase(*dynamic_cast<const SimulationActorBase*>(&sa))
{
    makePorts();
    reInitializeBlock();
}

SimulationActor::SimulationActor(SimulationActor &&sa) noexcept : SimulationActorBase(std::move(*dynamic_cast<SimulationActorBase*>(&sa)))
{
    makePorts();
    reInitializeBlock();
}

SimulationActor::SimulationActor(SimulationActorBaseData &&sabd) noexcept : SimulationActorBase(std::move(sabd))
{
    makePorts();
    reInitializeBlock();
}

void SimulationActor::makePorts()
{
    assert(configuration != nullptr);
    auto totalX = configuration->xSize / configuration->patchSize;
    auto totalY = configuration->ySize / configuration->patchSize;
    auto xPos = position[0];
    auto yPos = position[1];
    dataIn[(int)BoundaryEdge::BND_LEFT] = (xPos != 0) ? this->makeInPort<std::vector<float>, 32>("BND_LEFT") : nullptr;
    dataOut[(int)BoundaryEdge::BND_LEFT] = (xPos != 0) ? this->makeOutPort<std::vector<float>, 32>("BND_LEFT") : nullptr;
    dataIn[(int)BoundaryEdge::BND_RIGHT] = (xPos != totalX - 1) ? this->makeInPort<std::vector<float>, 32>("BND_RIGHT") : nullptr;
    dataOut[(int)BoundaryEdge::BND_RIGHT] = (xPos != totalX - 1) ? this->makeOutPort<std::vector<float>, 32>("BND_RIGHT") : nullptr;
    dataIn[(int)BoundaryEdge::BND_BOTTOM] = (yPos != 0) ? this->makeInPort<std::vector<float>, 32>("BND_BOTTOM") : nullptr;
    dataOut[(int)BoundaryEdge::BND_BOTTOM] = (yPos != 0) ? this->makeOutPort<std::vector<float>, 32>("BND_BOTTOM") : nullptr;
    dataIn[(int)BoundaryEdge::BND_TOP] = (yPos != totalY - 1) ? this->makeInPort<std::vector<float>, 32>("BND_TOP") : nullptr;
    dataOut[(int)BoundaryEdge::BND_TOP] = (yPos != totalY - 1) ? this->makeOutPort<std::vector<float>, 32>("BND_TOP") : nullptr;
}

void SimulationActor::initializeBlock()
{
    size_t xPos = position[0];
    size_t yPos = position[1];
    auto totalX = configuration->xSize / configuration->patchSize;
    auto totalY = configuration->ySize / configuration->patchSize;
    block.initScenario(patchArea.minX, patchArea.minY, *(configuration->scenario), true);
    initializeBoundary(BoundaryEdge::BND_LEFT, [xPos]() { return xPos == 0; });
    initializeBoundary(BoundaryEdge::BND_RIGHT, [xPos, totalX]() { return xPos == totalX - 1; });
    initializeBoundary(BoundaryEdge::BND_BOTTOM, [yPos]() { return yPos == 0; });
    initializeBoundary(BoundaryEdge::BND_TOP, [yPos, totalY]() { return yPos == totalY - 1; });
    this->computeWriteDelta();
#if !defined(NDEBUG) && defined(PRINT)
    std::cout << "initialzie simulation actor" << std::endl;
    std::cout << this->toString() << std::endl;
#endif
    assert(configuration->scenario->defWaterHeight() >= 9.99f && configuration->scenario->defWaterHeight() <= 10.1f);
    this->active = block.displacement(configuration->scenario->defWaterHeight());
}

void SimulationActor::reInitializeBlock()
{
    size_t xPos = position[0];
    size_t yPos = position[1];
    auto totalX = configuration->xSize / configuration->patchSize;
    auto totalY = configuration->ySize / configuration->patchSize;
    block.reinitScenario(patchArea.minX, patchArea.minY, *(configuration->scenario), true);
    initializeBoundary(BoundaryEdge::BND_LEFT, [xPos]() { return xPos == 0; });
    initializeBoundary(BoundaryEdge::BND_RIGHT, [xPos, totalX]() { return xPos == totalX - 1; });
    initializeBoundary(BoundaryEdge::BND_BOTTOM, [yPos]() { return yPos == 0; });
    initializeBoundary(BoundaryEdge::BND_TOP, [yPos, totalY]() { return yPos == totalY - 1; });
    // this->computeWriteDelta();
#if !defined(NDEBUG) && defined(PRINT)
    std::cout << "reinitialize simulation actor" << std::endl;
    std::cout << this->toString() << std::endl;
#endif
}

void SimulationActor::initializeBoundary(BoundaryEdge edge, std::function<bool()> isBoundary)
{
    const Scenario *sc = configuration->scenario;
    if (isBoundary())
    {
        block.setBoundaryType(edge, sc->getBoundaryType(edge));
    }
    else
    {
        block.setBoundaryType(edge, BoundaryType::PASSIVE);
#ifdef PRINT
        std::cout << this->getName() << " communicator with config->patchSize " << configuration->patchSize << std::endl;
#endif
        communicators[(int)edge] = BlockCommunicator(configuration->patchSize, block.registerCopyLayer(edge), block.grabGhostLayer(edge));
    }
}

bool SimulationActor::mayRead()
{
    bool res = true;
    // std::stringstream ss;
    for (int i = 0; i < 4; i++)
    {
        if (this->dataIn[i])
        {
            if (this->dataIn[i]->available() <= 0)
            {
                // ss << dataIn[i]->getName() << " is empty | ";
                // res = false;
                return false;
            }
            else
            {
                // ss << dataIn[i]->getName() << " has messages | ";
            }
        }
    }
    // std::cout << this->getName() << " " << ss.str() << std::endl;
    return res;
}

bool SimulationActor::mayWrite()
{
    bool res = true;
    // std::stringstream ss;
    // ss << this->getName() << " can write to: ";
    for (int i = 0; i < 4; i++)
    {
        res &= (!this->dataOut[i] || this->dataOut[i]->freeCapacity() > 0);
        // if (this->dataOut[i] && this->dataOut[i]->freeCapacity() > 0)
        //{
        //    ss << this->dataOut[i]->getName() << " | ";
        //}
    }
    return res;
}

bool SimulationActor::hasReceivedTerminationSignal()
{
#ifdef TIME
    auto start = std::chrono::high_resolution_clock::now();
#endif

    bool res = false;
    for (int i = 0; i < 4; i++)
    {
        if (this->dataIn[i] && this->dataIn[i]->available() > 0)
        {
            auto v = dataIn[i]->peek();
            if (v.size() == 1 && v[0] == std::numeric_limits<float>::min())
            {
                res |= true;
                dataIn[i]->read();
            }
        }
    }

#ifdef TIME
    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    timeSpentReceiving += elapsed.count();
#endif

    return res;
}

void SimulationActor::sendData()
{
    // std::cout << this->getName() << " sending data" << std::endl;
    for (int i = 0; i < 4; i++)
    {
        if (this->dataOut[i])
        {
            auto packedData = communicators[i].packCopyLayer();
            this->dataOut[i]->write(std::move(packedData));
        }
    }
}

void SimulationActor::sendTerminationSignal()
{
    for (int i = 0; i < 4; i++)
    {
        if (this->dataOut[i] && this->dataOut[i]->freeCapacity() > 0)
        {
            std::vector<float> v;
            v.push_back(std::numeric_limits<float>::min());
            dataOut[i]->write(std::move(v));
        }
    }
}

void SimulationActor::receiveData()
{
    for (int i = 0; i < 4; i++)
    {
        if (this->dataIn[i])
        {
            auto packedData = dataIn[i]->read();
            communicators[i].receiveGhostLayer(std::move(packedData));
        }
    }
}

bool SimulationActor::noChange()
{
    for (int i = 0; i < 4; i++)
    {
        if (this->dataIn[i])
        {
            if (this->dataIn[i]->available() > 0)
            {
                auto packedData = dataIn[i]->peek();
                // communicators[i].receiveGhostLayer(packedData);
                if (diffThanInitial(packedData))
                {
                    return false;
                }
            }
        }
    }
    return true;
}
