#include "SimulationActorBase.hpp"

const Configuration *SimulationActorBase::configuration = nullptr;
static tools::Logger &l = tools::Logger::logger;

SimulationActorBase::SimulationActorBase(std::string &&name, std::tuple<size_t, size_t> &&coordinates, bool slow) noexcept
    : ActorImpl(std::move(name)), slow(slow), position{std::get<0>(coordinates), std::get<1>(coordinates)},
      block(SimulationActorBase::configuration->patchSize, SimulationActorBase::configuration->patchSize, SimulationActorBase::configuration->dx,
            SimulationActorBase::configuration->dy),
      currentState(SimulationActorState::INITIAL), currentTime(0.0f), timestepBaseline(0.0f), outputDelta(0.0f), nextWriteTime(0.0f),
      endTime(SimulationActorBase::configuration->scenario->endSimulation()), patchUpdates(0),
      patchArea(makePatchArea(SimulationActorBase::configuration, position[0], position[1])),
#if defined(WRITENETCDF)
      writer(SimulationActorBase::configuration->fileNameBase, {1, 1, 1, 1}, patchArea,
             SimulationActorBase::configuration->scenario->endSimulation() / SimulationActorBase::configuration->numberOfCheckpoints,
             SimulationActorBase::configuration->patchSize, SimulationActorBase::configuration->patchSize, SimulationActorBase::configuration->dx,
             SimulationActorBase::configuration->dy, patchArea.minX, patchArea.minY, 1),
#elif defined(WRITEVTK)
      writer(configuration->fileNameBase, {1, 1, 1, 1}, SimulationActorBase::configuration->patchSize, SimulationActorBase::configuration->patchSize,
             SimulationActorBase::configuration->dx, SimulationActorBase::configuration->dy, position[0] * SimulationActorBase::configuration->patchSize,
             position[1] * SimulationActorBase::configuration->patchSize),
#endif
      active(false) //, activeNeighbors()
{
}

SimulationActorBase::SimulationActorBase(const std::string &name, const std::tuple<size_t, size_t> &coordinates, bool slow) noexcept
    : ActorImpl(name), slow(slow), position{std::get<0>(coordinates), std::get<1>(coordinates)},
      block(SimulationActorBase::configuration->patchSize, SimulationActorBase::configuration->patchSize, SimulationActorBase::configuration->dx,
            SimulationActorBase::configuration->dy),
      currentState(SimulationActorState::INITIAL), currentTime(0.0f), timestepBaseline(0.0f), outputDelta(0.0f), nextWriteTime(0.0f),
      endTime(SimulationActorBase::configuration->scenario->endSimulation()), patchUpdates(0),
      patchArea(makePatchArea(SimulationActorBase::configuration, position[0], position[1])),
#if defined(WRITENETCDF)
      writer(SimulationActorBase::configuration->fileNameBase, {1, 1, 1, 1}, patchArea,
             SimulationActorBase::configuration->scenario->endSimulation() / SimulationActorBase::configuration->numberOfCheckpoints,
             SimulationActorBase::configuration->patchSize, SimulationActorBase::configuration->patchSize, SimulationActorBase::configuration->dx,
             SimulationActorBase::configuration->dy, patchArea.minX, patchArea.minY, 1),
#elif defined(WRITEVTK)
      writer(SimulationActorBase::configuration->fileNameBase, {1, 1, 1, 1}, SimulationActorBase::configuration->patchSize,
             SimulationActorBase::configuration->patchSize, SimulationActorBase::configuration->dx, SimulationActorBase::configuration->dy,
             position[0] * SimulationActorBase::configuration->patchSize, position[1] * SimulationActorBase::configuration->patchSize),
#else
// do nothing
#endif
      active(false) //, activeNeighbors()
{
}

SimulationActorBase::SimulationActorBase(ActorData &&ad, bool slow, size_t xPos, size_t yPos, SWE_WaveAccumulationBlock &&block,
                                         SimulationActorState currentState, float currentTime, float timestepBaseline, float outputDelta, float nextWriteTime,
                                         float endTime, uint64_t patchUpdates,
#if defined(WRITENETCDF)
                                         io::NetCdfWriter &&writer,
#elif defined(WRITEVTK)
                                         io::VtkWriter &&writer,
#endif
                                         SimulationArea &&sa, bool actv /*,std::vector<std::string> &&activeNeighbors*/) noexcept
    : ActorImpl(std::move(ad)), slow(slow), /* config(&configuration), */ position{xPos, yPos}, block(std::move(block)), currentState(currentState),
      currentTime(currentTime), timestepBaseline(timestepBaseline), outputDelta(outputDelta), nextWriteTime(nextWriteTime), endTime(endTime),
      patchUpdates(patchUpdates), patchArea(std::move(sa)),
#if defined(WRITENETCDF) || defined(WRITEVTK)
      writer(std::move(writer)),
#endif
      active(actv) //, activeNeighbors()
{
}

SimulationActorBase::SimulationActorBase(
    ActorData &&ad, std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> &&tup, bool slow,
    size_t xPos, size_t yPos, SWE_WaveAccumulationBlock &&block, SimulationActorState currentState, float currentTime, float timestepBaseline,
    float outputDelta, float nextWriteTime, float endTime, uint64_t patchUpdates,
#if defined(WRITENETCDF)
    io::NetCdfWriter &&writer,
#elif defined(WRITEVTK)
    io::VtkWriter &&writer,
#endif
    SimulationArea &&sa, bool actv) noexcept
    : ActorImpl(std::move(ad), std::move(tup)), slow(slow), position{xPos, yPos}, block(std::move(block)), currentState(currentState), currentTime(currentTime),
      timestepBaseline(timestepBaseline), outputDelta(outputDelta), nextWriteTime(nextWriteTime), endTime(endTime), patchUpdates(patchUpdates),
      patchArea(std::move(sa)),
#if defined(WRITENETCDF) || defined(WRITEVTK)
      writer(std::move(writer)),
#endif
      active(actv) //, activeNeighbors()
{
}

SimulationActorBase::SimulationActorBase(SimulationActorBaseData &&sabd) noexcept
    : ActorImpl(std::move(*(sabd.ad)), std::move(sabd.tup)), slow(sabd.slow), position{sabd.position[0], sabd.position[1]}, block(std::move(*(sabd.block))),
      currentState(sabd.currentState), currentTime(sabd.currentTime), timestepBaseline(sabd.timestepBaseline), outputDelta(sabd.outputDelta),
      nextWriteTime(sabd.nextWriteTime), endTime(sabd.endTime), patchUpdates(sabd.patchUpdates), patchArea(std::move(sabd.sa)),
#if defined(WRITENETCDF) || defined(WRITEVTK)
      writer(std::move(*(sabd.writer))),
#endif
      active(sabd.actv)
{
#ifdef TIME
    timeSpentFinishing = sabd.finishing;
    timeSpentRunning = sabd.running;
    timeSpentTerminated = sabd.terminated;
    timeSpentReading = sabd.timeSpentReading;
    timeSpentWriting = sabd.timeSpentWriting;
    timeSpentReceiving = sabd.timeSpentReceiving;
    timeSpentNotReceiving = sabd.timeSpentNotReceiving;
    timeSpentComputing = sabd.timeSpentComputing;
    timesWrittenToFile = sabd.timesWrittenToFile;
#endif

    delete sabd.writer;
    sabd.writer = nullptr;

    delete sabd.ad;
    sabd.ad = nullptr;

    delete sabd.block;
    sabd.block = nullptr;
}

SimulationActorBase::SimulationActorBase(const SimulationActorBase &sa) noexcept
    : ActorImpl(dynamic_cast<ActorImpl const &>(sa)), slow(sa.slow), position{sa.position[0], sa.position[1]}, block(sa.block), currentState(sa.currentState),
      currentTime(sa.currentTime), timestepBaseline(sa.timestepBaseline), outputDelta(sa.outputDelta), nextWriteTime(sa.nextWriteTime), endTime(sa.endTime),
      patchUpdates(sa.patchUpdates), patchArea(sa.patchArea),
#if defined(WRITENETCDF) || defined(WRITEVTK)
      writer(sa.writer),
#endif
      active(sa.active)
#ifdef TIME
      ,
      timeSpentRunning(sa.timeSpentRunning), timeSpentFinishing(sa.timeSpentFinishing), timeSpentTerminated(sa.timeSpentTerminated),
      timeSpentReading(sa.timeSpentReading), timeSpentWriting(sa.timeSpentWriting), timeSpentReceiving(sa.timeSpentReceiving),
      timeSpentNotReceiving(sa.timeSpentNotReceiving), timeSpentComputing(sa.timeSpentComputing), timesWrittenToFile(sa.timesWrittenToFile)
#endif

{
}

SimulationActorBase::SimulationActorBase(SimulationActorBase &&sa) noexcept
    : ActorImpl(dynamic_cast<ActorImpl &&>(std::move(sa))), slow(sa.slow), position{sa.position[0], sa.position[1]}, block(std::move(sa.block)),
      currentState(sa.currentState), currentTime(sa.currentTime), timestepBaseline(sa.timestepBaseline), outputDelta(sa.outputDelta),
      nextWriteTime(sa.nextWriteTime), endTime(sa.endTime), patchUpdates(sa.patchUpdates), patchArea(std::move(sa.patchArea)),
#if defined(WRITENETCDF) || defined(WRITEVTK)
      writer(std::move(sa.writer)),
#endif
      active(sa.active)
#ifdef TIME
      ,
      timeSpentRunning(sa.timeSpentRunning), timeSpentFinishing(sa.timeSpentFinishing), timeSpentTerminated(sa.timeSpentTerminated),
      timeSpentReading(sa.timeSpentReading), timeSpentWriting(sa.timeSpentWriting), timeSpentReceiving(sa.timeSpentReceiving),
      timeSpentNotReceiving(sa.timeSpentNotReceiving), timeSpentComputing(sa.timeSpentComputing), timesWrittenToFile(sa.timesWrittenToFile)
#endif
{
#ifdef PRINT
    std::cout << "move constructing simulationactor" << std::endl;
#endif
}

float SimulationActorBase::getMaxBlockTimestepSize()
{
    block.computeMaxTimestep();
    return block.getMaxTimestep();
}

void SimulationActorBase::setTimestepBaseline(float timestepBaseline) { this->timestepBaseline = timestepBaseline; }

void SimulationActorBase::act()
{

    if (currentState == SimulationActorState::TERMINATED)
    {
        return;
    }

    auto start = std::chrono::high_resolution_clock::now();

#ifdef LAZY_ACTIVATION
    if (currentState == SimulationActorState::RESTING)
    {
        if (!noChange())
        {
            currentState = SimulationActorState::RUNNING;
        }
    }
#endif

    // it is active send activation signal and change state to
    if (currentState == SimulationActorState::INITIAL && mayWrite())
    {

#if defined(REPORT_MAIN_ACTIONS) || defined(PRINT)
        l.cout() << getName() << " sending initial data to neighbors." << std::endl;

#endif

#ifndef NOWRITE
#ifdef PRINT
        std::cout << this->getName() << ", cur time: " << currentTime << ", timeStepBaseline: " << timestepBaseline << ", outputDelta " << outputDelta
                  << ", nextWriteTime " << nextWriteTime << ", endyTime " << endTime << std::endl;
#endif
        writeTimeStep(0.0f);
#ifdef TIME
        timesWrittenToFile += 1;
#endif
#endif

#ifdef TIME
        auto writestart = std::chrono::high_resolution_clock::now();
        sendData();
        auto writeend = std::chrono::high_resolution_clock::now();
        auto writeelapsed = std::chrono::duration_cast<std::chrono::microseconds>(writeend - writestart);
        timeSpentWriting += writeelapsed.count();
#else
        sendData();
#endif

#ifdef LAZY_ACTIVATION
        if (active)
        {
            currentState = SimulationActorState::RUNNING;
        }
        else
        {
            currentState = SimulationActorState::RESTING;
        }
#else
        currentState = SimulationActorState::RUNNING;
#endif
    }
#ifdef LAZY_ACTIVATION
    else if (currentState == SimulationActorState::RESTING && currentTime < endTime && !hasReceivedTerminationSignal() && mayRead() && mayWrite())
    {

#if defined(REPORT_MAIN_ACTIONS) || defined(PRINT)
        l.cout() << getName() << " iteration at " << currentTime << std::endl;

#endif

#ifdef TIME
        auto readstart = std::chrono::high_resolution_clock::now();
        receiveData();
        auto readend = std::chrono::high_resolution_clock::now();
        auto readelapsed = std::chrono::duration_cast<std::chrono::microseconds>(readend - readstart);
        timeSpentReading += readelapsed.count();
#else
        receiveData();
#endif

#ifdef TIME
        auto computestart = std::chrono::high_resolution_clock::now();
        // block.setGhostLayer();
        // block.computeNumericalFluxes();
        // block.updateUnknowns(timestepBaseline);
        auto computeend = std::chrono::high_resolution_clock::now();
        auto computeelapsed = std::chrono::duration_cast<std::chrono::microseconds>(computeend - computestart);
        timeSpentComputing += computeelapsed.count();
#else
        // block.setGhostLayer();
        // block.computeNumericalFluxes();
        // block.updateUnknowns(timestepBaseline);
#endif

#ifdef TIME
        auto writestart = std::chrono::high_resolution_clock::now();
        sendData();
        auto writeend = std::chrono::high_resolution_clock::now();
        auto writeelapsed = std::chrono::duration_cast<std::chrono::microseconds>(writeend - writestart);
        timeSpentWriting += writeelapsed.count();
#else
        sendData();
#endif
        currentTime += timestepBaseline;
#ifndef NOWRITE
        if (currentTime >= nextWriteTime)
        {
            writeTimeStep(currentTime);
            nextWriteTime += outputDelta;
#ifdef TIME
            timesWrittenToFile += 1;
#endif
#ifdef PRINT
            std::cout << this->getName() << ", cur time: " << currentTime << ", timeStepBaseline: " << timestepBaseline << ", outputDelta " << outputDelta
                      << ", nextWriteTime " << nextWriteTime << ", endTime " << endTime << std::endl;
#endif
        }
#endif
        patchUpdates++;
        if (currentTime > endTime)
        {
#ifdef PRINT
            l.cout() << getName() << "\treached endTime." << std::endl;
#endif
            currentState = SimulationActorState::FINISHED;
        }
    }
#endif
    else if (currentState == SimulationActorState::RUNNING && currentTime < endTime && !hasReceivedTerminationSignal() && mayRead() && mayWrite())
    {
#if defined(REPORT_MAIN_ACTIONS)
        l.cout() << getName() << " iteration at " << currentTime << std::endl;
#endif

#ifdef TIME
        auto readstart = std::chrono::high_resolution_clock::now();
        receiveData();
        auto readend = std::chrono::high_resolution_clock::now();
        auto readelapsed = std::chrono::duration_cast<std::chrono::microseconds>(readend - readstart);
        timeSpentReading += readelapsed.count();
#else
        receiveData();
#endif

#ifdef TIME
        auto computestart = std::chrono::high_resolution_clock::now();

#endif
        block.setGhostLayer();
        auto v1 = std::chrono::steady_clock::now();
        block.computeNumericalFluxes();
        auto v2 = std::chrono::steady_clock::now();
        auto calc = std::chrono::duration_cast<std::chrono::microseconds>(v2 - v1);
        std::this_thread::sleep_for(calc);

        block.updateUnknowns(timestepBaseline);
#ifdef TIME
        auto computeend = std::chrono::high_resolution_clock::now();
        auto computeelapsed = std::chrono::duration_cast<std::chrono::microseconds>(computeend - computestart);
        timeSpentComputing += computeelapsed.count();
#endif

#ifdef TIME
        auto writestart = std::chrono::high_resolution_clock::now();
        sendData();
        auto writeend = std::chrono::high_resolution_clock::now();
        auto writeelapsed = std::chrono::duration_cast<std::chrono::microseconds>(writeend - writestart);
        timeSpentWriting += writeelapsed.count();
#else
        sendData();
#endif
        currentTime += timestepBaseline;
#ifndef NOWRITE
        if (currentTime >= nextWriteTime)
        {
            writeTimeStep(currentTime);
            nextWriteTime += outputDelta;
#ifdef TIME
            timesWrittenToFile += 1;
#endif
#ifdef PRINT
            std::cout << this->getName() << ", cur time: " << currentTime << ", timeStepBaseline: " << timestepBaseline << ", outputDelta " << outputDelta
                      << ", nextWriteTime " << nextWriteTime << ", endTime " << endTime << std::endl;
#endif
        }
#endif
        patchUpdates++;
        if (currentTime > endTime)
        {
#ifdef PRINT
            l.cout() << getName() << "\treached endTime." << std::endl;
#endif
            currentState = SimulationActorState::FINISHED;
        }
    }
    else if ((currentState == SimulationActorState::FINISHED || hasReceivedTerminationSignal()) && mayWrite())
    {
#ifdef PRINT
        l.cout() << getName() << " terminating at " << currentTime << std::endl;
#endif

        sendTerminationSignal();
        currentState = SimulationActorState::TERMINATED;

        stop();
    }

    auto end = std::chrono::high_resolution_clock::now();

    // std::chrono::duration<double> elapsed = end - start;
    auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    size_t add = elapsed.count();

#ifdef TIME
    if (currentState == SimulationActorState::FINISHED)
    {
        timeSpentFinishing += add;
    }
    else if (currentState == SimulationActorState::RUNNING)
    {
        timeSpentRunning += add;
    }
    else if (currentState == SimulationActorState::TERMINATED)
    {
        timeSpentTerminated += add;
    }
#endif
}

SimulationActorBase::~SimulationActorBase() noexcept
{
#ifdef WRITENETCDF
    if (writer.open)
        writer.closeFile();
#endif
}

void SimulationActorBase::writeTimeStep(float currentTime)
{
#if defined(WRITENETCDF) || defined(WRITEVTK)
    writer.writeTimeStep(block.getBathymetry(), block.getWaterHeight(), block.getDischargeHu(), block.getDischargeHv(), upcxx::rank_me(), currentTime);
#endif
}

std::vector<float> SimulationActorBase::createDefaultData()
{
    std::vector<float> data;
    float psize = communicators->patchSize;
    data.resize(communicators->patchSize * 3);

    float val = configuration->scenario->defWaterHeight();
    std::fill(data.begin(), data.begin() + psize, val);
    std::fill(data.begin() + psize, data.end(), 0);
    assert(data.size() == psize * 3);
    return data;
}

bool SimulationActorBase::diffThanInitial(const std::vector<float> &packedData)
{
    float def = configuration->scenario->defWaterHeight();
    int patchSize = packedData.size() / 3;
    for (int i = 0; i < 3 * patchSize; i++)
    {

        if (i < patchSize)
        {
            float diff = def - packedData[i];
            if (diff < -0.05 || diff > 0.05)
            {
                // std::cout << diff << " " << def << " " << packedData[i] << std::endl;
                return true;
            }
        }
        else
        {
            float val = packedData[i];
            if (val < -0.05 || val > 0.05)
            {
                // std::cout << val << " " << def << std::endl;
                return true;
            }
        }
    }
    return false;
}

std::string SimulationActorBase::strConfig() const
{
    std::stringstream ss;
    ss << configuration->xSize << "\n";
    ss << configuration->ySize << "\n";
    ss << configuration->patchSize << "\n";
    ss << configuration->fileNameBase << "\n";
    ss << configuration->dx << "\n";
    ss << configuration->dy << "\n";
    ss << configuration->scenario << "\n";
    return ss.str();
}

void SimulationActorBase::computeWriteDelta()
{
    auto numCheckpoints = configuration->numberOfCheckpoints;
    auto endTime = configuration->scenario->endSimulation();
    this->outputDelta = endTime / numCheckpoints;
    this->nextWriteTime = this->outputDelta;
}

uint64_t SimulationActorBase::getNumberOfPatchUpdates() const { return patchUpdates; }

void SimulationActorBase::prepareDerived()
{
#ifdef WRITENETCDF
    this->writer.closeFile();
#endif
}

std::string SimulationActorBase::getTimeInfo()
{
    std::stringstream ss;
    ss << upcxx::rank_me() << ": Actor " << this->getName() << " worked for";
#ifdef TIME
    ss << ": Running " << (timeSpentRunning >> 6) << " -> Computing: " << (timeSpentComputing >> 6) << ", Finishing " << (timeSpentFinishing >> 6)
       << ", Terminated " << (timeSpentTerminated >> 6) << " read for: " << (timeSpentReading >> 6) << " written for: " << (timeSpentWriting >> 6)
       << " received signal for: " << (timeSpentReceiving >> 6) << " checked for hasnt received: " << (timeSpentNotReceiving >> 6)
       << " written to file for times: " << (timesWrittenToFile) << "\n";
#else
    ss << "it was not compiled with TIME option \n";
#endif

    return ss.str();
}

bool SimulationActorBase::getActive() const { return active; }
