#pragma once
#include "block/BlockCommunicator.hpp"
#include "block/SWE_Block.hh"
#include "block/SWE_WaveAccumulationBlock.hh"
#include "writer/Writer.hh"

#ifdef WRITENETCDF
#include "writer/NetCdfWriter.hh"
#else
#include "writer/VtkWriter.hh"
#endif

#include "SimulationActorState.hpp"
#include "actorlib/ActorData.hpp"

class InPortInformationBase;
class OutPortInformationBase;

#include "actorlib/PortInformationBase.hpp"
#include <memory>
#include <vector>

class SimulationActorBaseData
{
  public:
    ActorData *ad;
    size_t position[2];
    SWE_WaveAccumulationBlock *block;
    SimulationActorState currentState;
    float currentTime;
    float timestepBaseline;
    float outputDelta;
    float nextWriteTime;
    float endTime;
    uint64_t patchUpdates;
    SimulationArea sa;

#if defined(WRITENETCDF)
    io::NetCdfWriter *writer;
#elif defined(WRITEVTK)
    io::VtkWriter *writer;
#else
    void *writer;
#endif

    bool actv;

#ifdef TIME
    size_t running;
    size_t finishing;
    size_t terminated;

    size_t timeSpentReading;
    size_t timeSpentWriting;
    size_t timeSpentReceiving;
    size_t timeSpentNotReceiving;
    size_t timeSpentComputing;
    size_t timesWrittenToFile;
#endif

    bool slow;
    std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> tup;

    std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> copy_tup() const
    {
        auto &ins = std::get<0>(tup);
        auto &outs = std::get<1>(tup);

        std::vector<std::unique_ptr<InPortInformationBase>> r1;
        std::vector<std::unique_ptr<OutPortInformationBase>> r2;

        for (auto &in : ins)
        {
            auto *inp = in.get();
            r1.emplace_back(inp->generateCopy());
        }

        for (auto &out : outs)
        {
            auto *outp = out.get();
            r2.emplace_back(outp->generateCopy());
        }

        return std::make_tuple(std::move(r1), std::move(r2));
    }

    SimulationActorBaseData() noexcept
        : ad(nullptr), position{0, 0}, block(nullptr), currentState(), currentTime(0.f), timestepBaseline(0.f), nextWriteTime(0.f), endTime(0.f),
          patchUpdates(0), sa(), writer(nullptr), actv(false),
#ifdef TIME
          running(0), finishing(0), terminated(0), timeSpentReading(0), timeSpentWriting(0), timeSpentReceiving(0), timeSpentNotReceiving(0),
          timeSpentComputing(0), timesWrittenToFile(0),
#endif
          slow(false), tup()
    {
    }

    SimulationActorBaseData(SimulationActorBaseData &&other)
        : ad(other.ad), position{other.position[0], other.position[1]}, block(other.block), currentState(other.currentState), currentTime(other.currentTime),
          timestepBaseline(other.timestepBaseline), nextWriteTime(other.nextWriteTime), endTime(other.endTime), patchUpdates(other.patchUpdates), sa(other.sa),
#if defined(WRITENETCDF) || defined(WRITEVTK)
          writer(other.writer),
#endif
          actv(other.actv),
#ifdef TIME
          running(other.running), finishing(other.finishing), terminated(other.terminated), timeSpentReading(other.timeSpentReading),
          timeSpentWriting(other.timeSpentWriting), timeSpentReceiving(other.timeSpentReceiving), timeSpentNotReceiving(other.timeSpentNotReceiving),
          timeSpentComputing(other.timeSpentComputing), timesWrittenToFile(other.timesWrittenToFile),
#endif
          slow(other.slow), tup(std::move(other.tup))
    {
        other.ad = nullptr;
        other.block = nullptr;
#if defined(WRITENETCDF) || defined(WRITEVTK)
        other.writer = nullptr;
#endif
    }

    SimulationActorBaseData(const SimulationActorBaseData &other)
        : ad(new ActorData(*(other.ad))), position{other.position[0], other.position[1]}, block(new SWE_WaveAccumulationBlock(*other.block)),
          currentState(other.currentState), currentTime(other.currentTime), timestepBaseline(other.timestepBaseline), nextWriteTime(other.nextWriteTime),
          endTime(other.endTime), patchUpdates(other.patchUpdates), sa(other.sa),
#if defined(WRITENETCDF)
          writer(new io::NetCdfWriter(*dynamic_cast<io::NetCdfWriter *>(other.writer))),
#elif defined(WRITEVTK)
          writer(new io::VtkWriter(*dynamic_cast<io::VtkWriter *>(other.writer))),
#endif
          actv(other.actv),
#ifdef TIME
          running(other.running), finishing(other.finishing), terminated(other.terminated), timeSpentReading(other.timeSpentReading),
          timeSpentWriting(other.timeSpentWriting), timeSpentReceiving(other.timeSpentReceiving), timeSpentNotReceiving(other.timeSpentNotReceiving),
          timeSpentComputing(other.timeSpentComputing), timesWrittenToFile(other.timesWrittenToFile),
#endif
          slow(other.slow), tup(other.copy_tup())
    {
    }

    SimulationActorBaseData(
        ActorData *ad, size_t position[2], SWE_WaveAccumulationBlock *block, SimulationActorState currentState, float currentTime, float timestepBaseline,
        float outputDelta, float nextWriteTime, float endTime, uint64_t patchUpdates, SimulationArea sa,

#if defined(WRITENETCDF)
        io::NetCdfWriter *writer,
#elif defined(WRITEVTK)
        io::VtkWriter *writer,
#else
        void *writer,
#endif
        bool actv,
#ifdef TIME
        size_t running, size_t finishing, size_t terminated, size_t timeSpentReading, size_t timeSpentWriting, size_t timeSpentReceiving,
        size_t timeSpentNotReceiving, size_t timeSpentComputing, size_t timesWrittenToFile,
#endif
        bool slow, std::tuple<std::vector<std::unique_ptr<InPortInformationBase>>, std::vector<std::unique_ptr<OutPortInformationBase>>> &&tup) noexcept
        : ad(ad), position{position[0], position[1]}, block(block), currentState(currentState), currentTime(currentTime), timestepBaseline(timestepBaseline),
          nextWriteTime(nextWriteTime), endTime(endTime), patchUpdates(patchUpdates), sa(sa), writer(writer), actv(actv),
#ifdef TIME
          running(running), finishing(finishing), terminated(terminated), timeSpentReading(timeSpentReading), timeSpentWriting(timeSpentWriting),
          timeSpentReceiving(timeSpentReceiving), timeSpentNotReceiving(timeSpentNotReceiving), timeSpentComputing(timeSpentComputing),
          timesWrittenToFile(timesWrittenToFile),
#endif
          slow(slow), tup(std::move(tup))
    {
    }

    ~SimulationActorBaseData() noexcept
    {
        delete ad;
        delete block;
        delete writer;
    };

    UPCXX_SERIALIZED_DELETE();
};