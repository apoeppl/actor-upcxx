#include <fstream>
#include <iostream>
#include <random>
#include <string>

int main() {
  int len = 5;
  int rankcount = 2;
  std::random_device dev;
  std::mt19937 rng{dev()};
  std::uniform_int_distribution<> distrib(0, rankcount - 1);
  int occurences[rankcount] = {
      0,
  };
  int occurencelimit = len * len / rankcount;
  int randomranks[len * len] = {
      0,
  };

  if (len * len % rankcount != 0) {
    // some numbers have to get more
    occurencelimit += 1;
  }

  for (int i = 0; i < len; i++) {
    for (int j = 0; j < len; j++) {
      int k = distrib(rng);
      while (occurences[k] >= occurencelimit) {
        k = distrib(rng);
      }
      randomranks[i * len + j] = k;
      occurences[k] += 1;
    }
  }

  std::cout << "[" << std::endl;
  for (int i = 0; i < len; i++) {
    for (int j = 0; j < len - 1; j++) {
      if (randomranks[i * len + j] < 10) {
        std::cout << " " << randomranks[i * len + j] << ", ";
      } else {
        std::cout << randomranks[i * len + j] << ",";
      }
    }
    if (randomranks[i * len + len - 1] < 10) {
      std::cout << " " << randomranks[i * len + len - 2] << std::endl;
    } else {
      std::cout << randomranks[i * len + len - 2] << std::endl;
    }
  }
  std::cout << "]" << std::endl;

  for (int i = 0; i < rankcount; i++) {
    std::cout << "rank " << i << " has " << occurences[i] << " actors"
              << std::endl;
  }
  return 0;
}
