#include <upcxx/upcxx.hpp>

class NoMove {
public:
  int a;

  NoMove() : a(42) {}
  NoMove(int i) : a(i) {}
  NoMove(const NoMove &other) : a(other.a) {}
  NoMove(NoMove &&other) = delete;
};

int main() {
  upcxx::init();
  NoMove nm;
  if (upcxx::rank_me() == 0) {
    bool same = upcxx::rpc(
                    1, [](NoMove nm) { return nm.a; }, nm)
                    .wait();
    std::cout << "what have i done?" << std::endl;
  }
  upcxx::finalize();
}