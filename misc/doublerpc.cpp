#include <iostream>
#include <upcxx/upcxx.hpp>

int main() {
  upcxx::init();

  if (upcxx::rank_me() == 0) {
    bool back = upcxx::rpc(1, []() {
                  return upcxx::rpc(2, []() { return true; }).wait();
                }).wait();
    std::cout << "what have i done?" << std::endl;
  }
  upcxx::finalize();
}