#include <fstream>
#include <iostream>
#include <random>
#include <string>

int main() {
  std::random_device dev;
  std::mt19937 rng{dev()};
  std::uniform_int_distribution<> distrib(0, 55);

  std::string s = "";

  for (int i = 0; i < 32; i++) {
    for (int j = 0; j < 31; j++) {
      int k = distrib(rng);
      if (k < 10) {
        s += " " + std::to_string(k) + ", ";
      } else {
        s += std::to_string(k) + ", ";
      }
    }
    int k = distrib(rng);
    if (k < 10) {
      s += " " + std::to_string(k) + ",\n";
    } else {
      s += std::to_string(k) + ",\n";
    }
  }

  std::ofstream myfile;
  myfile.open("randomated.txt");
  myfile << s;
  myfile.close();
  return 0;
}